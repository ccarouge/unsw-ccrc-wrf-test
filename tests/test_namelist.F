program test_namelist
   implicit none
   
   real, dimension(1,1,10) :: t2_time, q2_time
   real, dimension(1,1) :: t2_mean, q2_max

   namelist /INPUT/ t2_time, q2_time
   namelist /OUTPUT/ t2_mean, q2_max

   open(13,'test.nml')
   read(13, INPUT)
   read(13, OUTPUT)

   print*, 'Input namelist: ', t2_time, q2_time
   print*, 'Output namelist: ', t2_mean, q2_mean

end program test_namelist
