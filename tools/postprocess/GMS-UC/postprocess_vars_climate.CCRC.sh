#! /bin/bash
# e.g. # ./postprocess_vars_climate.CCRC.sh /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf.ccrc.1month >& run_postprocess.log
Warnmsg="WARNING -- warning -- WARNING -- warning"
Errmsg="ERROR - error -- ERROR -- error"

if test $1 = '-h'
then
  echo " "
  echo "### ## # J. Fernandez & others. Grupo Meteorologia Santander # ## ###"
  echo "                 Universidad de Cantabria Oct. 2010"
  echo " Modified version by L. Fita. Climate Change Research Center. UNSW, Australia, Feb 2012"
  echo " "
  echo "****************************"
  echo "*** Shell to postprocess ***"
  echo "***     WRF outputs      ***"
  echo "***  obtained with WRF4G ***"
  echo "****************************"
  echo "postprocess_vars_climate.sh 'FILE' (absolute path/file)"
  echo "   'FILE' with different sections: "
  echo "       'MainFrame', 'FrameWork', 'GeneralNames', 'Periods', 'Variables'"
  echo "       "
  echo "        #MainFrame#"
  echo "        BASHsource [absolute path/file] file of any desired source"
  echo "        WRF4Ghome [WRF4Gpath] path to WRF4G root framework"
  echo "        p_interp [absolute path/p_interp] executable 'p_interp'"
  echo "        DomainNetCDF [absoulte path/netCDF of domain] netCDF of Domain (with prefix file g.e. 'geo_em.')"
  echo "        EXPDIR [absolute path] folder of experiment"
  echo "        POSTDIR [absolute path] folder for first step of postprocess"
  echo "        POST2DIR [absolute path] folder for second step of postprocess"
  echo "        POST3HDIR [absolute path] folder for 3H yearmon variable output"
  echo "        DIAGDIR [relative path] folder for diagnostics table ?"
  echo "        FIGSDIR [absolute path] folder for figures ?"
  echo "        OBSDIR [absolute path] folder for observations ?"
  echo "        postprocessor [name] WRF4G postprocessor name to be used"
  echo "        FULLnc netCDF file with all variables (without any postprocess)"
  echo "        FirstEXPy [YYYY] first year of experiment for avoiding inexistent 'prevfile'"
  echo "        LastEXPy [YYYY] Last year of experiment for avoiding inexistent 'nexfile'"
  echo "        overlap_yearmon [YYYYMM]:[YYYYMM]:... up to which [YYYY][MM] has to be taken from [YYYY]f and complete the year with [YYYY]i. This is usefull on overlaping years coming from different threads "
  echo "        checking [1/0] whether checking has to be done"
  echo "        VarnameSecFile [n] which number of section ('_varname_') of file gives variable name" 
  echo "        ptstime 2 pair of values to draw monthly time-series for checking"
  echo "        Rhome path to R executable"
  echo "        envFile file with the environment of the postprocess"
  echo "       "
  echo "        #GeneralNames#"
  echo "        @experiment@ [Name] (label to appear in output files)"
  echo "        @simulations@ [Name1]:[Name2]:... (experiment labels; ':' sep.)"
  echo "       "
  echo "        #Periods#"
  echo "        @[periodname]@ [values]"
  echo "        ..."
  echo "        #Variables#"
  echo "        [varname] [level] [temp_postprocess] [periodname]"
  echo "        ..."
  echo "             NOTE: if '#' appears previously to [varname],"
  echo "                   variable is not computed"
  echo "       "
else

####### Functions
# fl_periods: Function to provide start/ending total period from a list of periods [YY1f]-[YY1l],[YY2f]-[YY2l],... --> Nperiods, [YY1f]-[YYnl]
# period_values: Function to provide equivalent year periods according to a coma separted list of period labels following a file with the values
# file_freq_name: Function to provide frequency name for the file from the real used frequency
# sub_list: Function that creates a new ':' list from a section (from sval to eval) of the values of the components of the list 
# leave_firstN: Function to leave first N characters
# leave_lastN: Function to leave last N characters
# last_word: Function that gives the last word of a line selected by a key-word (from Ncol) from a given file
# export_MainFrame: Function to export all variables of 'MainFrame' 
# simulation_path: Function to provide path of the simulation
# read_postprocess_variables: Function to postprocess variables from a given file (1st arg) with different sections
# CFstandard_names: Function to provide the CFstandard_name:CFlong_name:units from 'wrfncxnj.table' file
#   to be computed
# NON_accVarNames: Function to provide 'stats' surname from the name of such variables that are computed as a statistical value, but it is their
# secondsBetweenDates: Difference in seconds between two dates (up to days) DATE2 - DATE1
#   standard method
##

function secondsBetweenDates(){
# Difference in seconds between two dates (up to days) DATE2 - DATE1
# Date1
##
  YMD1=`(expr substr $1 1 8)`
  hours1=`(expr substr $1 9 2)`
  minutes1=`(expr substr $1 11 2)`
  seconds1=`(expr substr $1 13 2)`
  hours1=`(expr $hours1 '*' 3600)`
  minutes1=`(expr $minutes1 '*' 60)`
  seconds1=`(expr $hours1 + $minutes1 + $seconds1)`
  sec1=`(date +%s -u -d"$YMD1 $seconds1 seconds")`

# Date2
##
  YMD2=`(expr substr $2 1 8)`
  hours2=`(expr substr $2 9 2)`
  minutes2=`(expr substr $2 11 2)`
  seconds2=`(expr substr $2 13 2)`
  hours2=`(expr $hours2 '*' 3600)`
  minutes2=`(expr $minutes2 '*' 60)`
  seconds2=`(expr $hours2 + $minutes2 + $seconds2)`
  sec2=`(date +%s -u -d"$YMD2 $seconds2 seconds")`
 
  seconds=`(expr $sec2 - $sec1)`

  echo $seconds
}

function fl_periods() {
# Function to provide start/ending total period from a list of periods [YY1f]-[YY1l],[YY2f]-[YY2l],... --> Nperiods, [YY1f]-[YYnl]
  pers=$1
  perns=`echo ${pers} | tr ',' ' '`
  Npers=`echo ${perns} | wc -w`
  fyr=`echo ${perns} | tr ' ' '\n' | head -n 1 | tr '-' ' ' | awk '{print $1}'`
  lyr=`echo ${perns} | tr ' ' '\n' | tail -n 1 | tr '-' ' ' | awk '{print $2}'`
  echo ${Npers} ${fyr}-${lyr}
}

function period_values() {
# Function to provide equivalent year periods according to a coma separted list of period labels following a file with the values
  periodlabs=$1
  filevals=$2

  perns=`echo ${periodlabs} | tr ',' ' '`
  Npers=`echo ${perns} | wc -w`

  yrpers=''
  iper=1
  for pern in ${perns}; do
    yr1per=`cat ${filevals} | grep '@'${pern}'@' | awk '{print $2}'` 
    if test ${iper} -eq 1; then
      yrpers=${yr1per}
    else
      yrpers=${yrpers}','${yr1per}
    fi
    iper=`expr ${iper} + 1`
  done # end of periods

  echo ${Npers}" "${yrpers}
}

function NON_accVarNames() {
# Function to provide 'stats' surname from the name of such variables that are computed as a statistical value, but it is their standard method
#   to be computed
#
####### variables
# filen: name of the file
# varn: variable name
# CFvarn: netCDF CF varname
#
####
# nostvars: variables that should not contain a statistic 'surname' in their name [old variable name]:[CF varname]@[correct variable name]
  filen=$1
  varn=$2
  CFvarn=$3

  nostvars='prac:precipitation_flux@pr praccac:precipitation_amount@pracc'
  nostvars=${nostvars}' tasmeantstepmean:air_temperature@tasmeantstep tasmintstepmean:air_temperature@tasmintstep tasmaxtstepmean:air_temperature@tasmaxtstep'
  nostvars=${nostvars}' pr1Hmaxtstepmean:1Hmax_precipitation_flux@pr1Hmaxtstep wss1Hmaxtstepmean:1Hmax_air_velocity@wss1Hmaxtstep'
  nostvars=${nostvars}' pr30maxtstepmean:30max_precipitation_flux@pr30maxtstep wss30maxtstepmean:30max_air_velocity@wss30maxtstep'
  nostvars=${nostvars}' pr20maxtstepmean:20max_precipitation_flux@pr20maxtstep wss20maxtstepmean:20max_air_velocity@wss20maxtstep'
  nostvars=${nostvars}' pr10maxtstepmean:10max_precipitation_flux@pr10maxtstep wss10maxtstepmean:10max_air_velocity@wss10maxtstep'
  nostvars=${nostvars}' pr5maxtstepmean:5max_precipitation_flux@pr5maxtstep wss5maxtstepmean:5max_air_velocity@wss5maxtstep'
  nostvars=${nostvars}' pr1Hmaxtstepmax:1Hmax_precipitation_flux@pr1Hmaxtstep wss1Hmaxtstepmax:1Hmax_air_velocity@wss1Hmaxtstep'
  nostvars=${nostvars}' pr30maxtstepmax:30max_precipitation_flux@pr30maxtstep wss30maxtstepmax:30max_air_velocity@wss30maxtstep'
  nostvars=${nostvars}' pr20maxtstepmax:20max_precipitation_flux@pr20maxtstep wss20maxtstepmax:20max_air_velocity@wss20maxtstep'
  nostvars=${nostvars}' pr10maxtstepmax:10max_precipitation_flux@pr10maxtstep wss10maxtstepmax:10max_air_velocity@wss10maxtstep'
  nostvars=${nostvars}' pr5maxtstepmax:5max_precipitation_flux@pr5maxtstep wss5maxtstepmax:5max_air_velocity@wss5maxtstep'

  iname=0
  for nostvar in ${nostvars}; do
    nostvarold=`echo ${nostvar} | tr '@' ' ' | awk '{print $1}'`
    nostvarn=`echo ${nostvar} | tr '@' ' ' | awk '{print $2}'`
    if test ${varn}':'${CFvarn} = ${nostvarold}; then
      fileN=`basename ${filen}`
      foldN=`dirname ${filen}`
      fileNnc=`echo ${fileN} | tr '.' ' ' | awk '{print $1}'`
      LfileNnc=`expr length ${fileNnc}`
      LfileNnc2=`expr ${LfileNnc} - 2`
      fileNncnew=${fileNnc:0:${LfileNnc2}}
      echo ${nostvarn}" "${foldN}"/"${fileNncnew}
      iname='1'
      break
    fi
  done
  if test ${iname} -eq 0; then echo ${varn}" "${filen}; fi
}

function CFstandard_names() {
# Function to provide the CFstandard_name:CFlong_name:units ('@' for space) from 'wrfncxnj.table' file
  wrfncxnjtable=$1
  varname=$2

  c1='#'
  c2=\"

  nlinevar=`cat -n ${wrfncxnjtable} | awk '{print " "$2" "$1}' | grep ' '${varname}' ' | awk '{print $2}' | head -n 1`
  varline=`head -n ${nlinevar} ${wrfncxnjtable} | tail -n 1`
  varline_tmp1=`echo ${varline} | tr ' ' ':' | sed s/${c2}/${c1}/g`
  ilongname=`expr index ${varline_tmp1} ${c1}`
  varline_tmp2=`leave_firstN $varline_tmp1 $ilongname`
  elongname=`expr index ${varline_tmp2} ${c1}`
  elname1=`expr ${elongname} - 1`
  longname=`expr substr ${varline_tmp2} 1 ${elname1}`
  elname1=`expr ${elongname} + 1`
  varline_tmp3=`leave_firstN $varline_tmp2 $elname1`
  istdname=`expr index ${varline_tmp3} ${c1}`
  varline_tmp4=`leave_firstN $varline_tmp3 $istdname`
  estdname=`expr index ${varline_tmp4} ${c1}`
  estdname1=`expr ${estdname} - 1`
  stdname=`expr substr ${varline_tmp4} 1 ${estdname1}`
  estdname1=`expr ${estdname} + 1`
  varline_tmp5=`leave_firstN $varline_tmp4 $estdname1`
  units=`echo ${varline_tmp5} | tr '#' ' ' | awk '{print $1}'`

  stdname=`echo ${stdname} | tr ':' '@'`
  longname=`echo ${longname} | tr ':' '@'`
  untis=`echo ${units} | tr ':' '@'`
  
  echo ${stdname}':'${longname}':'${units}
}

function file_freq_name() {
# Function to provide frequency name for the file from the real used frequency
  freqN=$1
    
  freq2=${freqN:0:2}

  case ${freq2} in
    'DA')     freqNfile='DAM'     ;;
    'MO')     freqNfile='MOM'     ;;
    'SE')     freqNfile='SEM'     ;;
    'YE')     freqNfile='YEM'     ;;
    *)        freqNfile=${freqN}  ;;
  esac

  freq3=${freqN:2:1}
  case ${freq3} in
    'N')      varStat='min'     ;;
    'X')      varStat='max'     ;;
    'M')      varStat='mean'    ;;
    'S')      varStat='ac'      ;;
  esac

  echo ${freqNfile} ${varStat}
}

function sub_list() {
# Function that creates a new ':' list from a section (from sval to eval) of the values of
#    the components of the list 
   list=$1
   sval=$2
   eval=$3

   Nvalues=`echo ${list} | tr ':' ' ' | wc -w | awk '{print $1}'`
   iv=1

   while test ${iv} -le ${Nvalues}
   do
     val=`echo ${list}" "${iv}" "$sval" "$eval | awk '{split($1,words,":"); \
       print substr(words[$2],$3,$4-$3+1)}'`
     if test ${iv} -eq 1
     then
       newlist=${val}
     else
       newlist=${newlist}':'${val}
     fi
     iv=`expr ${iv} + 1`
   done # end Nvalues

   echo ${newlist}
}

function leave_firstN() {
# Function to leave first N characters
  string=$1
  N=$2

  N1=`expr $N + 1`
  Lstring=`expr length $string`

  Estring=`expr $Lstring - $N`
  newstring=`expr substr ${string} $N1 ${Estring}`
  echo $newstring
}

function leave_lastN() {
# Function to leave last N characters
  string=$1
  N=$2
  Lstring=`expr length $string`
  
  Estring=`expr $Lstring - $N`
  newstring=`expr substr ${string} 1 ${Estring}`
  echo $newstring
}

function last_word() {
# Function that gives the last word of a line selected by a key-word (from Ncol) from a 
#    given file
  wfile=$1
  keyword=$2
  Ncol=$3

  case $Ncol in
    1)
      nlinevar=`cat -n ${wfile} | awk '{print " "$2" "$1}' | grep ' '${keyword}' ' | awk '{print $2}' | head -n 1`
    ;;
    2)
      nlinevar=`cat -n ${wfile} | awk '{print " "$3" "$1}' | grep ' '${keyword}' ' | awk '{print $2}' | head -n 1`
    ;;
    3)
      nlinevar=`cat -n ${wfile} | awk '{print " "$4" "$1}' | grep ' '${keyword}' ' | awk '{print $2}' | head -n 1`
    ;;
  esac
  fline=`head -n ${nlinevar} ${wfile} | tail -n 1`
  nword=`echo ${fline} | wc -w | awk '{print $1}'`
  fline=`echo ${fline} | tr ' ' ':'`
  word=`echo $fline" "$nword | awk '{split($1,words,":"); print words[$2]}'`
  echo $word
}

function export_MainFrame() {
# Function to export all variables of 'MainFrame' 
  mfile=$1
  export POSTFILE=${mfile}

  Nframe=`cat -n ${mfile} | grep '#MainFrame#' | awk '{print $1}'`
  Nnames=`cat -n ${mfile} | grep '#GeneralNames#' | awk '{print $1}'`

  iframe=`expr $Nframe + 1`
  lastframe=`expr $Nnames - 1`

  while test $iframe -lt $lastframe
  do
    varname=`head -n ${iframe} ${mfile} | tail -n 1 | awk '{print $1}'`
    varvalue=`head -n ${iframe} ${mfile} | tail -n 1 | awk '{print $2}'`
    export -p ${varname}=${varvalue}

    iframe=`expr ${iframe} + 1`
### End of variables of MainFrame
  done
}

function environtment_creation() {
# function to create the environment file from requested file
  reqf=$1

  varnamesl='BASHsource:WRF4Ghome:p_interp:DomainNetCDF:EXPDIR:POSTDIR:POST2DIR:POSTPROJDIR:postprocessor:FULLnc'
  varnamesl=${varnamesl}':FirstEXPy:LastEXPy:overlap_yearmon:checking:VarnameSecFile:ptstime:DOMAINsim:NAMELISToutput:RefDate:Rhome:CDOhome:ncHOME'

  envfile=`cat ${reqf} | grep envFile | awk '{print $2}'`
  rm ${envfile} >& /dev/null

  varnames=`echo ${varnamesl} | tr ":" " "`
  ivarf=1
  for varname in ${varnames}; do
    value=`cat ${reqf} | grep ${varname} | awk '{print $2}'`
    if test ${ivarf} -eq 1; then
      echo ${varname}"="${value} > ${envfile}
    else
      echo ${varname}"="${value} >> ${envfile}
    fi
    ivarf=`expr ${ivarf} + 1`
  done
}

##
export_MainFrame $1
environtment_creation $1
##
overlap_years=`sub_list ${overlap_yearmon} 1 4`

function simulation_path(){
# Function to provide path of the simulation
  sname=$1
   Nargs=`cat ${POSTFILE} | grep ${sname} | wc -w`
   if test $Nargs -eq 3; then
     simpath=$EXPDIR'/'$(cat ${POSTFILE} | grep ${sname} | awk '{print $3}')
   else
     simpath=$EXPDIR
   fi
   echo ${simpath}
}

function read_postprocess_variables() {
  # Function to postprocess variables from a given file (1st arg) with different sections: 
  # 'MainFrame', 'FrameWork', 'GeneralNames', 'Periods' and 'Variables'
  #
  # #MainFrame#
  # (...)
  #
  # #GeneralNames#
  # @experiment@ [Name] (label to appear in output files)
  #
  # #Periods#
  # @[periodname]@=[values]
  # ...
  #
  # #Variables#
  # [varname] [level] [temp_postprocess] [periodname]
  # ...
  # NOTE: if previously to [varname] is there a '#', variable is not computed
  ##
  # 
  errmsg='ERROR -- error -- ERROR -- error'

  file=$1

  Nvariables=`cat -n ${file} | grep '#Variables#' | awk '{print $1}'`
  experimentName=`cat ${file} | grep '@experiment@' | awk '{print $2}'`

  filesk='globalattr projection'
# global attributes file: wrfncxnj.globalattr_[experimentName]
# projection attributes file: wrfncxnj.projection_[experimentName]
  for filekind in ${filesk}; do
    if test -f ${ROOTpost}/wrfncxnj.${filekind}_${experimentName}; then
      if test ${filekind} = 'globalattr'; then
        export -p ${filekind}FILE=${ROOTpost}/wrfncxnj.${filekind}_${experimentName}_wrfconf
## Addition of WRF configuration
        echo "  WRF configuration... .. ."
        DOMAINsimS=`echo ${DOMAINsim} | awk '{printf("%02d",$1)}'`
        WRFfileConf=`ls -1 ${EXPDIR}/wrfout_d${DOMAINsimS}_* | head -n 1 | tail -n 1`
        python ${WRF4Ghome}/util/python/wrf_schemes.py -w ${WRF4Ghome}/util/WRF/WRF_schemes.inf -f ${WRFfileConf} -s \
          ${WRFconf}
        cp ${ROOTpost}/wrfncxnj.${filekind}_${experimentName} ${ROOTpost}/wrfncxnj.${filekind}_${experimentName}_wrfconf
        cat wrf.conf >> ${ROOTpost}/wrfncxnj.${filekind}_${experimentName}_wrfconf
      else
        export -p ${filekind}FILE=${ROOTpost}/wrfncxnj.${filekind}_${experimentName}
      fi
    else
      echo ${errmsg}
      echo "  Mandatory file '"${ROOTpost}"/wrfncxnj."${filekind}"_"${experimentName}"' does not exist !!!"
      echo ${errmsg}
      exit
    fi
  done

# NO blank lines in 'wrfncxnj.globalattr_[ExperimentName]'
#

  if test ! -f ${FULLnc}; then
    echo ${errmsg}
    echo "  Mandatory file FULLnc '"${FULLnc}"' does not exist !!!"
    echo ${errmsg}
    exit
  fi

  export -p wxajcmd="python ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.py -a ${globalattrFILE} -g ${tmpdir}/${domname} -r ${RefDate} --fullfile ${FULLnc}" 
##export -p ${wxajcmd}

#
# NO geo_em.d0N.nc
##  wxajcmd="python ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.py -a ${globalattrFILE} -r ${RefDate} --fullfile ${FULLnc}" 

  if test $cleanAll -eq 1; then
    echo ${Warnmsg}
    echo "  Removing all the previous post-processed data ! ..."
    echo ${Warnmsg}

    rm $POSTDIR/* >& /dev/null
    rm $POST2DIR/${experimentName}* >& /dev/null
    rm $POSTPROJDIR/${experimentName}* >& /dev/null
#    for tdir in ${ROOTpost}/tmpdir.*; do
#      if test ! ${tdir} = ${tmpdir}; then rm -rf ${ROOTpost}/tmpdir.* ${tmpdir}; fi
#    done # end of tmpdirs
  fi

  if test $cleanAll -eq 2; then
    echo ${Warnmsg}
    echo " Removing all the previous post-processed data except content in '"${POST2DIR}"' ! ..."
    echo ${Warnmsg}

    rm $POSTDIR/* >& /dev/null
    rm $POSTPROJDIR/${experimentName}* >& /dev/null
  fi

  Nlines=`wc -l ${file} | awk '{print $1}'`

  ivar=`expr $Nvariables + 1`
  while test ${ivar} -le ${Nlines}
  do
    varname=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $1}'`
    if test ! ${varname} = '#'
    then
      do_nothing=false
#
# Checking existence of variable
      Nwords=`cat ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | grep ${varname} | wc -w`
      if test ${Nwords} -lt 4; then
        echo ${errmsg}
        echo "  Variable name '"${varname}"' does not exist in '"                                \
          ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table"'!!"
        exit
      fi

      lastword=`last_word ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${varname} 1`
      if test ${lastword} = 'do_nothing'
      then
        do_nothing=true
        varname=`leave_lastN ${varname} 4`
      fi
      nlinevar=`cat -n ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | awk '{print " "$2" "$1}' | grep ' '${varname}' ' | awk '{print $2}'`
      CFvarname=`head -n ${nlinevar} ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table | tail -n 1 | awk '{print $2}'`
      tablevalues=`CFstandard_names ${WRF4Ghome}/util/postprocess/wrfncxnj/wrfncxnj.table ${varname}`
      CFstdname=`echo ${tablevalues} | tr ':' ' ' | awk '{print $1}' | tr '@' ' ' `

      outkind=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $2}'`
      varlev=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $3}'`
      vartime=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $4}'`
      varperiod=`head -n ${ivar} ${file} | tail -n 1 | awk '{print $5}'`

#
# Checking periods to postprocess
#      periodval=`cat ${file} | grep '@'${varperiod}'@' | awk '{print $2}'` 
      periodval=`period_values ${varperiod} ${file} | awk '{print $2}'`
      periodvaln=`fl_periods ${periodval} | awk '{print $2}'`

      Lperiodval=`expr length ${periodval}`
      Lperiodval=`expr ${Lperiodval} + 0`
      if test ${Lperiodval} -lt 1
      then
        echo "ERROR -- error -- ERROR -- error: Period '"${varperiod}"' does not exists!"
        exit
      fi
      speriodval=`echo ${periodvaln} | tr '-' ' ' | awk '{print $1}'`
      eperiodval=`echo ${periodvaln} | tr '-' ' ' | awk '{print $2}'`
     
      speriodval=${speriodval}'01'
      eperiodval=${eperiodval}'12'
      echo "  POSTprocessing '"${varname}"' at "${varlev}" along period: "${speriodval}"-"       \
        ${eperiodval}" frq: "${vartime}

#
# Period base files of the variables are only produced if they do not exist before and only are 
#   produced the files that specifically require the project
#
      read freqname thetimes <<< ${vartime/_/ }

      firstPerChar=${freqname:0:1} 
      pervals=`echo ${periodval} | tr ',' ' '`

      if test ${firstPerChar} = '&'; then
        freqend=`echo ${freqname} | tr '&' ' ' | tr '+' ' ' | awk '{print $1}'`
        freqprj=`echo ${freqname} | tr '&' ' ' | tr '+' ' ' | awk '{print $2}'`
        freqp=`file_freq_name ${freqend} | awk '{print $1}'`
        stat=`file_freq_name ${freqprj} | awk '{print $2}'`
        CFvarnameStat=${CFvarname}${stat}
        CFvarnameStat=`NON_accVarNames file_${CFvarnameStat}.nc ${CFvarnameStat} ${CFstdname} |  \
          awk '{print $1}'`

        if test $cleanINDIV = 1; then
          rm ${POSTPROJDIR}/${experimentName}_${freqp}_${periodvaln}_${CFvarname}${stat}.nc.gz >& /dev/null
          rm ${POSTPROJDIR}/${experimentName}_${freqp}_${periodvaln}_${CFvarname}${stat}.nc >& /dev/null
          for perval in ${pervals}; do
            rm ${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}${stat}.nc.gz >& /dev/null
            rm ${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}${stat}.nc >& /dev/null
          done # end of periods
        fi
      else
        if test $cleanINDIV = 1; then
          rm ${POSTPROJDIR}/${experimentName}_${freq}_${periodvaln}_${CFvarname}.nc.gz >& /dev/null
          rm ${POSTPROJDIR}/${experimentName}_${freq}_${periodvaln}_${CFvarname}.nc >& /dev/null
          for perval in ${pervals}; do
            rm ${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}.nc.gz >& /dev/null
            rm ${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}.nc >& /dev/null
          done # end of periods
        fi
      fi

#
## Production of high frequency outputs
#
      if test ! ${firstPerChar} = '&'; then
        for perval in ${pervals}; do

          if test ! -f ${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}.nc.gz && \
            test ! -f ${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}.nc; then
# Producing CF-convention files from WRF outs
#
            speriodval=`echo ${perval} | tr '-' ' ' | awk '{print $1}'`
            eperiodval=`echo ${perval} | tr '-' ' ' | awk '{print $2}'`
     
            speriodval=${speriodval}'01'
            eperiodval=${eperiodval}'12'

            echo "Generation of the file :'"${POST2DIR}/${experimentName}_All_${perval}_${CFvarname}.nc"'"
            ${ROOTpost}/get_hfq_data.bash ${ROOTpost}/${envFile} ${experimentName} $(simulation_path \
              ${experimentName}) ${varname} ${outkind} ${speriodval} ${eperiodval} ${varlev} ${CFvarname}

# Producing period values
#
            ${ROOTpost}/process.bash ${ROOTpost}/${envFile} ${experimentName} ${CFvarname} ${varlev} ${perval} \
              ${NAMELISToutput} ${do_nothing} ${PROCESSout}
          fi # end of pre-existing files

        done # end of periods
      fi # end of non '&'

# Producing project files
#
      # Checking for pressure interpolated values
      # 
      kindp=${outkind:0:2}
      if test ${kindp} == 'p@'; then
        Loutkindi=`expr length ${outkind}`
        Loutkindi2=`expr ${Loutkindi} - 2`
        outkind=${outkind:2:${Loutkindi}}
        isamp=`expr index ${outkind} '&'`
        if test ${isamp} -ne 0; then outkind=`echo ${outkind} | tr '&' ' ' | awk '{print $1}'`; fi
      fi
      if test ${firstPerChar} = '&'; then
        ${ROOTpost}/process_from_proj.bash ${ROOTpost}/${envFile} ${experimentName} ${CFvarnameStat} \
          ${varlev} ${vartime} ${periodval} ${do_nothing} ${outkind} ${PROJout}:${HFRQout}
      else
        ${ROOTpost}/process_from_hfrq.bash ${ROOTpost}/${envFile} ${experimentName} ${CFvarname}     \
          ${varlev} ${vartime} ${periodval} ${do_nothing} ${outkind} ${HFRQout}:${PROCESSout}
      fi

## removing intermediate data file (from get_hfq_data.bash)
      rm ${POSTDIR}/${experimentName}_*.nc >& /dev/null
# renaming intermediate data file (from get_3h_data)
      if test ! ${varlev} = 'SFC'; then
        varplev=$(printf "%04d" ${varlev})
      fi
#      rm ${POSTPROJDIR}/${experimentName}_*.nc >& /dev/null

    fi
    ivar=`expr ${ivar} + 1`
##    exit
  done # varname
}

#################################################
# Main program
################################################
inidate=`date +%Y%m%d%H%M%S`

#tag3h=minimal
variablesfile=$1

export -p requestedfile=${variablesfile}

scriptdir=$( (cd `dirname $0` && echo $PWD) )
thisdir=$(pwd)
export -p ROOTpost=${thisdir}

source $BASHsource -p
cd ${thisdir}
#
#  Get a private space to run
#
tmpdir=${thisdir}/tmpdir.`date +%Y%m%d%H%M%S%n`
mkdir -p ${tmpdir} && cd ${tmpdir} || exit
#
# NO geo_em.d0N.nc
cp ${DomainNetCDF} .
domname=`basename ${DomainNetCDF}`

#
# NO geo_em.d0N.nc
##domS=`printf "%02d" ${DOMAINsim}`
##DomainNetCDF=${DomainNetCDF}d${domS}.nc
##cp ${DomainNetCDF} .
##domname=`basename ${DomainNetCDF}`

mkdir -p $POSTDIR
mkdir -p $POST2DIR
mkdir -p $POSTPROJDIR

tag=$(date +'%Y%m%d%H%M%S')

# Postprocessing variables from '${variablesfile}'
read_postprocess_variables ${variablesfile}

rm ${tmpdir}/*
cd ${thisdir}
rmdir ${tmpdir}

enddate=`date +'%Y%m%d%H%M%S'`
diffseconds=`secondsBetweenDates ${inidate} ${enddate}`

cat << EOF > ${thisdir}/mail.inf
Postprocess finished!
requested file: $1
Output is in $POSTPROJDIR 
seconds used: ${diffseconds} 
EOF

mail -s "Postprocess of '$1' finished" ${UserMail} < ${thisdir}/mail.inf
echo "********************************"
echo "*** Postprocess FINISHED !!! ***"
echo "********************************"

fi
