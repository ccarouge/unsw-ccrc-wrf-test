#!/bin/bash
# e.g. # ./showVARS.bash 
rootsh=`pwd`
period='1990-1999'
outfold='/home/lluis/studies/NARCliMGreatSydney2km/data/d01/postPROJ/'${period}

fold=NARCliM11
#fold=123

rm ${fold}/*.nc 
cp ${outfold}/*.nc.gz ${fold}

cd ${fold}
for filen in *.nc.gz; do
  gunzip ${filen}
done

cd ${rootsh}
for filen in ${fold}/*.nc; do
  filenam=`basename $filen`
  varname=`echo $filenam | tr '_' ' ' | awk '{print $6}' | tr '.' ' ' | awk '{print $1}'`
  echo "**** '"${filen}"' varname :"${varname}
  ncdump -h $filen | grep ${varname} | grep -v 'history' | grep -v 'netcdf'
done # end of files
