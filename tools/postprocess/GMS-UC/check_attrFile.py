#!/usr/bin/python
# e.g. # ./check_attrFile.py -a wrfncxnj.globalattr_CCRC_GreatSydney

from optparse import OptionParser
import sys, time, string, csv

parser = OptionParser()
parser.add_option("-a", "--attributeFile", dest="attfile",
                  help="attribute file to check", metavar="FILE")
(opts, args) = parser.parse_args()

for line in csv.reader(
  open(opts.attfile, "r"),
  delimiter=" ",
  skipinitialspace=True
  ):
  print line[0], line[1]
