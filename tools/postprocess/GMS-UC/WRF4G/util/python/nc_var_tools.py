import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
import re

errormsg='nc_var_tools.py: ERROR -- error -- ERROR -- error'
warnmsg='nc_var_tool.py: WARNING -- warning -- WARNING -- warning'

fillValue = 1.e20

def filexist(filen, emsg, fmsg):
    """ Check existence of a file 
    filen= file name
    emsg= general error message
    fmsg= message before generic text of inexistence of file
    """
    if not os.path.isfile(filen):
        print emsg
        print '  ' + fmsg + ' file "' + filen + '" does not exist !!'
        print emsg
        quit(-1)    

def varinfile(ncf, filen, emsg, vmsg, varn):
    """ Check existence of a variable inside a netCDF file 
    ncf= netCDF object
    filen= file name
    emsg= general error message
    vmsg= message before generic text of inexistence of variable
    varn= variable name
    """
    if not ncf.variables.has_key(varn):
        print emsg
        print '  file "' + filen + '" does not have ' + vmsg + ' variable "' + varn + '" !!'
        print emsg
        ncf.close()
        quit(-1)    

class testvarinfile(object):
    """ Check existence of a variable inside a netCDF file 
    ncf= netCDF object
    filen= file name
    emsg= general error message
    vmsg= message before generic text of inexistence of variable
    varn= variable name
    self.exist= boolean value of existence
    self.message= error message
    """
    def __init__(self, ncf, filen, emsg, vmsg, varn):

        if ncf is None:
            self.exist= None
            self.message = None
        else:
            if not ncf.variables.has_key(varn):
                self.exist = False
                self.message = emsg + '\n' + '  file "' + filen + '" does not have "' + vmsg + '" variable "' + varn + '" !!\n' + \
                  emsg
            else:
                self.exist = True

def attrinvar(varobj, emsg, vmsg, attrn):
    """ Check existence of an attribute in a netCDF variable object
    varobj= netCDF variable object
    emsg= general error message
    vmsg= message before generic text of inexistence of variable
    attrrn= attribute name
    """
    varattrs = varobj.ncattrs()

    if not isInlist(varattrs, attrn):
        print emsg
        print '  variable "' + varobj.name + '" does not have attribute ' + attrn + '" !!'
        print emsg
        ncf.close()
        quit(-1)    

def reduce_spaces(string):
    """ Function to give words of a line of text removing any extra space
    """
    values = string.replace('\n','').split(' ')
    vals = []
    for val in values:
         if len(val) > 0:
             vals.append(val)

    return vals

def reduce_tabulars(string):
    """ Function to give words of a line of text removing any extra space and tabular separation
    """
    values = string.replace('\n','').split('\t')
    vals = []
    for val in values:
         if len(val) > 0:
             vals.append(val)

    return vals

def valmodoper(varVal, valuesS):
    """ Function to run the modification
    """
    valsS = valuesS.split(',')
    modins = valsS[0]
    modval = float(valsS[1])

    if modins == 'sumc':
        varVal[:] = varVal[:] + modval
    elif modins == 'subc':
        varVal[:] = varVal[:] - modval
    elif modins == 'mulc':
        varVal[:] = varVal[:] * modval
    elif modins == 'divc':
        varVal[:] = varVal[:] / modval
    elif modins == 'lowthres':
        varVal2 = np.where(varVal[:] < float(valsS[1]), float(valsS[2]), varVal[:])
        varVal[:] = varVal2
    elif modins == 'upthres':
        varVal2 = np.where(varVal[:] > float(valsS[1]), float(valsS[2]), varVal[:])
        varVal[:] = varVal2
    else: 
        print errormsg
        print '  valmodoper: Operation to modify values ' + modins + ' is not defined !!!'
        print errormsg
        quit(-1)

    return varVal

def valmod(values, ncfile, varn):
    """ Function to modify the value of a variable
    values = modins,modval1,[modval2,...]
      modins = instruction: 
        'sumc', add [modval1]
        'subc', substraction [modval1]
        'mulc', multiply by [modval1] 
        'divc', divide by [modval1] 
        'lowthres': modify all values below [modval1] to [modval2]
        'upthres': modify all values above [modval1] to [modval2]
    ncfile = netCDF file name
    varn = name of the variable
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile
#  #  errormsg='ERROR -- error -- ERROR -- error'
    vals = values.split(',')
    modins = vals[0]
    modval = float(vals[1])

    if not os.path.isfile(ncfile):
      print errormsg
      print '   valmod: File "' + ncfile + '" does not exist !!'
      print errormsg
      quit(-1)    

    ncf = NetCDFFile(ncfile,'a')

    if ncf.dimensions.has_key('plev'):
      # removing pressure level from variable name
      varn = re.sub("\d+", "", varn) 

    if not ncf.variables.has_key(varn):
      print errormsg
      print '   valmod: File does not have variable "' + varn + '" !!!!'
      print errormsg
      ncf.close()
      quit(-1)

    var = ncf.variables[varn]
    varshape = var.shape
    vartype = var.dtype
    Ndims = len(varshape)
    
    varshapevals = list(varshape)
    Nvars = len(varshape)
   
    if not Nvars == 0:
        if Nvars == 1:
            varvals = np.zeros(varshape[0], dtype=vartype) 
        elif Nvars == 2:
            varvals = np.zeros((varshape[0], varshape[1]), dtype=vartype)
        elif Nvars == 3:
            varvals = np.zeros((varshape[1], varshape[2]), dtype=vartype)
        elif Nvars == 4:
            varvals = np.zeros((varshape[2], varshape[3]), dtype=vartype) 
        elif Nvars == 5:
            varvals = np.zeros((varshape[3], varshape[4]), dtype=vartype)
        elif Nvars == 6:
            varvals = np.zeros((varshape[4], varshape[5]), dtype=vartype)
        else:
            print errormsg
            print '  fvaradd: variable size ',Nvars,' is not ready!!!!'

# Allocating all the necessary memory (just in case)

        if Nvars <= 2:
            varvals = var[:]
            var[:] = valmodoper(varvals, values)
        elif Nvars == 3:
            for i in range(varshape[0]):
                varvals[:] = var[i,:,:]
                var[i,:,:] = valmodoper(varvals, values)
        elif Nvars == 4:
    	    for i in range(varshape[0]):
                for j in range(varshape[1]):
                    varvals[:] = var[i,j,:,:]
                    var[i,j,:,:] = valmodoper(varvals, values)
        elif Nvars == 5:
  	    for i in range(varshape[0]):
                for j in range(varshape[1]):
                    for k in range(varshape[2]):
                        varvals[:] = var[i,j,k,:,:]
                        var[i,j,k,:,:] = valmodoper(varvals, values)
        elif Nvars == 6:
	    for i in range(varshape[0]):
                for j in range(varshape[1]):
                    for k in range(varshape[2]):
                        for l in range(varshape[3]):
                            varvals[:] = var[i,j,k,l,:,:]
                            var[i,j,k,l,:,:] = valmodoper(varvals, values)

    ncf.sync()
    ncf.close()

def rangedim(end, shape):
    """Gives the instruction to retrieve values from a dimension of a variable
    >>> print rangedim(-1, 15)
    15
    """
    if end == -1:
      return shape
    else:
      return end

def varaddref(values, ncfile, varn):
  """ Function to add a variable in an existing file copying characteristics from an existing one
  values = [variable ref]:[attr name]@[value][:[attr2]@[value2], ...]:[value/file with values] add a new variable [varn] 
    with dimension and attributes from an already existing [variable ref] with attributes [[attr name]@[value][:[attr2]@[value2], ...]] 
    in the file [netcdf] and value [value/file with valules]
  netcdf = netCDF file name
  varn = new variable name
  """

  varvalues = values.split(':')

  Nvarvalues = len(varvalues)
  varprev = varvalues[0]
  newattrs = {}
  for iattr in range(Nvarvalues - 2):
    attrv = varvalues[iattr+1]
    newattrs[attrv.split('@')[0]] = attrv.split('@')[1]

  ncf = NetCDFFile(ncfile,'a')

  if ncf.variables.has_key(varn):
    print errormsg
    print '   varaddref: File already has the varible ' + varn + ' !!!'
    print errormsg
    ncf.close()
    quit(-1)

  if not ncf.variables.has_key(varprev):
    print errormsg
    print '    varaddref: File does not have variable ' + varprev + ' !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  varp = ncf.variables[varprev]
  varpVal = varp[:]
  varpshape = varp.shape
  Nvarpshape = len(varpshape)
  varpattrs = varp.ncattrs()
  varptype = varp.dtype
  varpdims = varp.dimensions
  varpndims = varp.ndim

##  print '  shape of the variable used as reference: ',varpshape
#
# variable characteristics 
  if len(varpshape) == 4:
    dimpt = varpshape[0]
    dimpz = varpshape[1]
    dimpy = varpshape[2]
    dimpx = varpshape[3]
  elif len(varpshape) == 3:
    dimpt = varpshape[0]
    dimpy = varpshape[1]
    dimpx = varpshape[2]
  elif len(varpshape) == 3:
    dimpy = varpshape[0]
    dimpx = varpshape[1]

  newvar = ncf.createVariable(varn, varptype, dimensions=varpdims)
  newvar = ncf.variables[varn]
#
# Values
  varplen=1
  for idim in range(varpndims):
    varplen=varplen*varpshape[idim]

  if varptype == 'float' or varptype == 'float32' or varptype == 'float64' or vartype == np.float(1.) or vartype == np.float32(1.):
    newvals = np.reshape(np.arange(varplen), varpshape)*1.
  else:
    newvals = np.reshape(np.arange(varplen), varpshape)

  if not os.path.isfile(varvalues[Nvarvalues-1]):
##    print '  Using constant value'
    newvals[:] = float(varvalues[Nvarvalues-1])
  else:
##    print '  Using 2-D values from ' + varvalues[Nvarvalues-1]
    asciif = open(varvalues[Nvarvalues-1], 'r')

    fdimx = len(asciif.readlines())
    asciif.seek(0)

    if len(varpshape) == 4:
      if not fdimx == dimpx:
        print errormsg
        print '   varaddref: Provided file has dimx=', fdimx, ' and variable has dimx=', dimpx, ' !!!'
        print errormsg
        ncf.close()
        quit(-1)

      iline = 0
      idx = 0
      for fline in asciif:
        line = fline.replace('\n','')
        yvals = []
        for iyval in line.split('     '):
          yvals.append(float(iyval))

        if iline == 0:
          fdimy = len(yvals)
          if not fdimy == dimpy:
            print errormsg
            print '    varaddref: Provided file has dimy=', fdimy, ' and variable has dimy= ', dimpy, ' !!!'
            print errormsg
            ncf.close()
            quit(-1)
        for it in range(dimpt):
          for iz in range(dimpz):
            newvals[it,iz,:,idx] = yvals
  
        idx = idx+1
        iline = iline+1

    elif len(varpshape) == 3:
      if not fdimx == dimpx:
        print errormsg
        print '    varaddref: Provided file has dimx=', fdimx, ' and variable has dimx=', dimpx, ' !!!'
        print errormsg
        ncf.close()
        quit(-1)

      iline = 0
      idx = 0
      for fline in asciif:
        line = fline.replace('\n','')
        yvals = []
        for iyval in line.split('     '):
          yvals.append(float(iyval))

        if iline == 0:
          fdimy = len(yvals)
          if not fdimy == dimpy:
            print errormsg
            print '    varaddref: Provided file has dimy=', fdimy, ' and variable has dimy= ',dimpy, ' !!!'
            print errormsg
            ncf.close()
            quit(-1)
        for it in range(dimpt):
          newvals[it,:,idx] = yvals

        idx = idx+1
        iline = iline+1
    elif len(varpshape) == 2:
      if not fdimx == dimpx:
        print errormsg
        print '    varaddref: Provided file has dimx=', fdimx, ' and variable has dimx=', dimpx, ' !!!'
        print errormsg
        ncf.close()
        quit(-1)

      iline = 0
      idx = 0
      for fline in asciif:
        line = fline.replace('\n','')
        yvals = []
        for iyval in line.split('     '):
          yvals.append(float(iyval))

        if iline == 0:
          fdimy = len(yvals)
          if not fdimy == dimpy:
            print errormsg
            print '    varaddref: Provided file has dimy=', fdimy, ' and variable has dimy= ',dimpy, ' !!!'
            print errormsg
            ncf.close()
            quit(-1)

        newvals[:,idx] = yvals
        idx = idx+1
        iline = iline+1

  asciif.close()
  newvar[:] = newvals

#
# Attributes
  for iattr in range(len(varpattrs)):
    attrn = varpattrs[iattr]
    attrval = varp.getncattr(attrn)

    if attrn in newattrs:
      newattr = newvar.setncattr(attrn, newattrs[attrn])
    else:
      newattr = newvar.setncattr(attrn, attrval)

  ncf.sync()
  ncf.close()

def varout(values, ncfile, varn):
  """ Function when we want to output variable values
  values = [optsIrange]:[optsErange]
    [optsIrange]: val1,val2,...,valN inital value for the 'N' dimensions of the variable
    [optsErange]: val1,val2,...,valN ending value for the 'N' dimensions of the variable
  ncfile = netCDF file name
  varn = variable name
  """
  import numpy as np
  from netCDF4 import Dataset as NetCDFFile

  optsIrange = values.split(':')[0]
  optsErange = values.split(':')[1]

  inirange=optsIrange.split(',')
  endrange=optsErange.split(',')

  irange = [int(val) for val in inirange]
  erange = [int(val) for val in endrange]

  if not len(irange) == len(erange):
    print errormsg
    print '    varout: Different number of values in each range!'
    print '    varout: initial range: ' + optsIrange
    print '    varout: ending range: ' + optsErange
    print errormsg
    quit(-1)
  else:
    ndims=len(irange)
 
  if not os.path.isfile(ncfile):
    print errormsg
    print '    varout: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'r')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    varout: File "' + ncfile + '" does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  var = ncf.variables[varn]

  varshape = var.shape
  Nvarshape = len(varshape)
 
  if not Nvarshape == ndims:
    print errormsg
    print '    varout: Provided number of values of the range ' + ndims + ' is different of the shape of the variable ' + Nvarshape + ' !!!'
    print errormsg
    ncf.close()
    quit(-1)

  if Nvarshape == 1:
    varValrange = var[irange[0]:rangedim(erange[0], varshape[0])]
  elif Nvarshape == 2:
    varValrange = var[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1]), varshape[1]]
  elif Nvarshape == 3:
    varValrange = var[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2])]
  elif Nvarshape == 4:
    varValrange = var[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2]),      \
      irange[3]:rangedim(erange[3], varshape[3])]
  elif Nvarshape == 5:
    varValrange = var[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2]),      \
      irange[3]:rangedim(erange[3], varshape[3]), irange[4]:rangedim(erange[4], varshape[4])]
  elif Nvarshape == 6:
    varValrange = var[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2]),      \
      irange[3]:rangedim(erange[3], varshape[3]), irange[4]:rangedim(erange[4], varshape[4]),      \
      irange[5]:rangedim(erange[5], varshape[5])]

  ncf.close()

  Nshaperange = len(varValrange.shape)
  Noneshape = 0
  for ir in range(Nshaperange):
    if varValrange.shape[ir] == 1:
      Noneshape = Noneshape + 1

  if Noneshape == Nshaperange - 1:
    for i in range(len(varValrange)):
      print '%2s %f' % ( 'NC', varValrange[i] )

def chdimname(values, ncfile, varn):
  """ Changing the name of the dimension
  values = [olddimname]:[newdimname]
    [olddimname]: old name of the dimension
    [newdimname]: new name of the dimension
  ncfile = netCDF file name
  varn = variable name
  """

  if not os.path.isfile(ncfile):
    print errormsg
    print '    chdimname: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  olddimname=values.split(':')[0]
  newdimname=values.split(':')[1]

  if not ncf.dimensions.has_key(olddimname):
    
    if ((olddimname=='gsize') or (olddimname=='nb2')):
        pass
    else:
        print warnmsg
        print '    chdimname: File "' + ncfile + '" does not have dimension "' + olddimname + '" !!!!'
    ncf.close()
    quit()

  if not olddimname == newdimname and not ncf.dimensions.has_key(newdimname):
      newname = ncf.renameDimension(olddimname, newdimname)
      ncf.sync()
      ncf.close()
  else:
      print warnmsg
      print '    chdimname: File "' + ncfile + '" already has dimension name "' + newdimname + '" '
      print '    chdimname: modifying all the variables which use the old dimension'
      filevars = ncf.variables
      for fvarn in filevars:
          if ncf.variables.has_key(fvarn):
              fvar = ncf.variables[fvarn]
              fvardims = fvar.dimensions
              if searchInlist(fvardims, olddimname):
                  print '    variable "' + fvarn + '" uses dimension "' + olddimname + '" '
                  varinf = variable_inf(fvar)

                  newdims = tuple(fvardims)
                  change = {olddimname: newdimname}
# From http://stackoverflow.com/questions/9067043/python-replace-list-values-using-a-tuple
                  newdims = tuple([ change.get(x,x) for x in fvardims ])
                  newvar = ncf.createVariable(fvarn + 'tmpdname', varinf.dtype, newdims, fill_value=varinf.FillValue)
                  varv = fvar[:]
                  newvar[:] = varv

                  for attrn in varinf.attributes:
                      attrv = fvar.getncattr(attrn)
                      newar = newvar.setncattr(attrn, attrv)

                  fvar = ncf.renameVariable(fvarn, fvarn + 'rmdname')
                  ncf.sync()
                  newvar = ncf.renameVariable(fvarn + 'tmpdname', fvarn)
                  ncf.sync()
                  ncf.close()
                  varrm(ncfile, fvarn + 'rmdname')
                  ncf = NetCDFFile(ncfile,'a')

def chvarname(values, ncfile, varn):
  """Changing the name of the variable
  values = new variable name
  ncfile = netCDF file
  varn = name of the variable
  """

  if not os.path.isfile(ncfile):
    print errormsg
    print '    chvarname: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print warnmsg
    print '    chvarname: File does not have variable "' + varn + '" !!!!'
    ncf.close()
    quit(-1)

  if not varn == values and not ncf.variables.has_key(values):
    newname = ncf.renameVariable(varn, values)

  ncf.sync()
  ncf.close()

def searchInlist(listname, nameFind):
    """ Function to search a value within a list
    listname = list
    nameFind = value to find
    >>> searInlist(['1', '2', '3', '5'], '5')
    True
    """
    for x in listname:
      if x == nameFind:
        return True
    return False

def set_attribute(ncvar, attrname, attrvalue):
    """ Sets a value of an attribute of a netCDF variable. Removes previous attribute value if exists
    ncvar = object netcdf variable
    attrname = name of the attribute
    attrvalue = value of the attribute
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    attvar = ncvar.ncattrs()
    if searchInlist(attvar, attrname):
        attr = ncvar.delncattr(attrname)

    attr = ncvar.setncattr(attrname, attrvalue)

    return ncvar

def set_attributek(ncvar, attrname, attrval, attrkind):
    """ Sets a value of an attribute of a netCDF variable with a kind. Removes previous attribute value if exists
    ncvar = object netcdf variable
    attrname = name of the attribute
    attrvalue = value of the attribute
    atrtrkind = kind of attribute: 'S', string ('!' as spaces); 'I', integer; 'R', real; 'D', double
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    if attrkind == 'S':
        attrvalue = str(attrval.replace('!', ' '))
    elif attrkind == 'I':
        attrvalue = int(attrval)
    elif attrkind == 'R':
        attrvalue = float(attrval)
    elif attrkind == 'D':
        attrvalue = np.float64(attrval)
    else:
        print errormsg
        print '    set_attributek: "' + attrkind + '" kind of attribute is not ready!'
        quit(-1)

    attvar = ncvar.ncattrs()
    if searchInlist(attvar, attrname):
        attr = ncvar.delncattr(attrname)
    attr = ncvar.setncattr(attrname, attrvalue)

    return ncvar

def basicvardef(varobj, vstname, vlname, vunits):
    """ Function to give the basic attributes to a variable
    varobj= netCDF variable object
    vstname= standard name of the variable
    vlname= long name of the variable
    vunits= units of the variable
    """
    attr = varobj.setncattr('standard_name', vstname)
    attr = varobj.setncattr('long_name', vlname)
    attr = varobj.setncattr('units', vunits)

def gaddattr(values, ncfile):
  """ Add a global attribute to a netCDF. Removes previous attribute if it exist
  values = [attrname]|[attrvalue ('!' as spaces)]
  ncfile = netCDF file
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    gaddattr: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  attrvals=values.split('|')
  attrn=attrvals[0]
  attrv=attrvals[1].replace('!', ' ')

  ncf = set_attribute(ncf, attrn, attrv)

  ncf.sync()
  ncf.close()

def gaddattrk(values, ncfile):
  """ Add a global attribute to a netCDF caring about the type. Removes previous attribute if it exist
  values = [attrname]|[attrvalue]|[attrk]
    attrname = name of the attribute
    attrvalue = value of the attribute
    attrk = 'S', string ('!' as spaces); 'I', integer; 'R', real; 'D', double
  ncfile = netCDF file
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    gaddattrk: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  attrvals=values.split('|')
  attrn=attrvals[0]
  attrv0=attrvals[1]
  attrk=attrvals[2]

  if attrk == 'S':
    attrv = str(attrv0.replace('!', ' '))
  elif attrk == 'I':
    attrv = int(attrv0)
  elif attrk == 'R':
    attrv = float(attrv0)
  elif attrk == 'D':
    attrv = np.float64(attrv0)
  else:
    print errormsg
    print '    gaddattrk: "' + attrk + '" kind of attribute is not ready!'
    ncf.close()
    quit(-1)

  ncf = set_attribute(ncf, attrn, attrv)

  ncf.sync()
  ncf.close()

def varaddattr(values, ncfile, varn):
  """ Add an attribute to a variable. Removes previous attribute if it exists
  values = [attrname]|[attrvalue('!' as spaces)]
  ncfile = netCDF file name
  varn = name of the variable
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    varaddattr: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    varaddattr: File "' + ncfile + '" does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  attrvals=values.split('|')
  attrn=attrvals[0]
  attrv=attrvals[1].replace('!', ' ')

  var = ncf.variables[varn]
  var = set_attribute(var, attrn, attrv)

  ncf.sync()
  ncf.close()

def varaddattrk(values, ncfile, varn):
  """ Add an attribute to a variable caring about the type
  values = [attrname]|[attrvalue]|[attrk]
    attrname = name of the attribute
    attrvalue = value of the attribute
    attrk = 'S', string ('!' as spaces); 'I', integer; 'R', real; 'D', double
  ncfile = netCDF file
  varn = variable name
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    varaddattrk: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    varaddattrk: File "' + ncfile + '"does not have variable ' + varn + ' !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  attrvals=values.split('|')
  attrn=attrvals[0]
  attrv0=attrvals[1]
  attrk=attrvals[2]

  var = ncf.variables[varn]
  if attrk == 'S':
    attrv = str(attrv0.replace('!', ' '))
  elif attrk == 'I':
    attrv = int(attrv0)
  elif attrk == 'R':
    attrv = float(attrv0)
  elif attrk == 'D':
    attrv = np.float64(attrv0)
  else:
    print errormsg
    print '    varaddattrk: "' + attrk + '" kind of attribute is not ready!'
    ncf.close()
    quit(-1)

  var = set_attribute(var, attrn, attrv)

  ncf.sync()
  ncf.close()

def varrmattr(values, ncfile, varn):
  """ Removing an attribute from a variable
  values = attribute name
  ncfile = netCDF file name
  varn = variable name
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    varrmattr: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    varrmattr: File "' + ncfile + '" does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  var = ncf.variables[varn]

  attvar = var.ncattrs()
  if searchInlist(attvar, values):
      attr = var.delncattr(values)
  else:
      print warnmsg
      print '    varrmattr: "' + varn + '" does not have attribute: ' + values

  ncf.sync()
  ncf.close()

def grmattr(values, ncfile):
  """ Removing a global attribute
  values = attribute name
  ncfile = netCDF file
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    grmattr: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')

  attvar = ncf.ncattrs()
  if searchInlist(attvar, values):
      attr = ncf.delncattr(values)
  else:
      print warnmsg
      print '  grmattr: "' + ncfile + '" does not have attribute: ' + values

  ncf.sync()
  ncf.close()

def fvaradd(values, ncfile):
  """ Adding variable (and all its attributes and dimensions) from a reference file to a file
  values = [netCDFref]:[varnref]
    netCDFref = netCDF file name as reference for the variable to add
    varnref = name of the variable from [netCDFref] to be added
  ncfile = netCDF file name
  """

  refnc = values.split(':')[0]
  refvar = values.split(':')[1]

  if not os.path.isfile(ncfile):
    print errormsg
    print '    fvaradd: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  if not os.path.isfile(refnc):
    print errormsg
    print '    fvaradd: Reference file "' + refnc + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  refvars = ncref.variables
  if searchInlist(refvars, refvar):
      refvarv = ncref.variables[refvar]
  else:
      print errormsg
      print '    fvaradd: File "' + refnc + '" does not have variable: ' + refvar
      ncf.close()
      ncref.close()
      quit(-1)

  vardims = refvarv.dimensions
  vartype = refvarv.dtype
  varattr = refvarv.ncattrs()

# Checking dimensions
##
  newdims = ncf.dimensions
  for rdim in vardims:
      if not searchInlist(newdims, rdim):
          print '      fvaradd: Adding dimension ' + rdim
          ncf.close()
          ncref.close()
          fdimadd(refnc + ':' + rdim, ncfile)
          ncf = NetCDFFile(ncfile,'a')
          ncref = NetCDFFile(refnc,'r')

# Checking fill value
## 
  if searchInlist(varattr, '_FillValue'):
      varfil = refvarv._FillValue
  else:
      varfil = False

  print '      fvaradd: Adding refvar:', refvar, 'shape: ', refvarv.shape
  var = ncf.createVariable(refvar, vartype, vardims, fill_value=varfil)

  varshape = refvarv.shape
  Nvars = len(varshape)
   
  if not Nvars == 0:
      if Nvars == 1:
          varvals = np.zeros(varshape[0], dtype=vartype) 
      elif Nvars == 2:
          varvals = np.zeros((varshape[0], varshape[1]), dtype=vartype)
      elif Nvars == 3:
          varvals = np.zeros((varshape[0], varshape[1], varshape[2]), dtype=vartype)
      elif Nvars == 4:
          varvals = np.zeros((varshape[0], varshape[1], varshape[2], varshape[3]), dtype=vartype) 
      elif Nvars == 5:
          varvals = np.zeros((varshape[0], varshape[1], varshape[2], varshape[3], varshape[4]), dtype=vartype)
      elif Nvars == 6:
          varvals = np.zeros((varshape[0], varshape[1], varshape[2], varshape[3], varshape[4], varshape[5]), dtype=vartype)
      else:
          print errormsg
          print '  fvaradd: variable size ',Nvars,' is not ready!!!!'

# Allocating all the necessary memory (just in case)
      var[:] = varvals

      if Nvars <= 2:
          varvals = refvarv[:]
          var[:] = varvals
      elif Nvars == 3:
          for i in range(varshape[0]):
              varvals[i,:,:] = refvarv[i,:,:]
              var[i,:,:] = varvals[i,:,:]
      elif Nvars == 4:
	  for i in range(varshape[0]):
              for j in range(varshape[1]):
                  varvals[i,j,:,:] = refvarv[i,j,:,:]
                  var[i,j,:,:] = varvals[i,j,:,:]
      elif Nvars == 5:
	  for i in range(varshape[0]):
              for j in range(varshape[1]):
                  for k in range(varshape[2]):
                      varvals[i,j,k,:,:] = refvarv[i,j,k,:,:]
                      var[i,j,k,:,:] = varvals[i,j,k,:,:]
      elif Nvars == 6:
	  for i in range(varshape[0]):
              for j in range(varshape[1]):
                  for k in range(varshape[2]):
                      for l in range(varshape[3]):
                          varvals[i,j,k,l,:,:] = refvarv[i,j,k,l,:,:]
                          var[i,j,k,l,:,:] = varvals[i,j,k,l,:,:]

  newvar = ncf.variables[refvar]
  for attr in varattr:
      newvarattrs = newvar.ncattrs()
      attrv = refvarv.getncattr(attr)
      if not searchInlist(newvarattrs, attr):     
          newvar.setncattr(attr, attrv)

  ncf.sync()
  ncf.close()
  ncref.close()

def fdimadd(values, ncfile):
  """ Adding dimension from another reference file 
  values = [refnc]:[refdim]
    refnc = netCDF file name as reference
    refdim = name of the dimension to be added
  ncfile = netCDF file name
  """

  refnc = values.split(':')[0]
  refdim = values.split(':')[1]

  if not os.path.isfile(ncfile):
    print errormsg
    print '    fdimadd: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  if not os.path.isfile(refnc):
    print errormsg
    print '    fdimadd: Reference file "' + refnc + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  refdims = ncref.dimensions
  if searchInlist(refdims, refdim):
      refdimv = ncref.dimensions[refdim]
  else:
      print errormsg
      print '    fdimadd: File "' + refnc + '" does not have dimension: "' + refdim + '"'
      ncf.close()
      ncref.close()
      quit(-1)

  if refdimv.isunlimited():
      print '      fdimadd: Unlimited dimension '
      dimsize = None
  else:
      dimsize = len(refdimv)

  print '      fdimadd: Adding refdim:', refdim, 'size:', dimsize
  dim = ncf.createDimension(refdim, dimsize)
  
  ncf.sync()
  ncf.close()
  ncref.close()

def fattradd(var, values, ncfile):
  """ Adding attributes from a reference file
  var = variable to which has to be added the attribute
  values = [refnc]:[refvar]
    refnc = netCDF file name as reference
    refvar = variable from the reference file
  ncfile = netCDF file name
  """

  refnc = values.split(':')[0]
  refvar = values.split(':')[1]

  if not os.path.isfile(ncfile):
    print errormsg
    print '    fattradd: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  if not os.path.isfile(refnc):
    print errormsg
    print '    fattradd: Reference file "' + refnc + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  vars = ncf.variables
  if searchInlist(vars, var):
      varv = ncf.variables[var]
  else:
      print '  fattradd: File "' + ncfile + '" does not have variable: "' + var + '"'
      ncf.close()
      ncref.close()
      quit(-1)

  refvars = ncref.variables
  if searchInlist(refvars, refvar):
      refvarv = ncref.variables[refvar]
  else:
      print '    fattradd: File "' + refnc + '" does not have variable: "' + refvar + '"'
      ncf.close()
      ncref.close()
      quit(-1)

  refvarattrs = refvarv.ncattrs()
  Nattrs = len(refvarattrs)
  print '      fattradd: Adding ', Nattrs,' atributes from:', refvar

  for attr in refvarattrs:
      attrv = refvarv.getncattr(attr)
      atvar = set_attribute(varv, attr, attrv)
  
  ncf.sync()
  ncf.close()
  ncref.close()

def fgaddattr(values, ncfile):
  """ Adding global attributes from a reference file
  values = netCDF file name as reference
  ncfile = netCDF file name
  """

  refnc = values

  if not os.path.isfile(ncfile):
    print errormsg
    print '    fgaddattr: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  if not os.path.isfile(refnc):
    print errormsg
    print '    fgaddattr: Reference file "' + refnc + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  refgattrs = ncref.ncattrs()
  Nattrs = len(refgattrs)
  print '      fgaddattr: Adding ', Nattrs,' global atributes'

  for attr in refgattrs:
      attrv = ncref.getncattr(attr)
      atvar = set_attribute(ncf, attr, attrv)
  
  ncf.sync()
  ncf.close()
  ncref.close()

def varrm(ncfile, var):
  """ Removing a variable from a file
  ncfile = netCDF object file
  var = variable name to remove
  """
  import shutil as shu

  if not os.path.isfile(ncfile):
    print errormsg
    print '    varrm: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')
  ncvars = ncf.variables
  ncf.close()

  if not searchInlist(ncvars, var):
      print '    varrm: File "' + ncfile + '" does not have variable: "' + var + '"'
      ncf.close()
      quit(-1)

  tmpncf = NetCDFFile('tmp_py.nc' , 'w')
  gtmpattr = set_attribute(tmpncf, 'copy', 'temporal')
  tmpncf.sync()
  tmpncf.close()

  for varn in ncvars:
      if not varn == var:
           fvaradd(ncfile + ':' + varn, 'tmp_py.nc')

  fgaddattr(ncfile, 'tmp_py.nc')
  shu.copyfile('tmp_py.nc', ncfile)
  os.remove('tmp_py.nc')
 
def ivars(ncfile):
  """Give all the variable names of a file
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    ivars: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'a')
  ncvars = ncf.variables
  allvars=''
  for var in ncvars:
      print var
      allvars=allvars + ':' + var

  print '  # allvars= ' + allvars
  ncf.close()

def igattrs(ncfile):
  """Give all the global attributes of a file
  """
  if not os.path.isfile(ncfile):
    print errormsg
    print '    igattrs: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'r')
  ncattrs = ncf.ncattrs()
  allattrs=''
  allattrsdic = {}
  for attr in ncattrs:
      attrv=ncf.getncattr(attr)
      if type(attrv) == type(str('1')):
          attrk = 'S'
      elif type(attrv) == type(unicode('1')):
          attrk = 'S'
      elif type(attrv) == type(int(1)):
          attrk = 'I'
      elif type(attrv) == type(np.int(1)):
          attrk = 'I'
      elif type(attrv) == type(np.int32(1)):
          attrk = 'I'
      elif type(attrv) == type(float(1.)):
          attrk = 'R'
      elif type(attrv) == type(np.float32(1.)):
          attrk = 'R'
      elif type(attrv) == type(np.float64(1.)):
          attrk = 'D'
      else:
          print errormsg
          print '    igattr: Reading attribute "', type(attrv), '" not ready! value:', attrv
          ncf.close()
          quit(-1)
      print attr, '|',  attrv, '|', attrk
      allattrsdic[attr] = [attrv, attrk]
      allattrs=allattrs + ':' + attr + '|' + str(attrv) + '|' + attrk

  print '####### ###### ##### #### ### ## #'
  print '# allgattrs= ' + allattrs
  ncf.close()

  return allattrsdic

def isgattrs(values, ncfile):
  """Give a single global attribute of a file and its type
  values = attribute name
  ncfile = netCDF file name
  output:
    attribute name, '|',  attribute value, '|', attribute kind ('S', string '!' as spaces; 'I', integer; 'R', real; 'D', double )
  """

  if not os.path.isfile(ncfile):
    print errormsg
    print '    isgattrs: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'r')
  ncattrs = ncf.ncattrs()

  if not searchInlist(ncattrs, values):
      print '    isgattrs: File "' + ncfile + '" does not have global attribute: "' + values + '" '
      ncf.close()
      quit(-1)

  attrv=ncf.getncattr(values)
  if type(attrv) == type(str('1')):
      attrk = 'S'
      attrv = attrv.replace(' ','!')
  elif type(attrv) == type(unicode('1')):
      attrk = 'S'
      attrv = attrv.replace(' ','!')
  elif type(attrv) == type(int(1)):
      attrk = 'I'
  elif type(attrv) == type(np.int(1)):
      attrk = 'I'
  elif type(attrv) == type(np.int32(1)):
      attrk = 'I'
  elif type(attrv) == type(float(1.)):
      attrk = 'R'
  elif type(attrv) == type(np.float32(1.)):
      attrk = 'R'
  elif type(attrv) == type(np.float64(1.)):
      attrk = 'D'
  else:
      print errormsg
      print '    isgattr: Reading attribute "', type(attrv), '" not ready! value:', attrv
      ncf.close()
      quit(-1)
  print values, '|',  attrv, '|', attrk

  ncf.close()
  return [values, attrv, attrk]

def ivattrs(ncfile, varn):
  """Give all the attributes of a variable and its type
  ncfile = netCDF file name
  var = variable name
  output:
    attribute name, '|',  attribute value, '|', attribute kind ('S', string '!' as spaces; 'I', integer; 'R', real; 'D', double )
  """

  if not os.path.isfile(ncfile):
    print errormsg
    print '    ivattrs: File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'r')
  ncvars = ncf.variables

  if not searchInlist(ncvars, varn):
      print '    ivattrs: File "' + ncfile + '" does not have variable: "' + varn + '" '
      ncf.close()
      quit(-1)
  varval = ncf.variables[varn]

  ncattrs = varval.ncattrs()
  allattrs=''
  allattrsdic = {}
  for attr in ncattrs:
      attrv=varval.getncattr(attr)
      if type(attrv) == type(str('1')):
          attrk = 'S'
          attrv = attrv.replace(' ','!')
      elif type(attrv) == type(unicode('1')):
          attrk = 'S'
          attrv = attrv.replace(' ','!')
      elif type(attrv) == type(int(1)):
          attrk = 'I'
      elif type(attrv) == type(np.int(1)):
          attrk = 'I'
      elif type(attrv) == type(np.int32(1)):
          attrk = 'I'
      elif type(attrv) == type(float(1.)):
          attrk = 'R'
      elif type(attrv) == type(np.float32(1.)):
          attrk = 'R'
      elif type(attrv) == type(np.float64(1.)):
          attrk = 'D'
      else:
          print errormsg
          print '    ivattrs: Reading attribute "', type(attrv), '" not ready! value:', attrv
          ncf.close()
          quit(-1)
      print attr, '|',  attrv, '|', attrk
      allattrsdic[attr]  = [attrv, attrk]
      allattrs=allattrs + ':' + attr + '|' + str(attrv) + '|' + attrk

  print '####### ###### ##### #### ### ## #'
  print '# allvattrs= ' + allattrs
  ncf.close()

  return allattrsdic

def isvattrs(values, ncfile, varn):
  """Give a single attribute of a variable
  values = attribute name
  ncfile = netCDF file name
  varn = variable name
  """

  if not os.path.isfile(ncfile):
    print errormsg
    print '    isvattrs:File "' + ncfile + '" does not exist !!'
    print errormsg
    quit(-1)    

  ncf = NetCDFFile(ncfile,'r')
  ncvars = ncf.variables

  if not searchInlist(ncvars, varn):
      print errormsg
      print '    isvattrs:"' + ncfile + '" does not have variable: "' + varn + '" '
      ncf.close()
      quit(-1)
  varval = ncf.variables[varn]

  ncattrs = varval.ncattrs()
  if not searchInlist(ncattrs, values):
      print errormsg
      print '    isvattrs:' + ncfile + ' does not have global attribute: "' + values + '" '
      ncf.close()
      quit(-1)

  attrv=varval.getncattr(values)
  if type(attrv) == type(str('1')):
      attrk = 'S'
  elif type(attrv) == type(unicode('1')):
      attrk = 'S'
  elif type(attrv) == type(int(1)):
      attrk = 'I'
  elif type(attrv) == type(np.int(1)):
      attrk = 'I'
  elif type(attrv) == type(np.int32(1)):
      attrk = 'I'
  elif type(attrv) == type(float(1.)):
      attrk = 'R'
  elif type(attrv) == type(np.float(1.)):
      attrk = 'R'
  elif type(attrv) == type(np.float32(1.)):
      attrk = 'R'
  elif type(attrv) == type(np.float64(1.)):
      attrk = 'D'
  else:
      print errormsg
      print '    isvattr: Reading attribute "', type(attrv), '" not ready! value:', attrv
      ncf.close()
      quit(-1)
  print values, '|',  attrv, '|', attrk

  ncf.close()
  return [values, attrv, attrk]

def grattr(values, ncfile):
  """ Function to read a global atribute
  values = attribute name
  ncfile = netCDF file name
  """
  ncf = NetCDFFile(ncfile,'r')

  glob_attrs = ncf.ncattrs()

  attrPos = searchInlist(glob_attrs, values)

  if not attrPos:
    print errormsg
    print '    grattr: File "' + ncfile + '" does not have attribute "' + values + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  print ncf.getncattr(values)

  ncf.close()

def vrattr(values, ncfile, varn):
  """ Function to remove an atribute from a variable
  values = attribute name
  ncfile = netCDF file name
  varn = variable name
  """
  ncf = NetCDFFile(ncfile,'r')

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    vrattr: File "' + ncfile + '" does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  var = ncf.variables[varn]
  var_attrs = var.ncattrs()

  attrPos = searchInlist(var_attrs, values)

  if not attrPos:
    print errormsg
    print '   vrattr: Variable "' + varn + '" does not have attribute "' + values + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  print var.getncattr(values)

  ncf.close()

def datetimeStr_datetime(StringDT):
  """ Function to transform a string date ([YYYY]-[MM]-[DD]_[HH]:[MI]:[SS] format ) to a date object
  >>> datetimeStr_datetime('1976-02-17_00:00:00')
  1976-02-17 00:00:00
  """
  import datetime as dt

  dateDT = StringDT.split('_')
  dateD = dateDT[0].split('-')
  timeT = dateDT[1].split(':')

  if int(dateD[0]) == 0:
    print warnmsg
    print '    datetimeStr_datetime: 0 reference year!! changing to 1'
    dateD[0] = 1 

  if len(timeT) == 3:
    newdatetime = dt.datetime(int(dateD[0]), int(dateD[1]), int(dateD[2]), int(timeT[0]), int(timeT[1]), int(timeT[2]))
  else:
    newdatetime = dt.datetime(int(dateD[0]), int(dateD[1]), int(dateD[2]), int(timeT[0]), int(timeT[1]), 0)

  return newdatetime

def dateStr_date(StringDate):
  """ Function to transform a string date ([YYYY]-[MM]-[DD] format) to a date object
  >>> dateStr_date('1976-02-17')
  1976-02-17
  """
  import datetime as dt

  dateD = StringDate.split('-')
  if int(dateD[0]) == 0:
    print warnmsg
    print '    dateStr_date: 0 reference year!! changing to 1'
    dateD[0] = 1
  newdate = dt.date(int(dateD[0]), int(dateD[1]), int(dateD[2]))
  return newdate

def timeStr_time(StringDate):
  """ Function to transform a string date ([HH]:[MI]:[SS] format) to a time object
  >>> datetimeStr_datetime('04:32:54')
  04:32:54
  """
  import datetime as dt

  timeT = StringDate.split(':')
  if len(timeT) == 3:
    newtime = dt.time(int(timeT[0]), int(timeT[1]), int(timeT[2]))
  else:
    newtime = dt.time(int(timeT[0]), int(timeT[1]), 0)

  return newtime

def timeref_datetime(refd, timeval, tu):
    """ Function to transform from a [timeval] in [tu] units from the time referece [tref] to datetime object
    refd: time of reference (as datetime object)
    timeval: time value (as [tu] from [tref])
    tu: time units
    >>> timeref = date(1949,12,1,0,0,0)
    >>> timeref_datetime(timeref, 229784.36, hours)
    1976-02-17 08:21:36
    """
    import datetime as dt
    import numpy as np

## Not in timedelta
#    if tu == 'years':
#        realdate = refdate + dt.timedelta(years=float(timeval))
#    elif tu == 'months':
#        realdate = refdate + dt.timedelta(months=float(timeval))
    if tu == 'weeks':
        realdate = refd + dt.timedelta(weeks=float(timeval))
    elif tu == 'days':
        realdate = refd + dt.timedelta(days=float(timeval))
    elif tu == 'hours':
        realdate = refd + dt.timedelta(hours=float(timeval))
    elif tu == 'minutes':
        realdate = refd + dt.timedelta(minutes=float(timeval))
    elif tu == 'seconds':
        realdate = refd + dt.timedelta(seconds=float(timeval))
    elif tu == 'milliseconds':
        realdate = refd + dt.timedelta(milliseconds=float(timeval))
    else:
          print errormsg
          print '    timeref_datetime: time units "' + tu + '" not ready!!!!'
          quit(-1)

    return realdate

def timeref_datetime_mat(refd, timeval, tu):
    """ Function to transform from a [timeval] in [tu] units from the time referece [tref] to matrix with: year, day, month, hour, minute, second
    refd: time of reference (as datetime object)
    timeval: time value (as [tu] from [tref])
    tu: time units
    >>> timeref = date(1949,12,1,0,0,0)
    >>> timeref_datetime(timeref, 229784.36, hours)
    [1976    2   17    8   36   21]
    """
    import datetime as dt
    import numpy as np


    realdates = np.zeros(6, dtype=int)
## Not in timedelta
#    if tu == 'years':
#        realdate = refdate + dt.timedelta(years=float(timeval))
#    elif tu == 'months':
#        realdate = refdate + dt.timedelta(months=float(timeval))
    if tu == 'weeks':
        realdate = refd + dt.timedelta(weeks=float(timeval))
    elif tu == 'days':
        realdate = refd + dt.timedelta(days=float(timeval))
    elif tu == 'hours':
        realdate = refd + dt.timedelta(hours=float(timeval))
    elif tu == 'minutes':
        realdate = refd + dt.timedelta(minutes=float(timeval))
    elif tunits == 'seconds':
        realdate = refd + dt.timedelta(seconds=float(timeval))
    elif tunits == 'milliseconds':
        realdate = refd + dt.timedelta(milliseconds=float(timeval))
    else:
          print errormsg
          print '    timeref_datetime: time units "' + tu + '" not ready!!!!'
          quit(-1)

    realdates[0] = int(realdate.year)
    realdates[1] = int(realdate.month)
    realdates[2] = int(realdate.day)
    realdates[3] = int(realdate.hour)
    realdates[4] = int(realdate.second)
    realdates[5] = int(realdate.minute)

    return realdates

class cls_time_information(object):
    """ Classs to provide information about variable time
    ncfu = netCDF unit name
    tname = name of the variable time in [ncfu]
    self.calendar: calendar of the variable time
    self.unitsval: units value as it appears in attributes
    self.units: units of variable time
    self.Urefdate: reference date as it appears in the original 'units' section
    self.Srefdate: reference date as string [YYYY][MM][DD][HH][MI][SS]
    self.refdate: reference date
    self.attributes: attributes of time variable
    self.dimt: dimension of the time variable
    self.dt: distance between first two time-steps in self.tunits
    self.firstTu: first time value in self.units
    self.firstTt: first time value as datetime object
    self.firstTS: first time value as string [YYYY][MM][DD][HH][MI][SS]
    self.firstTm: first time value as matrix (from datetime; [Y], [M], [D], [H], [M], [S])
    self.lastTu: last time value in self.units
    self.firstTt: last time value as datetime object
    self.lastTS: last time value as string [YYYY][MM][DD][HH][MI][SS]
    self.lastTm: last time value as matrix (from datetime; [Y], [M], [D], [H], [M], [S])
    """

    import datetime as dt 

    def  __init__(self, ncfu, tname):

        if ncfu is None:
            self.unitsval = None
            self.units = None
            self.calendar = None
            self.Urefdate = None
            self.Srefdate = None
            self.refdate = None
            self.attributes = None
            self.dimt = None
            self.dt = None
            self.firstTu = None
            self.firstTt = None
            self.firstTS = None
            self.firstTm = None
            self.lastTu = None
            self.lastTt = None
            self.lastTS = None
            self.lastTm = None
        else:
            times = ncfu.variables[tname]
            attvar = times.ncattrs() 
            self.attributes = attvar
            if not searchInlist(attvar, 'units'):
                print errormsg
                print '    cls_time_information: time variable "', tname, '" does not have attribute: "units"'
                quit(-1)
            else:
                units = times.getncattr('units')

            self.unitsval = units

            if not searchInlist(attvar, 'calendar'):
                print warnmsg
                print '    cls_time_information: time variable "', tname, '" does not have attribute: "calendar"'
                self.calendar = '-'
            else:
                self.calendar = times.getncattr('calendar')
  
            txtunits = units.split(' ')
            self.units = txtunits[0]
            Srefdate = txtunits[len(txtunits) - 1]
# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
            timeval = Srefdate.find(':')

            if not timeval == -1:
#        print '  refdate with time!'
                self.refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
                self.Urefdate = txtunits[len(txtunits) - 2] + ' ' + Srefdate
            else:
                self.refdate = datetimeStr_datetime(Srefdate + '_00:00:00')
                self.Urefdate = Srefdate

            self.Srefdate = self.refdate.strftime("%Y%m%d%H%M%S")
            timev = times[:]

            self.dimt = times.shape[0]
            self.dt = timev[1]-timev[0]
            self.firstTu = timev[0]
            self.firstTt = timeref_datetime(self.refdate, self.firstTu, self.units)
            self.firstTS = self.firstTt.strftime("%Y%m%d%H%M%S")
            self.firstTm = timeref_datetime_mat(self.refdate, self.firstTu, self.units)
            self.lastTu = timev[self.dimt-1]
            self.lastTt = timeref_datetime(self.refdate, self.lastTu, self.units)
            self.lastTS = self.lastTt.strftime("%Y%m%d%H%M%S")
            self.lastTm = timeref_datetime_mat(self.refdate, self.lastTu, self.units)

def time_information(ncfu, tname):
    """ Function to provide information about variable time
    ncfu = netCDF unit name
    tname = name of the variable time in [ncfu]
    """
    times = ncfu.variables[tname]
    timeinf = []

    attvar = times.ncattrs()
    if not searchInlist(attvar, 'units'):
        print errormsg
        print '    "time" does not have attribute: "units"'
        quit(-1)
    else:
        units = times.getncattr('units')
  
    txtunits = units.split(' ')
    tunits = txtunits[0]
    Srefdate = txtunits[len(txtunits) - 1]
# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
    timeval = Srefdate.find(':')

    if not timeval == -1:
#        print '  refdate with time!'
        refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
    else:
        refdate = dateStr_date(Srefdate)

    timeinf.append(tunits)
    timeinf.append(Srefdate)
    timeinf.append(refdate)

    return timeinf

def CFtimes_datetime(ncfile, tname):
    """ Provide date/time array from a file with a series of netCDF CF-compilant time variable
    ncfile = netCDF file name
    tname = name of the variable time in [ncfile]
    output:
      array(dimt, 0) = year
      array(dimt, 1) = month
      array(dimt, 2) = day
      array(dimt, 3) = hour
      array(dimt, 4) = minute
      array(dimt, 5) = second
    """
    import datetime as dt

    times = ncfile.variables[tname]
    timevals = times[:]

    attvar = times.ncattrs()
    if not searchInlist(attvar, 'units'):
        print errormsg
        print '    CFtimes_datetime: "time" does not have attribute: "units"'
        quit(-1)
    else:
        units = times.getncattr('units')
  
    txtunits = units.split(' ')
    tunits = txtunits[0]
    Srefdate = txtunits[len(txtunits) - 1]
# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
    timeval = Srefdate.find(':')

    if not timeval == -1:
#        print '  refdate with time!'
        refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
    else:
        refdate = dateStr_date(Srefdate)

    dimt = len(timevals)
    realdates = np.zeros((dimt, 6), dtype=int)
    print realdates.shape

## Not in timedelta
#    if tunits == 'years':
#        for it in range(dimt):
#            realdate = refdate + dt.timedelta(years=float(times[it]))
#            realdates[it] = int(realdate.year)
#    elif tunits == 'months':
#        for it in range(dimt):
#            realdate = refdate + dt.timedelta(months=float(times[it]))
#            realdates[it] = int(realdate.year)
    if tunits == 'weeks':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(weeks=float(times[it]))
            realdates[it,0] = int(realdate.year)
            realdates[it,1] = int(realdate.month)
            realdates[it,2] = int(realdate.day)
            realdates[it,3] = int(realdate.hour)
            realdates[it,4] = int(realdate.second)
            realdates[it,5] = int(realdate.minute)
    elif tunits == 'days':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(days=float(times[it]))
            realdates[it,0] = int(realdate.year)
            realdates[it,1] = int(realdate.month)
            realdates[it,2] = int(realdate.day)
            realdates[it,3] = int(realdate.hour)
            realdates[it,4] = int(realdate.second)
            realdates[it,5] = int(realdate.minute)
    elif tunits == 'hours':
       for it in range(dimt):
            realdate = refdate + dt.timedelta(hours=float(times[it]))
            realdates[it,0] = int(realdate.year)
            realdates[it,1] = int(realdate.month)
            realdates[it,2] = int(realdate.day)
            realdates[it,3] = int(realdate.hour)
            realdates[it,4] = int(realdate.second)
            realdates[it,5] = int(realdate.minute)
    elif tunits == 'minutes':
       for it in range(dimt):
            realdate = refdate + dt.timedelta(minutes=float(times[it]))
            realdates[it,0] = int(realdate.year)
            realdates[it,1] = int(realdate.month)
            realdates[it,2] = int(realdate.day)
            realdates[it,3] = int(realdate.hour)
            realdates[it,4] = int(realdate.second)
            realdates[it,5] = int(realdate.minute)
    elif tunits == 'seconds':
       for it in range(dimt):
            realdate = refdate + dt.timedelta(seconds=float(times[it]))
            realdates[it,0] = int(realdate.year)
            realdates[it,1] = int(realdate.month)
            realdates[it,2] = int(realdate.day)
            realdates[it,3] = int(realdate.hour)
            realdates[it,4] = int(realdate.second)
            realdates[it,5] = int(realdate.minute)
    elif tunits == 'milliseconds':
       for it in range(dimt):
            realdate = refdate + dt.timedelta(milliseconds=float(times[it]))
            realdates[it,0] = int(realdate.year)
            realdates[it,1] = int(realdate.month)
            realdates[it,2] = int(realdate.day)
            realdates[it,3] = int(realdate.hour)
            realdates[it,4] = int(realdate.second)
            realdates[it,5] = int(realdate.minute)
    else:
          print errormsg
          print '    CFtimes_datetime: time units "' + tunits + '" not ready!!!!'
          quit(-1)

    return realdates

class variable_inf(object):
    """ Class to provide the information from a given variable
    var = object netCDF variable
    self.name: name of the variable
    self.dtype: type of the variable
    self.attributes: list with the name of attributes
    self.FillValue: value of the missing value
    self.dimns: name of the dimensions of the variable
    self.dims: dimensions of the variable
    self.Ndims: number of dimensions
    self.dimx: length of dimension along x-axis
    self.dimy: length of dimension along y-axis
    self.sname: standard name
    self.lname: long name
    self.corr: attribute 'coordinates'
    self.units: units of the variable
    """

    def __init__(self, var):

        if var is None:
            self.name = None
            self.dimns = None
            self.dims = None
            self.Ndims = None
            self.dimx = None
            self.dimy = None
            self.sname = None
            self.lname = None
            self.corr = None
            self.units = None
            self.FillValue = None
            self.dtype = None
            self.attributes = None
        else:
            self.name = var._name
            self.dimns = var.dimensions
            self.dtype = var.dtype
            self.attributes = var.ncattrs()
            self.dims = var.shape

            if searchInlist(self.attributes, 'standard_name'):
                self.sname = var.getncattr('standard_name')
            else:
                print '    variable_inf.classpy: variable "' + self.name + '" does not have attribute "standard_name"'
                self.sname = None

            if searchInlist(self.attributes, 'long_name'):
                self.lname = var.getncattr('long_name')
            else:
                print '    variable_inf.classpy: variable "' + self.name + '" does not have attribute "long_name"'
                self.lname = None

            if searchInlist(self.attributes, 'coordinates'):
                self.coor = var.getncattr('coordinates')
            else:
                print '    variable_inf.classpy: variable "' + self.name + '" does not have attribute "coordinates"'
                self.coor = None

            if searchInlist(self.attributes, 'units'):
                self.units = var.getncattr('units')
            else:
                print '    variable_inf.classpy: variable "' + self.name + '" does not have attribute "units"'
                self.units = None

            if searchInlist(self.attributes, '_FillValue'):
                self.FillValue = var.getncattr('_FillValue')
            else:
                print '    variable_inf.classpy: variable "' + self.name + '" does not have attribute "_FillValue"'
                self.FillValue = None
             
            self.Ndims = len(self.dims)
            if self.Ndims == 1:
                self.dimx=self.dims[0]
            if self.Ndims == 2:
                self.dimy=self.dims[0]
                self.dimx=self.dims[1]
            if self.Ndims == 3:
                self.dimy=self.dims[1]
                self.dimx=self.dims[2]
            if self.Ndims == 4:
                self.dimy=self.dims[2]
                self.dimx=self.dims[3]

def subyrs(values, ncfile, varn):
  """ Function to retrieve a series of years from a file
  values = 
    [year1]:[[year2]:...[yearn]] values for years [year1], [year2], ... [yearn]
    [yearI]-[yearE] values for the period between [yearI] and [yearN]
  ncfile = netCDF file name
  varn = variable name
  """
  import datetime as dt
  import calendar as cal
  ofile = 'subsetyrs.nc'

  ncf = NetCDFFile(ncfile,'r')

  if not ncf.variables.has_key(varn):
    print errormsg
    print '   subyrs: File does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  times = ncf.variables['time']
  timevals = times[:]
  realdates = CFtimes_datetime(ncf, 'time')
  dimt = len(timevals)

# Checking years
##
  isper = values.find('-')

  desiredvalues = np.array(dimt, dtype=bool)
  desiredvaluesi = np.array(dimt, dtype=bool)
  desiredvaluese = np.array(dimt, dtype=bool)

  if not isper == -1:
# Years list given as a period [YYYYi]-[YYYYf]
    print '      subyrs: There is a period of years "' + values + '"'
    iyr = int(values.split('-')[0])
    eyr = int(values.split('-')[1])
    desiredvaluesi = np.array(realdates[:,0] >= iyr)
    desiredvaluese = np.array(realdates[:,0] <= eyr)

    desiredvalues = desiredvaluesi*desiredvaluese

  else:
    yrs = values.split(':')
    nyr = 1
    for iyr in range(len(yrs)):
      desiredvaluesi = np.array(realdates[:,0] == int(yrs[iyr]))
      if nyr == 1:
        desiredvalues = desiredvaluesi
      else:
        desiredvalues = desiredvaluesi+desiredvalues
      nyr = nyr + 1

  Ndesiredvalues = len(realdates[desiredvalues])

  print '      subyrs: N values: ', Ndesiredvalues
  if Ndesiredvalues == 0:
    print errormsg
    print '    subyrs: No values found for "' + values + '"!!'
    ncf.close()
    quit(-1)

# Variable values (assuming time as first dimension)
##
  var = ncf.variables[varn]
#  varvals = var[:]
  vardims = var.shape
  varshape = len(vardims)
  print '      subyrs: Shape of data: ',varshape, ':' , vardims

  if varshape == 1:
    vardesiredvalues = np.arange(Ndesiredvalues)
    vardesiredvalues = var[desiredvalues]
  elif varshape == 2:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]).reshape(Ndesiredvalues,vardims[1])
    vardesiredvalues = var[desiredvalues, :]
  elif varshape == 3:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]).reshape(Ndesiredvalues,                        \
      vardims[1],vardims[2])
    vardesiredvalues = var[desiredvalues, :, :]
  elif varshape == 4:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]*vardims[3]).reshape(Ndesiredvalues,             \
      vardims[1],vardims[2],vardims[3])
    vardesiredvalues = var[desiredvalues, :, :, :]
  elif varshape == 5:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]*vardims[3]*vardims[4]).reshape(Ndesiredvalues,  \
      vardims[1],vardims[2],vardims[3],vardims[4])
    vardesiredvalues = var[desiredvalues, :, :, :, :]
  elif varshape == 6:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]*vardims[3]*vardims[4]*                          \
      vardims[5]).reshape(Ndesiredvalues,vardims[1],vardims[2],vardims[3],vardims[4],vardims[5])
    vardesiredvalues = var[desiredvalues, :, :, :, :, :]
  else:
     print errormsg
     print '    subyrs: ', varshape, ' shape of matrix not prepared !'
     ncf.close()
     quit(-1)

#  print '  shape of desired values: ', vardesiredvalues.shape
# Creation of file
##

  ncfo = NetCDFFile( ofile, 'w')

  vardims = var.dimensions
  vartype = var.dtype
  varattr = var.ncattrs()

# Checking dimensions
##
  newdims = ncfo.dimensions
  for rdim in vardims:
      if not searchInlist(newdims, rdim):
          if not rdim == 'time':
              print '      subyrs: Adding dimension ' + rdim
              ncfo.sync()
              ncf.close()
              ncfo.close()
              fdimadd(ncfile + ':' + rdim, ofile)
              ncf = NetCDFFile(ncfile,'r')
              ncfo = NetCDFFile(ofile,'a')
          else:
              ncfo.createDimension('time', None)

# Checking fill value
## 
  if searchInlist(varattr, '_FillValue'):
      varfil = var._FillValue
  else:
      varfil = False

  newvar = ncfo.createVariable(varn, vartype, vardims, fill_value=varfil)

  if not varshape == 0:
    newvar[:] = vardesiredvalues

  newvar = ncfo.variables[varn]
  for attr in varattr:
      newvarattrs = newvar.ncattrs()
      attrv = var.getncattr(attr)
      if not searchInlist(newvarattrs, attr):     
          newvar.setncattr(attr, attrv)

  vardims = times.dimensions
  vartype = times.dtype
  varattr = times.ncattrs()

  newvar = ncfo.createVariable('time', vartype, vardims, fill_value=varfil)
  newvar = ncfo.variables['time']
  newvar[:] = timevals[desiredvalues]

  ncf.close()
  ncfo.sync()
  ncfo.close()
  fattradd('time', ncfile + ':time', ofile)
  fvaradd(ncfile + ':lon', ofile)
  fvaradd(ncfile + ':lat', ofile)

  ncfo = NetCDFFile(ofile,'a')
  newvar = ncfo.variables['time']
  newvarattrs = newvar.ncattrs()
  if searchInlist(newvarattrs, 'bounds'):
      if newvar.getncattr('bounds') == 'time_bnds':
          ncf = NetCDFFile(ncfile,'r')
          tbnds = ncf.variables['time_bnds']
          vardims = tbnds.dimensions
          vartype = tbnds.dtype
          varattr = tbnds.ncattrs()
          ncfo.createDimension('bnds', 2)
          newvar = ncfo.createVariable('time_bnds', vartype, vardims, fill_value=varfil)
          newvar[:] = tbnds[desiredvalues,:]

          ncf.close()
          ncfo.sync()
          ncfo.close()
          fattradd('time_bnds', ncfile + ':time_bnds', ofile)
      else:
          ncfo.close()
  else:
      ncfo.close()

  fgaddattr(ncfile, ofile)

  print '      subyrs: File "' + ofile + '" with a subset of ' + values + ' has been created'

def submns(values, ncfile, varn):
  """ Function to retrieve a series of months from a file
  values = 
    [mon1]:[[mion2]:...[monn]] values for months [mon1], [mon2], ... [monn]
    [monI]-[monE] values for the period between [monI] and [monN]
  ncfile = netCDF file name
  varn = variable name
  """
  import datetime as dt
  import calendar as cal

  ofile = 'subsetmns.nc'

  ncf = NetCDFFile(ncfile,'r')

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    submns: File "' + ncfile + '" does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  times = ncf.variables['time']
  timevals = times[:]
  realdates = CFtimes_datetime(ncf, 'time')
  dimt = len(timevals)

# Checking months
##
  isper =values.find('-')

  desiredvalues = np.array(dimt, dtype=bool)
  desiredvaluesi = np.array(dimt, dtype=bool)
  desiredvaluese = np.array(dimt, dtype=bool)

  if not isper == -1:
# Months list given as a period [MMi]-[MMf]
    print '      submns: There is a period of months "' + values + '"'
    imn = int(values.split('-')[0])
    emn = int(values.split('-')[1])
    desiredvaluesi = np.array(realdates[:,1] >= imn)
    desiredvaluese = np.array(realdates[:,1] <= emn)

    desiredvalues = desiredvaluesi*desiredvaluese

  else:
    mns = values.split(':')
    nmn = 1
    for imn in range(len(mns)):
      desiredvaluesi = np.array(realdates[:,1] == int(mns[imn]))
      if nmn == 1:
        desiredvalues = desiredvaluesi
      else:
        desiredvalues = desiredvaluesi+desiredvalues
      nmn = nmn + 1

  Ndesiredvalues = len(realdates[desiredvalues])

  print '    submns: N values: ', Ndesiredvalues
  if Ndesiredvalues == 0:
    print errormsg
    print '    submns: No values found for "' + values + '"!!'
    ncf.close()
    quit(-1)

# Variable values (assuming time as first dimension)
##
  var = ncf.variables[varn]
#  varvals = var[:]
  vardims = var.shape
  varshape = len(vardims)
#  print '      submns: Shape of data: ',varshape, ':' , vardims

  if varshape == 1:
    vardesiredvalues = np.arange(Ndesiredvalues)
    vardesiredvalues = var[desiredvalues]
  elif varshape == 2:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]).reshape(Ndesiredvalues,vardims[1])
    vardesiredvalues = var[desiredvalues, :]
  elif varshape == 3:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]).reshape(Ndesiredvalues,                        \
      vardims[1],vardims[2])
    vardesiredvalues = var[desiredvalues, :, :]
  elif varshape == 4:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]*vardims[3]).reshape(Ndesiredvalues,             \
      vardims[1],vardims[2],vardims[3])
    vardesiredvalues = var[desiredvalues, :, :, :]
  elif varshape == 5:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]*vardims[3]*vardims[4]).reshape(Ndesiredvalues,  \
      vardims[1],vardims[2],vardims[3],vardims[4])
    vardesiredvalues = var[desiredvalues, :, :, :, :]
  elif varshape == 6:
    vardesiredvalues = np.arange(Ndesiredvalues*vardims[1]*vardims[2]*vardims[3]*vardims[4]*                          \
      vardims[5]).reshape(Ndesiredvalues,vardims[1],vardims[2],vardims[3],vardims[4],vardims[5])
    vardesiredvalues = var[desiredvalues, :, :, :, :, :]
  else:
     print errormsg
     print '    submns: ', varshape, ' shape of matrix not prepared !'
     ncf.close()
     quit(-1)

#  print '  shape of desired values: ', vardesiredvalues.shape
# Creation of file
##

  ncfo = NetCDFFile( ofile, 'w')

  vardims = var.dimensions
  vartype = var.dtype
  varattr = var.ncattrs()

# Checking dimensions
##
  newdims = ncfo.dimensions
  for rdim in vardims:
      if not searchInlist(newdims, rdim):
          if not rdim == 'time':
              print '      submns: Adding dimension ' + rdim
              ncfo.sync()
              ncf.close()
              ncfo.close()
              fdimadd(ncfile + ':' + rdim, ofile)
              ncf = NetCDFFile(ncfile,'r')
              ncfo = NetCDFFile(ofile,'a')
          else:
              ncfo.createDimension('time', None)

# Checking fill value
## 
  if searchInlist(varattr, '_FillValue'):
      varfil = var._FillValue
  else:
      varfil = False

  newvar = ncfo.createVariable(varn, vartype, vardims, fill_value=varfil)

  if not varshape == 0:
    newvar[:] = vardesiredvalues

  newvar = ncfo.variables[varn]
  for attr in varattr:
      newvarattrs = newvar.ncattrs()
      attrv = var.getncattr(attr)
      if not searchInlist(newvarattrs, attr):     
          newvar.setncattr(attr, attrv)

  vardims = times.dimensions
  vartype = times.dtype
  varattr = times.ncattrs()

  newvar = ncfo.createVariable('time', vartype, vardims, fill_value=varfil)
  newvar = ncfo.variables['time']
  newvar[:] = timevals[desiredvalues]

  ncf.close()
  ncfo.sync()
  ncfo.close()
  fattradd('time', ncfile + ':time', ofile)
  fvaradd(ncfile + ':lon', ofile)
  fvaradd(ncfile + ':lat', ofile)

  ncfo = NetCDFFile(ofile,'a')
  newvar = ncfo.variables['time']
  newvarattrs = newvar.ncattrs()
  if searchInlist(newvarattrs, 'bounds'):
      if newvar.getncattr('bounds') == 'time_bnds':
          ncf = NetCDFFile(ncfile,'r')
          tbnds = ncf.variables['time_bnds']
          vardims = tbnds.dimensions
          vartype = tbnds.dtype
          varattr = tbnds.ncattrs()
          ncfo.createDimension('bnds', 2)
          newvar = ncfo.createVariable('time_bnds', vartype, vardims, fill_value=varfil)
          newvar[:] = tbnds[desiredvalues,:]

          ncf.close()
          ncfo.sync()
          ncfo.close()
          fattradd('time_bnds', ncfile + ':time_bnds', ofile)
      else:
          ncfo.close()
  else:
      ncfo.close()

  fgaddattr(ncfile, ofile)

  print '      submns: File "' + ofile + '" with a subset of ' + values + ' has been created'

class statsValWeigthed(object):
  """Weigthed Statistics class providing:
  vals = values (can be a matrix)
  wgs = weights (can be a matrix)
  self.meanv: mean weigthed value
  self.mean2v: mean quadratic weigthed value
  self.stdv: weigthed standard deviation
  self.Nokvalue non None values of a list of values 
  self.meanwgt: mean of the weigths
  self.mean2wgt: cuadratic mean of the weigths
  self.stdwgt: standard deviation of the weigths
  """

  def __init__(self, vals, wgs):
    if vals is None:
      self.Nv = None
      self.meanv = None
      self.mean2v = None
      self.stdv = None
      self.Nokvalues = None
      self.meanwgt = None
      self.mean2wgt = None
      self.stdwgt = None
    else:
      values = vals.flatten() 
      weights = wgs.flatten()
      self.Nv=len(values)
      self.meanv=0.
      self.mean2v=0.
      self.stdv=0.
      self.meanwgt = 0.
      self.mean2wgt = 0.
      self.stdwgt = 0.
      self.Nokvalues = 0

      for inum in range(self.Nv):
        if not values[inum] is None:
          self.Nokvalues = self.Nokvalues + 1
          self.meanv = self.meanv+values[inum]*weights[inum]
          self.mean2v = self.mean2v+values[inum]*weights[inum]*values[inum]
          self.meanwgt = self.meanwgt+weights[inum]
          self.mean2wgt = self.mean2wgt+weights[inum]*weights[inum]

      self.meanv = self.meanv/float(self.meanwgt)
      self.mean2v = self.mean2v/float(self.meanwgt)
      self.stdv = np.sqrt(self.mean2v-self.meanv*self.meanv)
      self.meanwgt = self.meanwgt/float(self.Nokvalues)
      self.mean2wgt = self.mean2wgt/float(self.Nokvalues)
      self.stdwgt = np.sqrt(self.mean2wgt-self.meanwgt*self.meanwgt)

class statsValWeigthed_missVal(object):
  """Weigthed Statistics taking into account a missing value class providing:
  vals = values (can be a matrix)
  wgs = weights (can be a matrix)
  missVal= missing value
  self.meanv: mean weigthed value
  self.mean2v: mean quadratic weigthed value
  self.stdv: weigthed standard deviation
  self.Nokvalue non None values of a list of values 
  self.meanwgt: mean of the weigths
  self.mean2wgt: cuadratic mean of the weigths
  self.stdwgt: standard deviation of the weigths
  """

  def __init__(self, vals, wgs, missVal):

    fname='statsValWeigthed_missVal'
    if vals is None:
      self.Nv = None
      self.meanv = None
      self.mean2v = None
      self.stdv = None
      self.Nokvalues = None
      self.meanwgt = None
      self.mean2wgt = None
      self.stdwgt = None
    else:    
      Npt = 1
      for idim in range(len(vals.shape)):
        Npt = Npt * vals.shape[idim]
      if np.sum(vals >= missVal) == Npt:
          print errormsg
          print '  ' + fname + ' all values get missing!!'
          print errormsg
          quit(-1)
      vals0 = np.where(vals >= missVal, None, vals)
      values = vals0.flatten() 
      weights = wgs.flatten()
      self.Nv=Npt
      self.meanv=0.
      self.mean2v=0.
      self.stdv=0.
      self.meanwgt = 0.
      self.mean2wgt = 0.
      self.stdwgt = 0.
      self.Nokvalues = 0

      for inum in range(self.Nv):
        if not values[inum] is None:
          self.Nokvalues = self.Nokvalues + 1
          self.meanv = self.meanv+values[inum]*weights[inum]
          self.mean2v = self.mean2v+values[inum]*weights[inum]*values[inum]
          self.meanwgt = self.meanwgt+weights[inum]
          self.mean2wgt = self.mean2wgt+weights[inum]*weights[inum]

      self.meanv = self.meanv/float(self.meanwgt)
      self.mean2v = self.mean2v/float(self.meanwgt)
      self.stdv = np.sqrt(self.mean2v-self.meanv*self.meanv)
      self.meanwgt = self.meanwgt/float(self.Nokvalues)
      self.mean2wgt = self.mean2wgt/float(self.Nokvalues)
      self.stdwgt = np.sqrt(self.mean2wgt-self.meanwgt*self.meanwgt)

class stats2Val(object):
  """two variables Statistics class providing:
  vals1 = variable 1
  vals2 = variable 2
  power = power of the polynomial fitting to apply between both variables
  self.min[var], self.max[var], self.mean[var], self.mean2[var], self.std[var] of 
    [var] = var1+var2[v1Av2], var1-var2[v1Sv2], var1/var2[v1Dv2], var1*var2[v1Pv2]
  self.Nokvalues1: number of correct values of variable 1
  self.Nokvalues2: number of correct values of variable 2
  self.Nokvalues12: number of correct coincident values of variable 1 and variable 2
  self.mae=mean(abs(var1-var2)) 
  self.rmse=sqrt((var1-var2)**2) 
  self.correlation (and p-value) 
  self.linRegress: linear regression [trend, intercept, regression coefficient, p_value, standard error]
  self.polRegress: polinomial Regresion  of degree [power] [coef**[power], coef**[power-1], ...., coef**0]
  """

  def __init__(self, vals1, vals2, power):
    import numpy as np
    from scipy import stats as sts

    if vals1 is None:
      self.Nv = None
      self.Nokvalues1 = None
      self.Nokvalues2 = None
      self.Nokvalues12 = None
      self.NDvalNone = None
      self.minv1Av2 = None
      self.maxv1Av2 = None
      self.meanv1Av2 = None
      self.mean2v1Av2 = None
      self.stdv1Av2 = None
      self.minv1Sv2 = None
      self.maxv1Sv2 = None
      self.meanv1Sv2 = None
      self.mean2v1Sv2 = None
      self.stdv1Sv2 = None
      self.minv1Dv2 = None
      self.maxv1Dv2 = None
      self.meanv1Dv2 = None
      self.mean2v1Dv2 = None
      self.stdv1Dv2 = None
      self.minv1Pv2 = None
      self.maxv1Pv2 = None
      self.meanv1Pv2 = None
      self.mean2v1Pv2 = None
      self.stdv1Pv2 = None
      self.mae = None
      self.rmse = None
      self.corr = None
      self.linRegress = None
      self.polRegress = None
      self.polRegressResidual = None
      self.polRegressRes = None
      self.polRegressSingVal = None
    else:
      values1 = vals1.flatten() 
      values2 = vals2.flatten() 

      if not len(values1) == len(values2):
        print errormsg
        print '    stats2Val: lengths of variables differ!! Lvar1: ', len(values1), ' Lvar2: ',len(values2),' statistics between them can not be computed!'
        quit(-1)

      self.Nv=len(values1)
      self.minv1Av2=10000000000.
      self.maxv1Av2=-self.minv1Av2
      self.meanv1Av2=0.
      self.mean2v1Av2=0.
      self.stdv1Av2=0.
      self.minv1Sv2=self.minv1Av2
      self.maxv1Sv2=-self.minv1Av2
      self.meanv1Sv2=0.
      self.mean2v1Sv2=0.
      self.stdv1Sv2=0.
      self.minv1Dv2=self.minv1Av2
      self.maxv1Dv2=-self.minv1Av2
      self.meanv1Dv2=0.
      self.mean2v1Dv2=0.
      self.stdv1Dv2=0.
      self.minv1Pv2=self.minv1Av2
      self.maxv1Pv2=-self.minv1Av2
      self.meanv1Pv2=0.
      self.mean2v1Pv2=0.
      self.stdv1Pv2=0.
      self.mae = 0.
      self.rmse = 0.
      self.corr = np.array([0., 0.])
      self.linRegress = np.zeros(5, float)
      self.polRegress = np.zeros(power+1, float)
      self.polRegressResidual = 0.
      self.polRegressSingVal = np.zeros(power+1, float)

# v1 [+ / - / / / *] v2
##
      self.Nokvalues1 = 0
      self.Nokvalues2 = 0
      self.Nokvalues12 = 0
      self.NDvalNone = 0
      for inum in range(self.Nv):
        if not values1[inum] is None:
          self.Nokvalues1 = self.Nokvalues1 + 1
        if not values2[inum] is None:
          self.Nokvalues2 = self.Nokvalues2 + 1
        if not values1[inum] is None and not values2[inum] is None:
          self.Nokvalues12 = self.Nokvalues12 + 1
          Aval = values1[inum] + values2[inum]
          Sval = values1[inum] - values2[inum]
          Pval = values1[inum] * values2[inum]
          if np.isinf(values1[inum] / values2[inum]) or np.isnan(values1[inum] / values2[inum]):
            if self.NDvalNone < 1:
               print warnmsg
               print '      stats2Val: val1/val2 inf or Nan!!!!'
            Dval = None
            self.NDvalNone = self.NDvalNone + 1
          else:
            Dval = values1[inum] / values2[inum]

          self.mae = self.mae + abs(Sval)
          self.rmse = self.rmse + Sval**2

          if Aval < self.minv1Av2:
            self.minv1Av2 = Aval
          if Aval > self.maxv1Av2:
            self.maxv1Av2 = Aval
          if Sval < self.minv1Sv2:
            self.minv1Sv2 = Sval
          if Sval > self.maxv1Sv2:
            self.maxv1Sv2 = Sval
          if not Dval is None and Dval < self.minv1Dv2:
            self.minv1Dv2 = Dval
          if not Dval is None and  Dval > self.maxv1Dv2:
            self.maxv1Dv2 = Dval
          if Pval < self.minv1Pv2:
            self.minv1Pv2 = Pval
          if Pval > self.maxv1Pv2:
            self.maxv1Pv2 = Pval

          self.meanv1Av2 = self.meanv1Av2+Aval
          self.mean2v1Av2 = self.mean2v1Av2+Aval*Aval
          self.meanv1Sv2 = self.meanv1Sv2+Sval
          self.mean2v1Sv2 = self.mean2v1Sv2+Sval*Sval
          if not Dval is None:
            self.meanv1Dv2 = self.meanv1Dv2+Dval
            self.mean2v1Dv2 = self.mean2v1Dv2+Dval*Dval
          self.meanv1Pv2 = self.meanv1Pv2+Pval
          self.mean2v1Pv2 = self.mean2v1Pv2+Pval*Pval

##      print 'Nokvalues1: ', self.Nokvalues1, 'Nokvalues2: ', self.Nokvalues2, 'Nokvalues12: ', float(self.Nokvalues12), 'NDvalNone: ',self.NDvalNone
      self.meanv1Av2 = self.meanv1Av2/float(self.Nokvalues12)
      self.mean2v1Av2 = self.mean2v1Av2/float(self.Nokvalues12)
      self.stdv1Av2 = np.sqrt(self.mean2v1Av2-self.meanv1Av2*self.meanv1Av2)
      self.meanv1Sv2 = self.meanv1Sv2/float(self.Nokvalues12)
      self.mean2v1Sv2 = self.mean2v1Sv2/float(self.Nokvalues12)
      self.stdv1Sv2 = np.sqrt(self.mean2v1Sv2-self.meanv1Sv2*self.meanv1Sv2)
      if self.Nokvalues12 - self.NDvalNone == 0:
          self.meanv1Dv2 = None
          self.mean2v1Dv2 = None
          self.stdv1Dv2 = None
          print warnmsg
          print '      stats2Val: all values of val1/val2 are None!'
      else:
          self.meanv1Dv2 = self.meanv1Dv2/(float(self.Nokvalues12 - self.NDvalNone))
          self.mean2v1Dv2 = self.mean2v1Dv2/(float(self.Nokvalues12 - self.NDvalNone))
          self.stdv1Dv2 = np.sqrt(self.mean2v1Dv2-self.meanv1Dv2*self.meanv1Dv2)
      self.meanv1Pv2 = self.meanv1Pv2/float(self.Nokvalues12)
      self.mean2v1Pv2 = self.mean2v1Pv2/float(self.Nokvalues12)
      self.stdv1Pv2 = np.sqrt(self.mean2v1Pv2-self.meanv1Pv2*self.meanv1Pv2)

      self.mae = self.mae/self.Nokvalues12
      self.rmse = np.sqrt(self.rmse/self.Nokvalues12)

      self.corr = sts.pearsonr(values1, values2)

      self.linRegress[0], self.linRegress[1], self.linRegress[2], self.linRegress[3], self.linRegress[4] = sts.linregress(values1, values2)

      polyfitvals=np.polyfit(values1, values2, power, full = True)

      self.polRegress = polyfitvals[0]
      self.polRegressRes = polyfitvals[1]
      self.polRegressSingVal = polyfitvals[3]

class stats2Val_missVal(object):
  """two variables Statistics taking into account a missing value class providing:
  vals1 = variable 1
  vals2 = variable 2
  missVal = missing value
  power = power of the polynomial fitting to apply between both variables
  self.min[var], self.max[var], self.mean[var], self.mean2[var], self.std[var] of 
    [var] = var1+var2[v1Av2], var1-var2[v1Sv2], var1/var2[v1Dv2], var1*var2[v1Pv2]
  self.Nokvalues1: number of correct values of variable 1
  self.Nokvalues2: number of correct values of variable 2
  self.Nokvalues12: number of correct coincident values of variable 1 and variable 2
  self.mae=mean(abs(var1-var2)) 
  self.rmse=sqrt((var1-var2)**2) 
  self.correlation (and p-value) 
  self.linRegress: linear regression [trend, intercept, regression coefficient, p_value, standard error]
  self.polRegress: polinomial Regresion  of degree [power] [coef**[power], coef**[power-1], ...., coef**0]
  """

  def __init__(self, vals1, vals2, power, missVal):
    import numpy as np
    from scipy import stats as sts

    fname='stats2Val_missVal'
    if vals1 is None:
      self.Nv = None
      self.Nokvalues1 = None
      self.Nokvalues2 = None
      self.Nokvalues12 = None
      self.NDvalNone = None
      self.minv1Av2 = None
      self.maxv1Av2 = None
      self.meanv1Av2 = None
      self.mean2v1Av2 = None
      self.stdv1Av2 = None
      self.minv1Sv2 = None
      self.maxv1Sv2 = None
      self.meanv1Sv2 = None
      self.mean2v1Sv2 = None
      self.stdv1Sv2 = None
      self.minv1Dv2 = None
      self.maxv1Dv2 = None
      self.meanv1Dv2 = None
      self.mean2v1Dv2 = None
      self.stdv1Dv2 = None
      self.minv1Pv2 = None
      self.maxv1Pv2 = None
      self.meanv1Pv2 = None
      self.mean2v1Pv2 = None
      self.stdv1Pv2 = None
      self.mae = None
      self.rmse = None
      self.corr = None
      self.linRegress = None
      self.polRegress = None
      self.polRegressResidual = None
      self.polRegressRes = None
      self.polRegressSingVal = None
    else:
      Npt1 = 1
      for idim in range(len(vals1.shape)):
        Npt1 = Npt1 * vals1.shape[idim]
      if np.sum(vals1 >= missVal) == Npt1:
          print errormsg
          print '  ' + fname + ' all values 1 get missing!!'
          print errormsg
          quit(-1)
      Npt2 = 1
      for idim in range(len(vals2.shape)):
        Npt2 = Npt2 * vals2.shape[idim]
      if np.sum(vals2 >= missVal) == Npt2:
          print errormsg
          print '  ' + fname + ' all values 2 get missing!!'
          print errormsg
          quit(-1)
      vals10 = np.where(abs(vals1) >= missVal, None, vals1)
      vals20 = np.where(abs(vals2) >= missVal, None, vals2)
      values1 = vals10.flatten() 
      values2 = vals20.flatten() 

      if not len(values1) == len(values2):
        print errormsg
        print '    stats2Val: lengths of variables differ!! Lvar1: ', len(values1), ' Lvar2: ',len(values2),' statistics between them can not be computed!'
        quit(-1)

      self.Nv=Npt1
      self.minv1Av2=10000000000.
      self.maxv1Av2=-self.minv1Av2
      self.meanv1Av2=0.
      self.mean2v1Av2=0.
      self.stdv1Av2=0.
      self.minv1Sv2=self.minv1Av2
      self.maxv1Sv2=-self.minv1Av2
      self.meanv1Sv2=0.
      self.mean2v1Sv2=0.
      self.stdv1Sv2=0.
      self.minv1Dv2=self.minv1Av2
      self.maxv1Dv2=-self.minv1Av2
      self.meanv1Dv2=0.
      self.mean2v1Dv2=0.
      self.stdv1Dv2=0.
      self.minv1Pv2=self.minv1Av2
      self.maxv1Pv2=-self.minv1Av2
      self.meanv1Pv2=0.
      self.mean2v1Pv2=0.
      self.stdv1Pv2=0.
      self.mae = 0.
      self.rmse = 0.
      self.corr = np.array([0., 0.])
      self.linRegress = np.zeros(5, float)
      self.polRegress = np.zeros(power+1, float)
      self.polRegressResidual = 0.
      self.polRegressSingVal = np.zeros(power+1, float)

# v1 [+ / - / / / *] v2
##
      self.Nokvalues1 = 0
      self.Nokvalues2 = 0
      self.Nokvalues12 = 0
      self.NDvalNone = 0
      for inum in range(self.Nv):
        if not values1[inum] is None:
          self.Nokvalues1 = self.Nokvalues1 + 1
        if not values2[inum] is None:
          self.Nokvalues2 = self.Nokvalues2 + 1
        if not values1[inum] is None and not values2[inum] is None:
          self.Nokvalues12 = self.Nokvalues12 + 1
          Aval = values1[inum] + values2[inum]
          Sval = values1[inum] - values2[inum]
          Pval = values1[inum] * values2[inum]
          if np.isinf(values1[inum] / values2[inum]) or np.isnan(values1[inum] / values2[inum]):
            if self.NDvalNone < 1:
               print warnmsg
               print '      stats2Val: val1/val2 inf or Nan!!!!'
            Dval = None
            self.NDvalNone = self.NDvalNone + 1
          else:
            Dval = values1[inum] / values2[inum]

          self.mae = self.mae + abs(Sval)
          self.rmse = self.rmse + Sval**2

          if Aval < self.minv1Av2:
            self.minv1Av2 = Aval
          if Aval > self.maxv1Av2:
            self.maxv1Av2 = Aval
          if Sval < self.minv1Sv2:
            self.minv1Sv2 = Sval
          if Sval > self.maxv1Sv2:
            self.maxv1Sv2 = Sval
          if not Dval is None and Dval < self.minv1Dv2:
            self.minv1Dv2 = Dval
          if not Dval is None and  Dval > self.maxv1Dv2:
            self.maxv1Dv2 = Dval
          if Pval < self.minv1Pv2:
            self.minv1Pv2 = Pval
          if Pval > self.maxv1Pv2:
            self.maxv1Pv2 = Pval

          self.meanv1Av2 = self.meanv1Av2+Aval
          self.mean2v1Av2 = self.mean2v1Av2+Aval*Aval
          self.meanv1Sv2 = self.meanv1Sv2+Sval
          self.mean2v1Sv2 = self.mean2v1Sv2+Sval*Sval
          if not Dval is None:
            self.meanv1Dv2 = self.meanv1Dv2+Dval
            self.mean2v1Dv2 = self.mean2v1Dv2+Dval*Dval
          self.meanv1Pv2 = self.meanv1Pv2+Pval
          self.mean2v1Pv2 = self.mean2v1Pv2+Pval*Pval

##      print 'Nokvalues1: ', self.Nokvalues1, 'Nokvalues2: ', self.Nokvalues2, 'Nokvalues12: ', float(self.Nokvalues12), 'NDvalNone: ',self.NDvalNone
      self.meanv1Av2 = self.meanv1Av2/float(self.Nokvalues12)
      self.mean2v1Av2 = self.mean2v1Av2/float(self.Nokvalues12)
      self.stdv1Av2 = np.sqrt(self.mean2v1Av2-self.meanv1Av2*self.meanv1Av2)
      self.meanv1Sv2 = self.meanv1Sv2/float(self.Nokvalues12)
      self.mean2v1Sv2 = self.mean2v1Sv2/float(self.Nokvalues12)
      self.stdv1Sv2 = np.sqrt(self.mean2v1Sv2-self.meanv1Sv2*self.meanv1Sv2)
      if self.Nokvalues12 - self.NDvalNone == 0:
          self.meanv1Dv2 = None
          self.mean2v1Dv2 = None
          self.stdv1Dv2 = None
          print warnmsg
          print '      stats2Val: all values of val1/val2 are None!'
      else:
          self.meanv1Dv2 = self.meanv1Dv2/(float(self.Nokvalues12 - self.NDvalNone))
          self.mean2v1Dv2 = self.mean2v1Dv2/(float(self.Nokvalues12 - self.NDvalNone))
          self.stdv1Dv2 = np.sqrt(self.mean2v1Dv2-self.meanv1Dv2*self.meanv1Dv2)
      self.meanv1Pv2 = self.meanv1Pv2/float(self.Nokvalues12)
      self.mean2v1Pv2 = self.mean2v1Pv2/float(self.Nokvalues12)
      self.stdv1Pv2 = np.sqrt(self.mean2v1Pv2-self.meanv1Pv2*self.meanv1Pv2)

      self.mae = self.mae/self.Nokvalues12
      self.rmse = np.sqrt(self.rmse/self.Nokvalues12)

      vals1Nomiss = np.ones(len(values1), dtype=bool)
      vals2Nomiss = np.ones(len(values1), dtype=bool)

      for i in range(len(values1)):
          if values1[i] is None:
              vals1Nomiss[i] = False

      for i in range(len(values2)):
          if values2[i] is None:
              vals2Nomiss[i] = False

      v1 = np.array(values1[vals1Nomiss], dtype=float)
      v2 = np.array(values2[vals2Nomiss], dtype=float)

      if not v1.shape == v2.shape:
          print errormsg
          print '  ' + fname + ': variables without missing values v1: ',v1.shape , ' and v2: ',v2.shape ,' do not have the same shape! '
          print errormsg
          quit(-1)

      self.corr = sts.pearsonr(v1, v2)

      self.linRegress[0], self.linRegress[1], self.linRegress[2], self.linRegress[3], self.linRegress[4] = sts.linregress(v1, v2)

      polyfitvals=np.polyfit(v1, v2, power, full = True)

      self.polRegress = polyfitvals[0]
      self.polRegressRes = polyfitvals[1]
      self.polRegressSingVal = polyfitvals[3]

def mask_2masked(vals1, vals2):
    """ Function to provide the boolean matrix (in opposite way as it is in the mask) as combination of mask from to masked matrices
    """
    import numpy.ma as ma
    fname = 'mask_2masked'

#    if len(vals1.shape) != len(vals2.shape):
#        print errormsg
#        print '  ' + fname + ' matrix 1 :', len(vals1.shape), ' and matrix 2 ', len(vals2.shape), ' have different size!'
#        print errormsg
#        quit(-1)

#    for idim in range(len(vals1.shape)):
#        if vals1.shape[idim] != vals2.shape[idim]:
#            print errormsg
#            print '  ' + fname + ' dimension ', idim,' from matrix 1 :', vals1.shape[idim], ' and matrix 2 ', \
#                vals2.shape[idim], ' have different size!'
#            print errormsg
#            quit(-1)

    if type(vals1) == type(ma.array(1)):
        mask1array=np.where(ma.getmaskarray(vals1) == False, True, False)
    else:
        mask1array=np.ones(vals1.shape, dtype=bool)

    if type(vals2) == type(ma.array(1)):
        mask2array=np.where(ma.getmaskarray(vals2) == False, True, False)
    else:
        mask2array=np.ones(vals2.shape, dtype=bool)

    mask12 = mask1array*mask2array

    return mask12

def mask_pearsonr(xvals, yvals):
    """ Function to compute a pearson correlation from mask matrices
    """
    from scipy import stats as sts
    fillVal = 1.e20
    maskxy = mask_2masked(xvals, yvals)

    if np.sum(maskxy) > 1:
        pearsonr = sts.pearsonr(xvals[maskxy], yvals[maskxy])
        if np.isnan(pearsonr[0]) or np.isnan(pearsonr[1]): 
           pearsonr = ( fillVal, fillVal)
    else:
        pearsonr = (fillVal, fillVal)

    return pearsonr

def mask_quantiles(maskmat, Nquants):
    """ Function to provide the quantiles of a masked array 20 for %5 bins (21 in total)
    """
    import numpy.ma as ma

    fillValue = 1.e20

    sortmat = maskmat.flatten().copy()
    sortmat.sort()
    quants = np.zeros(Nquants+1, dtype=type(maskmat[0]))
    Nv = ma.size(maskmat)
    NoMask=maskmat.count()

    if NoMask < Nquants:
        quants[:] = fillValue
    else:
        for iq in range(Nquants):
            quants[iq] = sortmat[int((NoMask-1)*iq/(Nquants))]

        quants[Nquants] = sortmat[NoMask-1]

    return quants

def percendone(nvals,tot,percen,msg):
    """ Function to provide the percentage of an action across the matrix
    nvals=number of values
    tot=total number of values
    percen=percentage frequency for which the message is wanted
    msg= message
    """
    from sys import stdout

    num = int(tot * percen/100)
    if (nvals%num == 0): 
        print '\r        ' + msg + '{0:5.2g}'.format(nvals*100./tot) + ' %',
        stdout.flush()

    return ''

def mask_linregres(vals1, vals2):
    """ Function to compute a linear regression from masked data
    vals1: x-values for the regresion
    vals2: y-values for the regresion
    """
    import numpy.ma as ma

    fname = 'mask_linregres'

    missval1 = vals1.get_fill_value()    
    missval2 = vals2.get_fill_value()    

    vals10 = np.where(abs(vals1) >= abs(missval1*0.9), None, vals1)
    vals20 = np.where(abs(vals2) >= abs(missval2*0.9), None, vals2)

    values1 = vals10.flatten() 
    values2 = vals20.flatten() 

    vals1Nomiss = np.ones(len(values1), dtype=bool)
    vals2Nomiss = np.ones(len(values2), dtype=bool)

    for i in range(len(values1)):
        if values1[i] is None:
            vals1Nomiss[i] = False
    for i in range(len(values2)):
        if values2[i] is None:
            vals2Nomiss[i] = False

    v1 = np.array(values1[vals1Nomiss], dtype=float)
    v2 = np.array(values2[vals2Nomiss], dtype=float)

    if len(v1) != len(v2):
        print errormsg
        print fname + ': length of masked matrices mat1:',len(v1),'and mat2:',len(v2),'does not match!'
        print errormsg
        quit(-1)

    linregres = np.array(sts.linregress(v1, v2), dtype= np.float64)

    return linregres

def mask_space_stats(maskmat,statsvals,dim):
    """ Function to give back the multi-dimensional statisitcs of a given masked array
    maskmat=multidimensional masked array
    statsvals=[statn]:[values] 
      [statn]: statistics to do: 
        'quant', quantiles
      [values]: value for the statistics: Nquantiles
    dim= dimension to run the statistics
    """
    from sys import stdout

    fname = 'mask_space_stats'

    statn=statsvals.split(':')[0]
    if len(statsvals.split(':')) > 1:
        values=statsvals.split(':')[1]

    maskshape = maskmat.shape
    Ndims = len(maskshape)
    if statn == 'quant':
        if len(statsvals.split(':')) == 1:
            print errormsg
            print fname + ': statistics "' + statn + '" requires a value!!!'
            print errormsg
            quit(-1)
        Nquants=int(values)
        if Ndims == 2:
            if dim == 0:
                statval = np.ones((21, maskshape[1]), dtype=np.float64)*fillValue
                for i in range(maskshape[1]):
                   percendone(i, maskshape[1], 5, 'quantiles')
                   statval[:,i] = mask_quantiles(maskmat[:,i],Nquants)
            if dim == 1:
                statval = np.ones((21, maskshape[0]), dtype=np.float64)*fillValue
                for i in range(maskshape[0]):
                   percendone(i, maskshape[0], 5, 'quantiles')
                   statval[:,i] = mask_quantiles(statval[i,:],Nquants)
        elif Ndims == 3:
            if dim == 0:
                statval = np.ones((21, maskshape[1], maskshape[2]), dtype=np.float64)*fillValue
                for i in range(maskshape[1]):
                    for j in range(maskshape[2]):
                        percendone(i*maskshape[2] + j, maskshape[1]*maskshape[2], 5, 'quantiles')
                        statval[:,i,j] = mask_quantiles(maskmat[:,i,j],Nquants)
            if dim == 1:
                statval = np.ones((21, maskshape[0], maskshape[2]), dtype=np.float64)*fillValue
                for i in range(maskshape[0]):
                    for j in range(maskshape[2]):
                        percendone(i*maskshape[2] + j, maskshape[0]*maskshape[2], 5, 'quantiles')
                        statval[:,i,j] = mask_quantiles(maskmat[i,:,j],Nquants)
            if dim == 2:
                statval = np.ones((21, maskshape[0], maskshape[1]), dtype=np.float64)*fillValue
                for i in range(maskshape[0]):
                    for j in range(maskshape[1]):
                        percendone(i*maskshape[1] + j, maskshape[0]*maskshape[1], 5, 'quantiles')
                        statval[:,i,j] = mask_quantiles(maskmat[i,j,:],Nquants)
        elif Ndims == 4:
            if dim == 0:
                statval = np.ones((21, maskshape[1], maskshape[2], maskshape[3]), dtype=np.float64)*fillValue
                for i in range(maskshape[1]):
                    for j in range(maskshape[2]):
                        for k in range(maskshape[3]):
                            percendone(i*maskshape[1]*maskshape[2] + j*maskshape[2] + k, maskshape[1]*maskshape[2]*maskshape[3], 5, 'quantiles')
                            statval[:,i,j,k] = mask_quantiles(maskmat[:,i,j,k],Nquants)
            if dim == 1:
                statval = np.ones((21, maskshape[0], maskshape[2], maskshape[3]), dtype=np.float64)*fillValue
                for i in range(maskshape[0]):
                    for j in range(maskshape[2]):
                        for k in range(maskshape[3]):
                            percendone(i*maskshape[0]*maskshape[2] + j*maskshape[2] + k, maskshape[0]*maskshape[2]*maskshape[3], 5, 'quantiles')
                            statval[:,i,j,k] = mask_quantiles(maskmat[i,:,j,k],Nquants)
            if dim == 2:
                statval = np.ones((21, maskshape[0], maskshape[1], maskshape[3]), dtype=np.float64)*fillValue
                for i in range(maskshape[0]):
                    for j in range(maskshape[1]):
                        for k in range(maskshape[3]):
                            percendone(i*maskshape[0]*maskshape[1] + j*maskshape[1] + k, maskshape[0]*maskshape[1]*maskshape[3], 5, 'quantiles')
                            statval[:,i,j,k] = mask_quantiles(maskmat[i,j,:,k],Nquants)
            if dim == 3:
                statval = np.ones((21, maskshape[0], maskshape[1], maskshape[2]), dtype=np.float64)*fillValue
                for i in range(maskshape[0]):
                    for j in range(maskshape[1]):
                        for k in range(maskshape[2]):
                            percendone(i*maskshape[0]*maskshape[1] + j*maskshape[1] + k, maskshape[0]*maskshape[1]*maskshape[2], 5, 'quantiles')
                            statval[:,i,j,k] = mask_quantiles(maskmat[i,j,k,:],Nquants)
        else:
            print errormsg
            print fname + ': size of matrix ', Ndims,'not ready!!!'
            print errormsg
            quit(-1)

    else:
        print errormsg
        print fname + ':  statistics "' + statn + '" not ready!!!!'
        print errormsg
        quit(-1)

    print stdout.write("\n")
    
    return statval

class statsVal(object):
  """Statistics class providing
  vals = variable
  self.Nv = number of values
  self.minv = minimum value
  self.maxv = maximum value
  self.meanv = mean value
  self.mean2v = cuadratic mean value
  self.stdv = standard deviation value
  self.Nokvalues = number of correct values of variable
  self.quantilesv = quantiles (%5 bins) of the variable
  """

  def __init__(self, vals):
    if vals is None:
      self.Nv = None
      self.minv = None
      self.maxv = None
      self.meanv = None
      self.mean2v = None
      self.stdv = None
      self.Nokvalues = None
      self.quantilesv = None
    else:
      values = vals.flatten() 
      self.Nv=len(values)
      self.minv=10000000000.
      self.maxv=-self.minv
      self.meanv=0.
      self.mean2v=0.
      self.stdv=0.

      sortedvalues = sorted(values)

      self.Nokvalues = 0
      for inum in range(self.Nv):
        if not values[inum] is None:
          self.Nokvalues = self.Nokvalues + 1
          if values[inum] < self.minv:
            self.minv = values[inum]
          if values[inum] > self.maxv:
            self.maxv = values[inum]

          self.meanv = self.meanv+values[inum]
          self.mean2v = self.mean2v+values[inum]*values[inum]

      self.meanv = self.meanv/float(self.Nokvalues)
      self.mean2v = self.mean2v/float(self.Nokvalues)
      self.stdv = np.sqrt(self.mean2v-self.meanv*self.meanv)
      self.quantilesv = []
      for iq in range(20):
        self.quantilesv.append(sortedvalues[int((self.Nv-1)*iq/20)])

      self.quantilesv.append(sortedvalues[self.Nv-1])
      self.medianv = self.quantilesv[10]

class statsVal_missVal(object):
  """Statistics class tacking into account a missing value providing
  vals = variable
  missval = missing value
  self.Nv = number of values
  self.minv = minimum value
  self.maxv = maximum value
  self.meanv = mean value
  self.mean2v = cuadratic mean value
  self.stdv = standard deviation value
  self.Nokvalues = number of correct values of variable
  self.quantilesv = quantiles (%5 bins) of the variable
  """

  def __init__(self, vals, missVal):
    fname='statsVal_missVal'
  
    if vals is None:
      self.Nv = None
      self.minv = None
      self.maxv = None
      self.meanv = None
      self.mean2v = None
      self.stdv = None
      self.Nokvalues = None
      self.quantilesv = None
    else:
      Npt = 1
      for idim in range(len(vals.shape)):
        Npt = Npt * vals.shape[idim]
      if np.sum(vals >= missVal) == Npt:
          print errormsg
          print '  ' + fname + ' all values get missing!!'
          print errormsg
          quit(-1)

      vals0 = np.where(abs(vals) >= missVal, None, vals)
      values = vals0.flatten() 
      self.Nv=Npt
      self.minv=10000000000.
      self.maxv=-self.minv
      self.meanv=0.
      self.mean2v=0.
      self.stdv=0.

      sortedvalues = sorted(values)

      self.Nokvalues = 0
      for inum in range(self.Nv):
        if not values[inum] is None:
          self.Nokvalues = self.Nokvalues + 1
          if values[inum] < self.minv:
            self.minv = values[inum]
          if values[inum] > self.maxv:
            self.maxv = values[inum]

          self.meanv = self.meanv+values[inum]
          self.mean2v = self.mean2v+values[inum]*values[inum]

      self.meanv = self.meanv/float(self.Nokvalues)
      self.mean2v = self.mean2v/float(self.Nokvalues)
      self.stdv = np.sqrt(self.mean2v-self.meanv*self.meanv)
      self.quantilesv = []
      for iq in range(20):
        self.quantilesv.append(sortedvalues[int((self.Nv-1)*iq/20)])

      self.quantilesv.append(sortedvalues[self.Nv-1])
      self.medianv = self.quantilesv[10]

def spacemean(ncfile, varn):
  """ Function to retrieve a space mean series from a multidimensional variable of a file
  ncfile = netCDF file name
  varn = variable name
  """
  import datetime as dt
  import calendar as cal

  ofile = 'spacemean_' + varn + '.nc'
  varfil=1.e20
  statsn = ['min', 'max', 'mean', 'mean2', 'stdv', 'meanwgt', 'mean2wgt', 'stdvwgt', 'quant']
  statslongn = ['minimum', 'maximum', 'mean', 'quadratic mean', 'standard deviation', 'weigthed mean',  'weigthed quadratic mean', 'weigthed standard deviation', 'quantiles']

  ncf = NetCDFFile(ncfile,'r')

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    spacemean: File "' + ncfile + '" does not have variable "' + varn + '" !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  times = ncf.variables['time']
  timevals = times[:]

  attvar = times.ncattrs()
  if not searchInlist(attvar, 'units'):
    print errormsg
    print '    spacemean: "time" does not have attribute: "units"'
    ncf.close()
    quit(-1)
  else:
    units = times.getncattr('units')
  
  txtunits = units.split(' ')
  tunits = txtunits[0]
  Srefdate = txtunits[len(txtunits) - 1]
# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
  timeval = Srefdate.find(':')

  if not timeval == -1:
    print '      spacemean: refdate with time!'
    refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
  else:
    refdate = dateStr_date(Srefdate)

  dimt = len(timevals)
  realdates = np.zeros((dimt, 6), dtype=int)
  print realdates.shape

## Not in timedelta
#  if tunits == 'years':
#    for it in range(dimt):
#      realdate = refdate + dt.timedelta(years=float(times[it]))
#      realdates[it] = int(realdate.year)
#  elif tunits == 'months':
#    for it in range(dimt):
#      realdate = refdate + dt.timedelta(months=float(times[it]))
#      realdates[it] = int(realdate.year)
  if tunits == 'weeks':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(weeks=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'days':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(days=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'hours':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(hours=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'minutes':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(minutes=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'seconds':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(seconds=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'milliseconds':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(milliseconds=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  else:
    print errormsg
    print '    spacemean: time units "' + tunits + '" not ready!!!!'
    ncf.close()
    quit(-1)

# Variable values (assuming time as first dimension)
##
  var = ncf.variables[varn]
  varinf = variable_inf(var)
  if searchInlist(varinf.attributes, 'scale_factor'):
    scalefact = var.getncattr('scale_factor')
    print '      spacemean: data has a scale factor of ', scalefact
    varinf.dtype=type(np.float(1.))
  else:
    scalefact = 1.

  if searchInlist(varinf.attributes, 'add_offset'):
    offset = var.getncattr('add_offset')
    print '      spacemean: data has an offset of ', offset
  else:
    offset = 0.

#  varvals = var[:]
  vardims = var.shape
  varshape = len(vardims)
  vardimns = var.dimensions
##  print '      spacemean: Shape of data: ',varshape, ':' , vardims
  dimt=vardims[0]
  vardnames = []

# Spatial average spatial weigthed
## 
  lonvar = ncf.variables['lon']
  latvar = ncf.variables['lat']

  if not len(lonvar.shape) == 2:
    lonv = lonvar[:]
    latv = latvar[:]
    dx=lonvar.shape[0]
    dy=latvar.shape[0]
    lonval = np.zeros((dy, dx), dtype=np.float64)    
    latval = np.zeros((dy, dx), dtype=np.float64)    
    for iy in range(dy):
      lonval[iy,:] = lonv
    for ix in range(dx):
      latval[:,ix] = latv
  else:
    lonval = lonvar[:]
    latval = latvar[:]

  weightsv = abs(np.cos(latval*np.pi/180.))

  if varinf.Ndims == 1:
    print errormsg
    print '    spacemean: You can not compute a space mean for a ', varinf.Ndims, 'D var!!!'
    ncf.close()
    quit(-1)
  elif varinf.Ndims== 2:
    print errormsg
    print '    spacemean: You can not compute a space mean for a ', varinf.Ndims, 'D var!!!'
    ncf.close()
    quit(-1)
  elif varinf.Ndims == 3:
    varstats = np.ones((dimt, 8), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((dimt, 21), dtype=np.float64)
    varquant = varquant*varfil
    vardnames.append(vardimns[0])
    dy=vardims[1]
    dx=vardims[2]
    for it in range(dimt):
      if not varinf.FillValue is None:
        varvl = var[it,:,:]
        varval = np.where(varvl == varinf.FillValue, None, varvl)
      else:
        varval = var[it,:,:]
      percendone(it,dimt,5,'computed space statistics')
      varst = statsVal(varval)
      varstwgt  = statsValWeigthed(varval, weightsv)
      varstats[it,0] = varst.minv
      varstats[it,1] = varst.maxv
      varstats[it,2] = varst.meanv
      varstats[it,3] = varst.mean2v
      varstats[it,4] = varst.stdv
      varquant[it,:] = varst.quantilesv
      varstats[it,5] = varstwgt.meanv
      varstats[it,6] = varstwgt.mean2v
      varstats[it,7] = varstwgt.stdv
  elif varshape == 4:
    varstats = np.ones((dimt, vardims[1], 8), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((dimt, vardims[1], 21), dtype=np.float64)
    varquant = varquant*varfil
    vardimnames = (str(vardimns[0]), str(vardimns[1]))
    vardnames.append(vardimns[0])
    vardnames.append(vardimns[1])
    dy=vardims[2]
    dx=vardims[3]
    for it in range(dimt):
      for ik in range(vardims[1]):
        if not varinf.FillValue is None:
          varvl = var[it,ik,:,:]
          varval = np.where(varvl == varinf.FillValue, None, varvl)
        else:
          varval = var[it,ik,:,:]
        percendone(it*vardims[1]+ik,dimt*vardims[1],5,'computed space statistics')
        varst = statsVal(varval)
        varstwgt  = statsValWeigthed(varval, weightsv)
        varstats[it,ik,0] = varst.minv
        varstats[it,ik,1] = varst.maxv
        varstats[it,ik,2] = varst.meanv
        varstats[it,ik,3] = varst.mean2v
        varstats[it,ik,4] = varst.stdv
        varquant[it,ik,:] = varst.quantilesv
        varstats[it,ik,5] = varstwgt.meanv
        varstats[it,ik,6] = varstwgt.mean2v
        varstats[it,ik,7] = varstwgt.stdv
  elif varshape == 5:
    varstats = np.ones((dimt, vardims[1], vardims[2], 8), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((dimt,  vardims[1], vardims[2], 21), dtype=np.float64)
    varquant = varquant*varfil
    vardnames.append(vardimns[0])
    vardnames.append(vardimns[1])
    vardnames.append(vardimns[2])
    dy=vardims[3]
    dx=vardims[4]
    for it in range(dimt):
      for ik in range(vardims[1]):
        for il in range(vardims[2]):
          if not varinf.FillValue is None:
            varvl = var[it,ik,il,:,:]
            varval = np.where(varvl == varinf.FillValue, None, varvl)
          else:
            varval = var[it,ik,il,:,:]

          percendone(it*vardims[1]*vardims[2]+ik*vardims[1]+il,dimt*vardims[1]*vardims[2],5,'computed space statistics')
          varst = statsVal(varval)
          varstwgt  = statsValWeigthed(varval, weightsv)
          varstats[it,ik,il,0] = varst.minv
          varstats[it,ik,il,1] = varst.maxv
          varstats[it,ik,il,2] = varst.meanv
          varstats[it,ik,il,3] = varst.mean2v
          varstats[it,ik,il,4] = varst.stdv
          varquant[it,ik,il,:] = varst.quantilesv
          varstats[it,ik,il,5] = varstwgt.meanv
          varstats[it,ik,il,6] = varstwgt.mean2v
          varstats[it,ik,il,7] = varstwgt.stdv
  else:
     print errormsg
     print '    spacemean: ', varshape, ' shape of matrix not prepared !'
     ncf.close()
     quit(-1)

  vardimnames = tuple(vardnames)
#  print '  shape of desired values: ', vardesiredvalues.shape
# Creation of file
##

  ncfo = NetCDFFile( ofile, 'w')

  vartype = var.dtype
  varattr = var.ncattrs()

# Checking dimensions
##
  newdims = ncfo.dimensions
  Nvardnames = len(vardimnames)
  for idim in range(Nvardnames):
      rdim = vardimnames[idim] 
      if not searchInlist(newdims, rdim):
          if not rdim == 'time':
              print '      spacemean: Adding dimension ' + rdim
              ncfo.sync()
              ncf.close()
              ncfo.close()
              fdimadd(ncfile + ':' + rdim, ofile)
              ncf = NetCDFFile(ncfile,'r')
              ncfo = NetCDFFile(ofile,'a')
          else:
              ncfo.createDimension('time', None)

# Checking fill value
## 
  if searchInlist(varattr, '_FillValue'):
      varfil = var._FillValue
  else:
      varfil = False

  Nstats = len(statsn)
  for ist in range(Nstats):
    if statsn[ist] == 'quant':
      print ist, statsn[ist]##, ': ', varquant[int(dimt/2),10]
      newdim = ncfo.createDimension('quant', 21)
      newvardnames = list(vardnames)
      newvardnames.append('quant')
      newvar = ncfo.createVariable(varn + statsn[ist], varinf.dtype, tuple(newvardnames), fill_value=varfil)
      newvar[:] = varquant
    else:
      print ist, statsn[ist]##, ': ', varstats[int(dimt/2),ist]
      newvar = ncfo.createVariable(varn + statsn[ist], varinf.dtype, vardimnames, fill_value=varfil)
      if varshape == 3:
        newvar[:] = varstats[:,ist]*1.
      elif varshape == 4:
        newvar[:] = varstats[:,:,ist]*1.
      elif varshape == 5:
        newvar[:] = varstats[:,:,:,ist]*1.

    newvar = ncfo.variables[varn + statsn[ist]]
    for attr in varattr:
         newvarattrs = newvar.ncattrs()
         attrv = var.getncattr(attr)
         if not searchInlist(newvarattrs, attr):
             if attr != 'scale_factor' and attr != 'add_offset' and attr != 'valid_range' \
                and attr != 'unpacked_valid_range' and attr != 'actual_range' :
#                 if not statsn[ist] == 'stdv' and not statsn[ist] == 'stdvwgt':
                     print 'attr:', attr
                     newvar.setncattr(attr, attrv)
#             else:
#                 newvar.setncattr(attr, attrv)
    
    newvar.setncattr('cell_methods', 'space ' + statslongn[ist] + ' all domain ' + str(dy) + 'x' + str(dx))

##  print '  Adding time variable'
  vartdims = times.dimensions
  vartype = times.dtype
  varattr = times.ncattrs()

  newvar = ncfo.createVariable('time', vartype, vartdims, fill_value=varfil)
  newvar = ncfo.variables['time']
  newvar[:] = times

  ncf.close()
  ncfo.sync()
  ncfo.close()
  fattradd('time', ncfile + ':time', ofile)
  fvaradd(ncfile + ':lon', ofile)
  fvaradd(ncfile + ':lat', ofile)

  ncfo = NetCDFFile(ofile,'a')
  newvar = ncfo.variables['time']
  newvarattrs = newvar.ncattrs()
  if searchInlist(newvarattrs, 'bounds'):
      if newvar.getncattr('bounds') == 'time_bnds':
          ncf = NetCDFFile(ncfile,'r')
          tbnds = ncf.variables['time_bnds']
          vardims = tbnds.dimensions
          vartype = tbnds.dtype
          varattr = tbnds.ncattrs()
          ncfo.createDimension('bnds', 2)
          newvar = ncfo.createVariable('time_bnds', vartype, vardims, fill_value=varfil)
          newvar[:] = tbnds[:]

          ncf.close()
          ncfo.sync()
          ncfo.close()
          fattradd('time_bnds', ncfile + ':time_bnds', ofile)
      else:
          ncfo.close()
  else:
      ncfo.close()

  fgaddattr(ncfile, ofile)

  print '    spacemean: File "' + ofile + '" as space mean of "' + varn + '" has been created'

def timemean(values, ncfile, varn):
  """ Function to retrieve a time mean series from a multidimensional variable of a file
  values = power of the polynomial fitting with time to be applied
  ncfile = netCDF file name
  varn = variable name
  """
  import datetime as dt
  import calendar as cal

  powerv=int(values)
  ofile = 'timemean_' + varn + '.nc'
  varfil=1.e20
  statsn = ['min', 'max', 'mean', 'mean2', 'stdv', 'quant','linregress','polregress']
  statslongn = ['minimum', 'maximum', 'mean', 'quadratic mean', 'standard deviation', 'quantiles', \
    'linear regression', 'polynomial regression']

  ncf = NetCDFFile(ncfile,'r')

  if not ncf.variables.has_key(varn):
    print errormsg
    print '    timemean: File  "' + ncfile + '" does not have variable ' + varn + ' !!!!'
    print errormsg
    ncf.close()
    quit(-1)

  times = ncf.variables['time']
  timevals = times[:]

  attvar = times.ncattrs()
  if not searchInlist(attvar, 'units'):
    print errormsg
    print '    timemean: "time" does not have attribute: "units"'
    ncf.close()
    quit(-1)
  else:
    units = times.getncattr('units')
  
  txtunits = units.split(' ')
  tunits = txtunits[0]
  Srefdate = txtunits[len(txtunits) - 1]
# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
  timeval = Srefdate.find(':')

  if not timeval == -1:
    print '      timemean: refdate with time!'
    refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
  else:
    refdate = dateStr_date(Srefdate)

  dimt = len(timevals)
  realdates = np.zeros((dimt, 6), dtype=int)
  print realdates.shape

## Not in timedelta
#  if tunits == 'years':
#    for it in range(dimt):
#      realdate = refdate + dt.timedelta(years=float(times[it]))
#      realdates[it] = int(realdate.year)
#  elif tunits == 'months':
#    for it in range(dimt):
#      realdate = refdate + dt.timedelta(months=float(times[it]))
#      realdates[it] = int(realdate.year)
  if tunits == 'weeks':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(weeks=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'days':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(days=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'hours':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(hours=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'minutes':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(minutes=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'seconds':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(seconds=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  elif tunits == 'milliseconds':
    for it in range(dimt):
      realdate = refdate + dt.timedelta(milliseconds=float(times[it]))
      realdates[it,0] = int(realdate.year)
      realdates[it,1] = int(realdate.month)
      realdates[it,2] = int(realdate.day)
      realdates[it,3] = int(realdate.hour)
      realdates[it,4] = int(realdate.second)
      realdates[it,5] = int(realdate.minute)
  else:
    print errormsg
    print '    timemean: time units "' + tunits + '" not ready!!!!'
    ncf.close()
    quit(-1)

  timesv = (realdates[:,0] - realdates[0,0])*12 + realdates[:,1]

# Variable values (assuming time as first dimension)
##
  var = ncf.variables[varn]
  varinf = variable_inf(var)

#  varvals = var[:]
  vardims = varinf.dims
  varshape = varinf.Ndims
  vardimns = var.dimensions

##  print '     timemean: Shape of data: ',varshape, ':' , vardims
  dimt=vardims[0]
  vardnames = []

  if varshape == 1:
    print errormsg
    print '    timemean: You can not compute a time mean for a ', varshape, 'D var!!!'
    ncf.close()    
    quit(-1)
  elif varshape == 2:
    dx=vardims[1]
    varstats = np.ones((5, dx), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((21, dx), dtype=np.float64)
    varquant = varquant*varfil
    varlregress = np.ones((5, dx), dtype=np.float64)
    varlregress = varlregress*varfil
    varpregress = np.ones((powerv+1, dx), dtype=np.float64)
    varpregress = varpregress*varfil
    varpregressres = np.ones((dx), dtype=np.float64)
    varpregressres = varpregressres*varfil
    varpregresssingval = varpregress.copy()
    varpregresssingval = varpregresssingval*varfil
    vardnames.append(vardimns[1])
    for ix in range(dx):
      if not varinf.FillValue is None:
        varvl = var[:,ix]
        varval = np.where(varvl == varinf.FillValue, None, varvl)
      else:
        varval = var[:,ix]
      percendone(ix,dx,5,'computed time statistics')

      varst = statsVal(varval)
      var2st = stats2Val(timesv, varval, powerv)
      varstats[0,ix] = varst.minv
      varstats[1,ix] = varst.maxv
      varstats[2,ix] = varst.meanv
      varstats[3,ix] = varst.mean2v
      varstats[4,ix] = varst.stdv
      varquant[:,ix] = varst.quantilesv
      varlregress[:,ix] = var2st.linRegress
      varpregress[:,ix] = var2st.polRegress
      varpregressres[ix] = var2st.polRegressRes
      varpregresssingval[:,ix] = var2st.polRegressSingVal
  elif varshape == 3:
    dy=vardims[1]
    dx=vardims[2]
    varstats = np.ones((5,dy, dx), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((21,dy, dx), dtype=np.float64)
    varquant = varquant*varfil
    varlregress = np.ones((5,dy, dx), dtype=np.float64)
    varlregress = varlregress*varfil
    varpregress = np.ones((powerv+1,dy, dx), dtype=np.float64)
    varpregress = varpregress*varfil
    varpregressres = np.ones((dy,dx), dtype=np.float64)
    varpregressres = varpregressres*varfil
    varpregresssingval = varpregress.copy()
    varpregresssingval = varpregresssingval*varfil
    vardnames.append(vardimns[1])
    vardnames.append(vardimns[2])
    for iy in range(dy):
      for ix in range(dx):
        
        if not varinf.FillValue is None:
          varvl = var[:,iy,ix]
          varval = np.where(varvl == varinf.FillValue, None, varvl)
        else:
          varval = var[:,iy,ix]
      
        percendone(iy*dx+ix,dy*dx,5,'computed time statistics')
        varst = statsVal(varval)
        var2st = stats2Val(timesv, varval, powerv)
        varstats[0,iy,ix] = varst.minv
        varstats[1,iy,ix] = varst.maxv
        varstats[2,iy,ix] = varst.meanv
        varstats[3,iy,ix] = varst.mean2v
        varstats[4,iy,ix] = varst.stdv
        varquant[:,iy,ix] = varst.quantilesv
        varlregress[:,iy,ix] = var2st.linRegress
        varpregress[:,iy,ix] = var2st.polRegress
        varpregressres[iy,ix] = var2st.polRegressRes
        varpregresssingval[:,iy,ix] = var2st.polRegressSingVal
  elif varshape == 4:
    dz=vardims[1]
    dy=vardims[2]
    dx=vardims[3]
    varstats = np.ones((5, dz, dy, dx), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((21, dz, dy, dx), dtype=np.float64)
    varquant = varquant*varfil
    varlregress = np.ones((5, dz, dy, dx), dtype=np.float64)
    varlregress = varlregress*varfil
    varpregress = np.ones((powerv+1, dz, dy, dx), dtype=np.float64)
    varpregress = varpregress*varfil
    varpregressres = np.ones((dz,dy,dx), dtype=np.float64)
    varpregressres = varpregressres*varfil
    varpregresssingval = varpregress.copy()
    varpregresssingval = varpregresssingval*varfil
    vardnames.append(vardimns[1])
    vardnames.append(vardimns[2])
    vardnames.append(vardimns[3])
    for iz in range(dz):
      for iy in range(dy):
        for ix in range(dx):
          if not varinf.FillValue is None:
            varvl = var[:,iz,iy,ix]
            varval = np.where(varvl == varinf.FillValue, None, varvl)
          else:
            varval = var[:,iz,iy,ix]
      
          percendone(iz*dy*dx+iy*dx+ix,dz*dx*dy,5,'computed time statistics')
          varst = statsVal(varval)
          var2st = stats2Val(timesv, varval, powerv)
          varstats[0,iz,iy,ix] = varst.minv
          varstats[1,iz,iy,ix] = varst.maxv
          varstats[2,iz,iy,ix] = varst.meanv
          varstats[3,iz,iy,ix] = varst.mean2v
          varstats[4,iz,iy,ix] = varst.stdv
          varquant[:,iz,iy,ix] = varst.quantilesv
          varlregress[:,iz,iy,ix] = var2st.linRegress
          varpregress[:,iz,iy,ix] = var2st.polRegress
          varpregressres[iz,iy,ix] = var2st.polRegressRes
          varpregresssingval[:,iz,iy,ix] = var2st.polRegressSingVal

  elif varshape == 5:
    dn=vardims[1]
    dz=vardims[2]
    dy=vardims[3]
    dx=vardims[4]
    varstats = np.ones((5, dn, dz, dy, dx), dtype=np.float64)
    varstats = varstats*varfil
    varquant = np.ones((21, dn, dz, dy, dx), dtype=np.float64)
    varquant = varquant*varfil
    varlregress = np.ones((5, dn, dz, dy, dx), dtype=np.float64)
    varlregress = varlregress*varfil
    varpregress = np.ones((powerv+1, dn, dz, dy, dx), dtype=np.float64)
    varpregress = varpregress*varfil
    varpregressres = np.ones((dn,dz,dy,dx), dtype=np.float64)
    varpregressres = varpregressres*varfil
    varpregresssingval = varpregress.copy()
    varpregresssingval = varpregresssingval*varfil
    vardnames.append(vardimns[1])
    vardnames.append(vardimns[2])
    vardnames.append(vardimns[3])
    vardnames.append(vardimns[4])
    for iN in range(dn):
      for iz in range(dz):
        for iy in range(dy):
          for ix in range(dx):
            if not varinf.FillValue is None:
              varvl = var[:,iN,iz,iy,ix]
              varval = np.where(varvl == varinf.FillValue, None, varvl)
            else:
              varval = var[:,iN,iz,iy,ix]
      
            percendone(iN*dy*dx*dz+iz*dy*dx+iy*dx+ix,dn*dz*dx*dy,5,'computed time statistics')
            varst = statsVal(varval)
            var2st = stats2Val(timesv, varval, powerv)
            varstats[0,iN,iz,iy,ix] = varst.minv
            varstats[1,iN,iz,iy,ix] = varst.maxv
            varstats[2,iN,iz,iy,ix] = varst.meanv
            varstats[3,iN,iz,iy,ix] = varst.mean2v
            varstats[4,iN,iz,iy,ix] = varst.stdv
            varquant[:,iN,iz,iy,iiN,x] = varst.quantilesv
            varlregress[:,iN,iz,iy,ix] = var2st.linRegress
            varpregress[:,iN,iz,iy,ix] = var2st.polRegress
            varpregressres[iN,iz,iy,ix] = var2st.polRegressRes
            varpregresssingval[:,iN,iz,iy,ix] = var2st.polRegressSingVal

  else:
     print errormsg
     print '    timemean: ', varshape, ' shape of matrix not prepared !'
     ncf.close()
     quit(-1)

  vardimnames = tuple(vardnames)
#  print '  shape of desired values: ', vardesiredvalues.shape
# Creation of file
##

  ncfo = NetCDFFile( ofile, 'w')

  vartype = var.dtype
  varattr = var.ncattrs()

# Checking dimensions
##
  newdims = ncfo.dimensions
  Nvardnames = len(vardimnames)
  for idim in range(Nvardnames):
      rdim = vardimnames[idim] 
      if not searchInlist(newdims, rdim):
          if not rdim == 'time':
##              print '      timemean: Adding dimension ' + rdim
              ncfo.sync()
              ncf.close()
              ncfo.close()
              fdimadd(ncfile + ':' + rdim, ofile)
              ncf = NetCDFFile(ncfile,'r')
              ncfo = NetCDFFile(ofile,'a')
          else:
              print '      timemean: No time dimension!'

# Checking fill value
## 
  if searchInlist(varattr, '_FillValue'):
      varfil = var._FillValue
  else:
      varfil = False

  Nstats = len(statsn)
  for ist in range(Nstats):
    newvardnames = []
    if statsn[ist] == 'quant':
      print ist, statsn[ist]##, ': ', varquant[int(dimt/2),10]
      newdim = ncfo.createDimension('quant', 21)
      newvardnames.append('quant')
      newvardnames = newvardnames + list(vardnames)
      newvar = ncfo.createVariable(varn + statsn[ist], vartype, tuple(newvardnames), fill_value=varfil)
      newvar[:] = varquant
    elif statsn[ist] == 'linregress':
      print ist, statsn[ist]##, ': ', varquant[int(dimt/2),10]
      newdim = ncfo.createDimension('lregress', 5)
      newvar = ncfo.createVariable('lregressn', str, ('lregress',))
      newvar[0] = 'slope'
      newvar[1] = 'intercept'
      newvar[2] = 'r_value'
      newvar[3] = 'p_value'
      newvar[4] = 'std_err'
      newvardnames.append('lregress')
      newvardnames = newvardnames + list(vardnames)
      newvar = ncfo.createVariable(varn + statsn[ist], 'f4', tuple(newvardnames), fill_value=varfil)
      newvar[:] = varlregress*1.

    elif statsn[ist] == 'polregress':
      print ist, statsn[ist]##, ': ', varquant[int(dimt/2),10]
      newdim = ncfo.createDimension('pregress', powerv+1)
      newvar = ncfo.createVariable('pregressn', str, ('pregress',))
      for ip in range(powerv+1):
        newvar[ip]='coefficient**' + str(powerv-ip)
      newvardnames.append('pregress')
      newvardnames = newvardnames + list(vardnames)
      newvar = ncfo.createVariable(varn + statsn[ist], 'f4', tuple(newvardnames), fill_value=varfil)
      newvar[:] = varpregress*1.
      newvar.setncattr('power',powerv)
      newvar.setncattr('values','Polynomial coefficients, highest power first')
      newvar = ncfo.createVariable(varn + statsn[ist] + '_Residual', vartype, tuple(vardnames), fill_value=varfil)
      newvar[:] = varpregressres
      newvar.setncattr('power',powerv)
      newvar.setncattr('values','Polynomial residuals')
      newvar = ncfo.createVariable(varn + statsn[ist] + '_VandermondeSingularVector', vartype, tuple(newvardnames), fill_value=varfil)
      newvar[:] = varpregresssingval
      newvar.setncattr('power',powerv)
      newvar.setncattr('values','Polynomial coefficients, highest power first')
    else:
      print ist, statsn[ist]##, ': ', varstats[int(dimt/2),ist]
      if statsn[ist] == 'mean' or statsn[ist] == 'stdv' or statsn[ist] == 'mean2' or statsn[ist] == 'polregress_Residual' \
        or statsn[ist] == 'polregress_VandermondeSingularVector' and searchInlist(varattr, 'scale_factor'):
        newvar = ncfo.createVariable(varn + statsn[ist], 'f4', vardimnames, fill_value=varfil)
        if varshape == 2:
          newvar[:] = varstats[ist,:]*1.
        elif varshape == 3:
          newvar[:] = varstats[ist,:,:]*1.
        elif varshape == 4:
          newvar[:] = varstats[ist,:,:,:]*1.
        elif varshape == 5:
          newvar[:] = varstats[ist,:,:,:,:]*1.
      else:
        newvar = ncfo.createVariable(varn + statsn[ist], vartype, vardimnames, fill_value=varfil)
        if varshape == 2:
          newvar[:] = varstats[ist,:]
        elif varshape == 3:
          newvar[:] = varstats[ist,:,:]
        elif varshape == 4:
          newvar[:] = varstats[ist,:,:,:]
        elif varshape == 5:
          newvar[:] = varstats[ist,:,:,:,:]

    newvar = ncfo.variables[varn + statsn[ist]]
    for attr in varattr:
         newvarattrs = newvar.ncattrs()
         attrv = var.getncattr(attr)
         if not searchInlist(newvarattrs, attr):
              if attr == 'scale_factor' or attr == 'add_offset' or attr == 'valid_range' \
                or attr == 'unpacked_valid_range' or attr == 'actual_range' :
                  if not statsn[ist] == 'mean' and not statsn[ist] == 'stdv' and not statsn[ist] == 'mean2' and not \
                    statsn[ist] == 'linregress' and not statsn[ist] == 'polregress' \
                    and not statsn[ist] == 'polregress_Residual' and not \
                    statsn[ist] == 'polregress_VandermondeSingularVector':
                      newvar.setncattr(attr, attrv)
              else:
                  newvar.setncattr(attr, attrv)
    
    newvar.setncattr('cell_methods', 'time ' + statslongn[ist] + ' all period in file ' + str(dimt) + ' time steps')

  ncfo.sync()
  ncfo.close()

  fvaradd(ncfile + ':lon', ofile)
  fvaradd(ncfile + ':lat', ofile)

  fgaddattr(ncfile, ofile)

  print '    timemean: File "' + ofile + '" as time mean of "' + varn + '" has been created'

def printing_class(classobj):
    """ Function to print all the values of a given class
    """

    valscls = vars(classobj)
    for attrcls in valscls:
        print attrcls, ':', valscls[attrcls]

def fmtprinting_class(classobj):
    """ Function to print all the values of a given class
    """

    valscls = vars(classobj)
    for attrcls in valscls:
        print '@' + attrcls + '@', ':', valscls[attrcls]

def flipdim(values, filename, varn):
    """ flips the value following one dimension [Nflipdim]
    values=[Nflipdim]:[flipdim]
      [Nflipdim]: number of the dimension to flip
      [flipdim]: has also the corresondant variable dimensinon be flipped? (yes/no)
    filename= netCDF file name
    varn= variable name
    """
    ncf = NetCDFFile(filename,'a')

    Nflipdim=int(values.split(':')[0])
    flipdim=values.split(':')[1]

    if not ncf.variables.has_key(varn):
        print errormsg
        print '    flipdim: File  "' + filename + '" does not have variable ' + varn + ' !!!!'
        print errormsg
        ncf.close()
        quit(-1)

    varnc = ncf.variables[varn]
    varinf = variable_inf(varnc)

    if varinf.Ndims < Nflipdim:
        print errormsg
        print '    flipdim: variable "' + varn + '" has less dimensions ', varinf.Ndims, 'than the required to flip "', Nflipdim, '!!!!'
        print errormsg
        quit(-1)
 
    if flipdim == 'yes':
        flipdimname = varinf.dimns[Nflipdim]
        print '  flipdim: Flipping also associated dimension variable "' + flipdimname + '"'

        flipdim = ncf.variables[flipdimname]

        if len(flipdim.shape) == 1:
            newdim = flipdim[:]
            flipdim[:] = newdim[::-1]
        elif len(flipdim.shape) == 2:
            if Nflipdim == varinf.Ndims:
                for i in range(flipdim.shape[0]):
                    newdim = flipdim[i,:]
                    flipdim[i,:] = newdim[::-1]
            if Nflipdim == varinf.Ndims - 1:
                for i in range(flipdim.shape[1]):
                    newdim = flipdim[:,i]
                    flipdim[:,:] = newdim[::-1]
        else:
            print errormsg
            print '    flipdim: dimension to flip has ', len(flipdim.shape), ' dimensions, not ready to be flipped !!!'
            print errormsg
            quit(-1) 

    newvar = np.ones(varinf.dims, dtype=varinf.dtype)
    if varinf.Ndims == 1:
        newvar = varnc[:]
        varnc[:] = newvar[::-1]
    elif varinf.Ndims == 2:
        if Nflipdim == 1:
            for i in range(varinf.dims[0]):
                newvar[i,:] = varnc[i,:]
                varnc[i,:] = newvar[i,::-1]
        elif Nflipdim == 0:
            for i in range(varinf.dims[1]):
                newvar[:,i] = varnc[:,i]
                varnc[:,i] = newvar[::-1,i]
    elif varinf.Ndims == 3:
        if Nflipdim == 2:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[1]):
                    newvar[i,j,:] = varnc[i,j,:]
                    varnc[i,j,:] = newvar[i,j,::-1]
        elif Nflipdim == 1:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[2]):
                    newvar[i,:,j] = varnc[i,:,j]
                    varnc[i,:,j] = newvar[i,::-1,j]
        elif Nflipdim == 0:
            for i in range(varinf.dims[1]):
                for j in range(varinf.dims[2]):
                    newvar[:,i,j] = varnc[:,i,j]
                    varnc[:,i,j] = newvar[::-1,i,j]
    elif varinf.Ndims == 4:
        if Nflipdim == 3:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[1]):
                    for k in range(varinf.dims[2]):
                        newvar[i,j,k,:] = varnc[i,j,k,:]
                        varnc[i,j,k,:] = newvar[i,j,k,::-1]
        if Nflipdim == 2:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[1]):
                    for k in range(varinf.dims[3]):
                        newvar[i,j,:,k] = varnc[i,j,:,k]
                        varnc[i,j,:,k] = newvar[i,j,::-1,k]
        if Nflipdim == 1:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[2]):
                    for k in range(varinf.dims[3]):
                        newvar[i,:,j,k] = varnc[i,:,j,k]
                        varnc[i,:,j,k] = newvar[i,::-1,j,k]
        if Nflipdim == 0:
            for i in range(varinf.dims[1]):
                for j in range(varinf.dims[2]):
                    for k in range(varinf.dims[3]):
                        newvar[:,i,j,k] = varnc[:,i,j,k]
                        varnc[:,i,j,k] = newvar[::-1,i,j,k]
    elif varinf.Ndims == 5:
        if Nflipdim == 4:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[1]):
                    for k in range(varinf.dims[2]):
                        for l in range(varinf.dims[3]):
                            newvar[i,j,k,l,:] = varnc[i,j,k,l,:]
                            varnc[i,j,k,l,:] = newvar[i,j,k,l,::-1]
        if Nflipdim == 3:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[1]):
                    for k in range(varinf.dims[2]):
                        for l in range(varinf.dims[4]):
                            newvar[i,j,k,:,l] = varnc[i,j,k,:,l]
                            var[i,j,k,:,l] = newvar[i,j,k,::-1,l]
        if Nflipdim == 2:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[1]):
                    for k in range(varinf.dims[3]):
                        for l in range(varinf.dims[4]):
                            newvar[i,j,:,k,l] = varnc[i,j,:,k,l]
                            varnc[i,j,:,k,l] = newvar[i,j,::-1,k,l]
        if Nflipdim == 1:
            for i in range(varinf.dims[0]):
                for j in range(varinf.dims[2]):
                    for k in range(varinf.dims[3]):
                        for l in range(varinf.dims[4]):
                            newvar[i,:,j,k,l] = varnc[i,:,j,k,l]
                            varnc[i,:,j,k,l] = newvar[i,::-1,j,k,l]
        if Nflipdim == 0:
            for i in range(varinf.dims[1]):
                for j in range(varinf.dims[2]):
                    for k in range(varinf.dims[3]):
                        for l in range(varinf.dims[4]):
                            newvar[:,i,j,k,l] = varnc[:,i,j,k,l]
                            varnc[:,i,j,k,l] = newvar[::-1,i,j,k,l]
    else:
        print errormsg
        print '    flipdim: Number of dimensions ', varinf.Ndims, ' not ready!!!!'
        print errormsg
        quit(-1)

    ncf.sync()
    ncf.close()

def cycl_incr(cyclval,cycle,ninc):
    """ Function to increment a cyclic value [cyclval] with a cycle [cycle] a given number [ninc] of times
    >>> cycl_incr(1,4,1)
    2
    >>> cycl_incr(3,4,1)
    0
    >>> cycl_incr(1,4,10)
    3
    """

    if ninc >= cycle:
        print 'cycl_incr: WARNING -- warning -- WARNING -- warning'
        print '    given increment: ', ninc,' is larger than the cycle !!'
        ninc = ninc - cycle*int(ninc/cycle)
        print '    reducing it to: ', ninc

    val=cyclval + ninc
    if val >= cycle:
        val=cyclval + ninc - cycle

    return val

def times_4seasons(tvals, integrity):
    """ Function to split a time series in matricial date format ([:,year,mon,day,hour,minute,second]) in the four stations DJF,MAM,JJA,SON
    tvals= matrix withe times as [:,year,mon,day,hour,minute,second]
    integrity= only give values for entire seasons [True, 3 months for the season], or just split the values by seasons [False] 
    """
    fillVal=1.e20

#    print tvals

    dt=tvals.shape[0]
    seasons=np.ones((dt,4),dtype=bool)
    seasons=seasons*False

    monseas=[12,3,6,9]
    firstseas=False

    for it in range(dt):
        if not firstseas:
            if integrity:
                for iseas in range(4):
                    if tvals[it,1] == monseas[iseas]:
                        nseas=iseas
                        seasons[it,nseas]=True
                        firstseas=True
            else:
                for iseas in range(4):
                    for imon in range(3):
                        if tvals[it,1] == cycl_incr(monseas[iseas],12,imon):
                            nseas=iseas
                            seasons[it,nseas]=True
                            firstseas=True                    
        else:
            newseas=cycl_incr(nseas,4,1)
            if tvals[it,1] == monseas[newseas]:
                seasons[it,newseas] = True
                nseas=newseas
                begseas=it
            else:
                seasons[it,nseas] = True

    endseas = it
##    print 'Last season: ',nseas,' beginnig: ',begseas,' ending: ',endseas

# Checking integrity of last season (has to have 3 months)
##
    if integrity:
        fullseas=True
        Nmon=np.unique(tvals[begseas:endseas+1,1])
        for it in range(begseas,endseas): fullseas=fullseas*seasons[it,nseas]
        if len(Nmon) < 3 or not fullseas:
            seasons[begseas:endseas+1,nseas] = False

    return seasons

def realdatetime_CFcompilant(times, Srefdate, tunits):
    """ Function to transform a matrix with real time values ([year, month, day, hour, minute, second]) to a netCDF one
    times= matrix with times
    Srefdate= reference date ([YYYY][MM][DD][HH][MI][SS] format)
    tunits= units of time respect to Srefdate
    >>> realdatetime_CFcompilant(np.array([ [1976, 2, 17, 8, 20, 0], [1976, 2, 18, 8, 20, 0]], dtype=int), '19491201000000', 'hours')
    [ 229784.33333333  229808.33333333]
    """ 

    import datetime as dt
    yrref=int(Srefdate[0:4])
    monref=int(Srefdate[4:6])
    dayref=int(Srefdate[6:8])
    horref=int(Srefdate[8:10])
    minref=int(Srefdate[10:12])
    secref=int(Srefdate[12:14])
 
    refdate=dt.datetime(yrref, monref, dayref, horref, minref, secref)

    dimt=times.shape[0]
        
    cfdates = np.zeros((dimt), dtype=np.float64)
    if tunits == 'weeks':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = (cfdate.days + cfdate.seconds/(3600.*24.))/7.
    elif tunits == 'days':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = cfdate.days + cfdate.seconds/(3600.*24.)
    elif tunits == 'hours':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = cfdate.days*24. + cfdate.seconds/3600.
    elif tunits == 'minutes':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = cfdate.days*24.*60. + cfdate.seconds/60.
    elif tunits == 'seconds':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = cfdate.days*24.*3600. + cfdate.seconds
    elif tunits == 'milliseconds':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = cfdate.days*1000.*24.*3600. + cfdate.seconds*1000.
    elif tunits == 'microseconds':
        for it in range(dimt):
            cfdate = dt.datetime(times[it,0], times[it,1], times[it,2], times[it,3], times[it,4], times[it,5]) - refdate
            cfdates[it] = cfdate.days*1000000.*24.*3600. + cfdate.seconds*1000000.
    else:
        print errormsg
        print '  ' + fname + ': time units "' + tunits + '" is not ready!!!'
        quit(-1)

    return cfdates

def netCDFdatetime_realdatetime(units, tcalendar, times):
    """ Function to transfrom from netCDF CF-compilant times to real time
    """
    import datetime as dt

    txtunits = units.split(' ')
    tunits = txtunits[0]
    Srefdate = txtunits[len(txtunits) - 1]

# Calendar type
##
    is360 = False
    if tcalendar is not None:
      print '  netCDFdatetime_realdatetime: There is a calendar attribute'
      if tcalendar == '365_day' or tcalendar == 'noleap':
          print '    netCDFdatetime_realdatetime: No leap years!'
          isleapcal = False
      elif tcalendar == 'proleptic_gregorian' or tcalendar == 'standard' or tcalendar == 'gregorian':
          isleapcal = True
      elif tcalendar == '360_day':
          is360 = True
          isleapcal = False
      else:
          print errormsg
          print '    netCDFdatetime_realdatetime: Calendar "' + tcalendar + '" not prepared!'
          quit(-1)

# Does reference date contain a time value [YYYY]-[MM]-[DD] [HH]:[MI]:[SS]
##
    timeval = Srefdate.find(':')

    if not timeval == -1:
        print '  netCDFdatetime_realdatetime: refdate with time!'
        refdate = datetimeStr_datetime(txtunits[len(txtunits) - 2] + '_' + Srefdate)
    else:
        refdate = dateStr_date(Srefdate)

    dimt = len(times)
#    datetype = type(dt.datetime(1972,02,01))
#    realdates = np.array(dimt, datetype)
#    print realdates

## Not in timedelta
#  if tunits == 'years':
#    for it in range(dimt):
#      realdate = refdate + dt.timedelta(years=float(times[it]))
#      realdates[it] = int(realdate.year)
#  elif tunits == 'months':
#    for it in range(dimt):
#      realdate = refdate + dt.timedelta(months=float(times[it]))
#      realdates[it] = int(realdate.year)
#    realdates = []
    realdates = np.zeros((dimt, 6), dtype=int)
    if tunits == 'weeks':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(weeks=float(times[it]))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    elif tunits == 'days':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(days=float(times[it]))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    elif tunits == 'hours':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(hours=float(times[it]))
#            if not isleapcal:
#                Nleapdays = cal.leapdays(int(refdate.year), int(realdate.year))
#                realdate = realdate - dt.timedelta(days=Nleapdays)
#            if is360:
#                Nyears360 = int(realdate.year) - int(refdate.year) + 1
#                realdate = realdate -dt.timedelta(days=Nyears360*5)
#            realdates[it] = realdate
#        realdates = refdate + dt.timedelta(hours=float(times))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    elif tunits == 'minutes':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(minutes=float(times[it]))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    elif tunits == 'seconds':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(seconds=float(times[it]))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    elif tunits == 'milliseconds':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(milliseconds=float(times[it]))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    elif tunits == 'microseconds':
        for it in range(dimt):
            realdate = refdate + dt.timedelta(microseconds=float(times[it]))
            realdates[it,:]=[realdate.year, realdate.month, realdate.day, realdate.hour, realdate.minute, realdate.second]
    else:
        print errormsg
        print '  netCDFdatetime_realdatetime: time units "' + tunits + '" is not ready!!!'
        quit(-1)

    return realdates

class cls_seasonal_means(object):
    """ Class to compute the seasonal mean of a time series of values
    tvals=time values (year, month, day, hour, minute, second) format
    values=time-series of values
    dateref= reference date [YYYY][MM][DD][HH][MI][SS] format
    tunits= time units
    self.dimt = Number of values
    self.seasv = seasonal mean values
    self.seast = seasonal time mean values
    """
    def __init__(self, tvals, values, dateref, tunits):

        import numpy.ma as ma
        fillVal=1.e20
        fname = 'cls_seasonal_means'

        if tvals is None:
            self.dimt = None
            self.seasv = None
            self.seast = None

        else:
            tseasons=times_4seasons(tvals, True)

            dt=len(values)
            seasvals=np.ones((dt/12,4), dtype=np.float64)*fillVal
            seastimes=np.zeros((dt/12,4), dtype=np.float64)
            dates = realdatetime_CFcompilant(tvals, dateref, tunits)

            for iseas in range(4):
                for it in range(dt):
                    if tseasons[it,iseas]:
                        tfirstseas=int(it/11)
                        firstseas=True
##                        print '  first time-step of season ', iseas, ' is: ',it,' position: ',tfirstseas
                        break
                for itt in range(it, dt-1,12):
                    seasvals[int(itt/12),iseas]=(values[itt] + values[itt+1] + values[itt+2])/3.
                    seastimes[int(itt/12),iseas]=dates[itt] + (dates[itt+2] - dates[itt])/2.
##                    print itt, values[itt], values[itt+1], values[itt+2], '--->', seasvals[int(itt/12),iseas]

            self.dimt = dt/12
            self.seasv = ma.masked_equal(seasvals, fillVal)
            self.seast = ma.masked_equal(seastimes, fillVal)

class cls_seasonal_accums(object):
    """ Class to compute the seasonal accumulations of a time series of values
    tvals=time values (year, month, day, hour, minute, second) format
    values=time-series of values
    dateref= reference date [YYYY][MM][DD][HH][MI][SS] format
    tunits= time units
    self.dimt = Number of values
    self.seasv = seasonal mean values
    self.seast = seasonal time mean values
    """
    def __init__(self, tvals, values, dateref, tunits):
        import numpy.ma as ma
        fillVal=1.e20

        if tvals is None:
            self.dimt = None
            self.seasv = None
            self.seast = None

        else:
            tseasons=times_4seasons(tvals, True)

            dt=len(values)
            seasvals=np.ones((dt/12,4), dtype=np.float64)*fillVal
            seastimes=np.zeros((dt/12,4), dtype=np.float64)
            dates = realdatetime_CFcompilant(tvals, dateref, tunits)

            for iseas in range(4):
                for it in range(dt):
                    if tseasons[it,iseas]:
                        tfirstseas=int(it/11)
                        firstseas=True
#                        print '  first time-step of season ', iseas, ' is: ',it,' position: ',tfirstseas
                        break
                for itt in range(it, dt-1,12):
                    seasvals[int(itt/12),iseas]=values[itt] + values[itt+1] + values[itt+2]
                    seastimes[int(itt/12),iseas]=dates[itt] + (dates[itt+2] - dates[itt])/2.

            self.dimt = dt/12
            self.seasv = ma.masked_equal(seasvals, fillVal)
            self.seast = ma.masked_equal(seastimes, fillVal)

def seasonal_means(tvals, values):
    """ Function to compute the seasonal mean of a time series of values
    tvals=time values (year, month, day, hour, minute, second) format
    values=time-series of values
    """
    fillVal=1.e20

    tseasons=times_4seasons(tvals, True)

    dt=len(values)
    seasvals=np.ones((dt/4,4), dtype=np.float64)
    seasvals=seasvals*fillVal

    for iseas in range(4):
        for it in range(dt):
            if tseasons[it,iseas]:
                tfirstseas=int(it/11)
                firstseas=True
#                print '  first time-step of season ', iseas, ' is: ',it,' position: ',tfirstseas
                break
        for itt in range(it, dt-1,12):
            seasvals[int(itt/12),iseas]=(values[itt] + values[itt+1] + values[itt+2])/3.

    return seasvals

def seasonal_accum(tvals, values):
    """ Function to compute the seasonal accumulation of a time series of values
    tvals=time values (year, month, day, hour, minute, second) format
    values=time-series of values
    """
    fillVal=1.e20

    tseasons=times_4seasons(tvals, True)

    dt=len(values)
    seasvals=np.ones((dt/4,4), dtype=np.float64)
    seasvals=seasvals*fillVal

    for iseas in range(4):
        for it in range(dt):
            if tseasons[it,iseas]:
                tfirstseas=int(it/11)
                firstseas=True
#                print '  first time-step of season ', iseas, ' is: ',it,' position: ',tfirstseas
                break
        for itt in range(it, dt-1,12):
            seasvals[int(itt/11),iseas]=values[itt] + values[itt+1] + values[itt+2]

    return seasvals

def seasmean(timename, filename, varn):
    """ Function to compute the seasonal mean of a variable
    timename= name of the variable time in the file
    filename= netCDF file name
    varn= name of the variable
    """
    fillValue=1.e20

    ncf = NetCDFFile(filename,'a')
    ofile = 'seasmean_' + varn + '.nc'

    if not ncf.variables.has_key(varn):
        print errormsg
        print '    seasmean: File  "' + filename + '" does not have variable ' + varn + ' !!!!'
        print errormsg
        ncf.close()
        quit(-1)

    varobj = ncf.variables[varn]
    varinf = variable_inf(varobj)
    timeobj = ncf.variables[timename]
    timeinf = cls_time_information(ncf, timename)

    timevals=timeobj[:]
    netcdftimes = netCDFdatetime_realdatetime(timeinf.units + ' since ' + timeinf.Urefdate, timeinf.calendar, timevals)
    
    timeseasons=times_4seasons(netcdftimes, True)

    timeseasvals=seasonal_means(netcdftimes, timevals)
    
    ncfnew = NetCDFFile(ofile,'w')
    ncfnew.createDimension('seastime', timeinf.dimt/4)
    ncfnew.createDimension('seas', 4)

    newvarobj = ncfnew.createVariable('seastime', 'f4', ('seastime', 'seas',), fill_value=fillValue)
    newvarobj[:]=timeseasvals
    attr = newvarobj.setncattr('calendar', timeinf.calendar)
    attr = newvarobj.setncattr('units', timeinf.units)

    newvarobj = ncfnew.createVariable('seasn', str, ('seas'))
    attr = newvarobj.setncattr('standard_name', 'seasn')
    attr = newvarobj.setncattr('long_name', 'name of the season')
    attr = newvarobj.setncattr('units', '3-months')
    newvarobj[0]='DJF'
    newvarobj[1]='MAM'
    newvarobj[2]='JJA'
    newvarobj[3]='SON'

    newdims=[]
    idim = 0

    for ndim in varinf.dimns:
        if ndim == timename:
            newdims.append(unicode('seastime'))
            if not idim == 0:
                print errormsg
                print '  seasmean: File  "' + filename + '" does not have time dimension "' + timename + '" as the first one. It has it as # ', idim, ' !!!!'
                print errormsg
                ncf.close()
                quit(-1)
        else:
            newdims.append(ndim)

        if not ncfnew.dimensions.has_key(ndim):
            ncfnew.sync()
            ncfnew.close()
            ncf.close()            

            fdimadd(filename + ':' + ndim, ofile)
            ncf = NetCDFFile(filename,'r')
            ncfnew = NetCDFFile(ofile,'a')

    print ncfnew.dimensions
    namedims=tuple(newdims)
    print namedims

    varobj = ncf.variables[varn]
    varinf = variable_inf(varobj)

    newvarobj=ncfnew.createVariable(varn + '_seasmean', 'f4', namedims, fill_value=fillValue)
    attr = newvarobj.setncattr('standard_name', varn + '_seasmean')
    attr = newvarobj.setncattr('long_name', 'seasonal mean of ' + varn)
    attr = newvarobj.setncattr('units', varinf.units)

    newvar = np.ones(varinf.dims[0], dtype=varinf.dtype)
    if varinf.Ndims == 1:
        newvar = newvarobj[:]
        newvarobj[:] = seasonal_means(netcdftimes, newvar)
    if varinf.Ndims == 2:
        for i in range(varinf.dims[1]):
            newvar = newvarobj[:,i]
            newvarobj[:,i] = seasonal_means(netcdftimes, newvar)
    if varinf.Ndims == 3:
        for i in range(varinf.dims[1]):
            for j in range(varinf.dims[2]):
                newvar = newvarobj[:,i,j]
                newvarobj[:,i,j] = seasonal_means(netcdftimes, newvar)
    if varinf.Ndims == 4:
        for i in range(varinf.dims[1]):
            for j in range(varinf.dims[2]):
                for k in range(varinf.dims[3]):
                    newvar = newvarobj[:,i,j,k]
                    newvarobj[:,i,j,k] = seasonal_means(netcdftimes, newvar)
    if varinf.Ndims == 5:
        for i in range(varinf.dims[1]):
            for j in range(varinf.dims[2]):
                for k in range(varinf.dims[3]):
                    for l in range(varinf.dims[4]):
                        newvar = newvarobj[:,i,j,k,l]
                        newvarobj[:,i,j,k,l] = seasonal_means(netcdftimes, newvar)
    if varinf.Ndims == 6:
        for i in range(varinf.dims[1]):
            for j in range(varinf.dims[2]):
                for k in range(varinf.dims[3]):
                    for l in range(varinf.dims[4]):
                        for m in range(varinf.dims[5]):
                            newvar = newvarobj[:,i,j,k,l,m]
                            newvarobj[:,i,j,k,l,m] = seasonal_means(netcdftimes, newvar)
    else:
        print errormsg
        print '  seasmean: number of dimensions ', varinf.Ndims, ' not ready !!!!'
        print errormsg
        ncf.close()
        quit(-1)

    ncfnew.sync()
    ncfnew.close()

    print 'seasmean: Seasonal mean file "' + ofile + '" written!'

def load_variable_lastdims(var, prevdims, Nlastdims):
    """ Function to load the last [Nlastdims] dimensions of a variable [var] at the other dimensions at [prevdims]
    >>> load_variable_lastdims(np.array(range(5*5*5)).reshape(5,5,5), [1,2], 1)
    [35 36 37 38 39]
    """
    fname='load_variable_lastdims'

    dims=var.shape
    Ndims=len(dims)

    Nprevdims = len(prevdims)
    if not Nprevdims + Nlastdims == Ndims:
        print erromsg
        print '  ' + fname + ': number of dimensions previous (',Nprevdim,') and last (',Nlastdims, ') does not match with variable size: ',Ndims,' !!!!'
        print errormsg
        quit(-1)

    if Nlastdims > Ndims-1:
        print errormsg
        print '  ' + fname + ': number of last dimensions ', Nlastdims,' >= than the number of dimensions of the variable ', Ndims,' !!!!'
        print errormsg
        quit(-1)

    if Ndims == 1:
        loadvar = var[:]
    elif Ndims == 2:
        loadvar = var[prevdims[0], :]
    elif Ndims == 3:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], :]
        else:
            loadvar = var[prevdims[0], :, :]
    elif Ndims == 4:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], :]
        elif Nlastdims == 2:
            loadvar = var[prevdims[0], prevdims[1], :, :]
        else:
            loadvar = var[prevdims[0], :, :, :]
    elif Ndims == 5:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], :]
        elif Nlastdims == 2:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], :, :]
        elif Nlastdims == 3:
            loadvar = var[prevdims[0], prevdims[1], :, :, :]
        else:
            loadvar = var[prevdims[0], :, :, :, :]
    elif Ndims == 6:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], prevdims[4], :]
        elif Nlastdims == 2:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], :, :]
        elif Nlastdims == 3:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], :, :, :]
        elif Nlastdims == 4:
            loadvar = var[prevdims[0], prevdims[1], :, :, :, :]
        else:
            loadvar = var[prevdims[0], :, :, :, :]
    else:
        print errormsg
        print '  ' + fname + ' variable size ', Ndims, ' not ready!!!'
        print errormsg
        quit(-1)

    return loadvar

def load_ncvariable_lastdims(ncf, varn, prevdims, Nlastdims):
    """ Function to load the last [Nlastdims] dimensions of a variable [varn] from a netCDF object at the other dimensions at [prevdims]
    ncf= netCDF object
    varn= variable name
    prevdims= values at the previous dimensions
    Nlastims= number of last dimensions
    """
    fname='load_ncvariable_lastdims'

    var=ncf.variables[varn]

    dims=var.shape
    Ndims=len(dims)

    Nprevdims = len(prevdims)
    if not Nprevdims + Nlastdims == Ndims:
        print erromsg
        print '  ' + fname + ': number of dimensions previous (',Nprevdim,') and last (',Nlastdims, ') does not match with variable size: ',Ndims,' !!!!'
        print errormsg
        quit(-1)

    if Nlastdims > Ndims-1:
        print errormsg
        print '  ' + fname + ': number of last dimensions ', Nlastdims,' >= than the number of dimensions of the variable ', Ndims,' !!!!'
        print errormsg
        quit(-1)

    if Ndims == 1:
        loadvar = var[:]
    elif Ndims == 2:
        loadvar = var[prevdims[0], :]
    elif Ndims == 3:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], :]
        else:
            loadvar = var[prevdims[0], :, :]
    elif Ndims == 4:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], :]
        elif Nlastdims == 2:
            loadvar = var[prevdims[0], prevdims[1], :, :]
        else:
            loadvar = var[prevdims[0], :, :, :]
    elif Ndims == 5:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], :]
        elif Nlastdims == 2:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], :, :]
        elif Nlastdims == 3:
            loadvar = var[prevdims[0], prevdims[1], :, :, :]
        else:
            loadvar = var[prevdims[0], :, :, :, :]
    elif Ndims == 6:
        if Nlastdims == 1:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], prevdims[4], :]
        elif Nlastdims == 2:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], :, :]
        elif Nlastdims == 3:
            loadvar = var[prevdims[0], prevdims[1], prevdims[2], :, :, :]
        elif Nlastdims == 4:
            loadvar = var[prevdims[0], prevdims[1], :, :, :, :]
        else:
            loadvar = var[prevdims[0], :, :, :, :]
    else:
        print errormsg
        print '  ' + fname + ' variable size ', Ndims, ' not ready!!!'
        print errormsg
        quit(-1)

    return loadvar

def fill_ncvariable_lastdims(ncf, varn, prevdims, Nlastdims, fillvals):
    """ Function to fill the last [Nlastdims] dimensions of a variable [varn] from a netCDF object at the other dimensions at [prevdims]
      with as given set of [fillvals] values
    ncf= netCDF object
    varn= variable name
    prevdims= values at the previous dimensions
    Nlastims= number of last dimensions
    fillvals= values to use to fill
    """
    fname='fill_ncvariable_lastdims'

    var=ncf.variables[varn]

    dims=var.shape
    Ndims=len(dims)

    Nprevdims = len(prevdims)
    if not Nprevdims + Nlastdims == Ndims:
        print erromsg
        print '  ' + fname + ': number of dimensions previous (',Nprevdim,') and last (',Nlastdims, ') does not match with variable size: ',Ndims,' !!!!'
        print errormsg
        quit(-1)

    if Nlastdims > Ndims-1:
        print errormsg
        print '  ' + fname + ': number of last dimensions ', Nlastdims,' >= than the number of dimensions of the variable ', Ndims,' !!!!'
        print errormsg
        quit(-1)

    if Ndims == 1:
        var[:] = fillvals
    elif Ndims == 2:
        var[prevdims[0], :] = fillvals
    elif Ndims == 3:
        if Nlastdims == 1:
            var[prevdims[0], prevdims[1], :] = fillvals
        else:
            var[prevdims[0], :, :] = fillvals
    elif Ndims == 4:
        if Nlastdims == 1:
            var[prevdims[0], prevdims[1], prevdims[2], :] = fillvals
        elif Nlastdims == 2:
            var[prevdims[0], prevdims[1], :, :] = fillvals
        else:
            var[prevdims[0], :, :, :] = fillvals
    elif Ndims == 5:
        if Nlastdims == 1:
            var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], :] = fillvals
        elif Nlastdims == 2:
            var[prevdims[0], prevdims[1], prevdims[2], :, :] = fillvals
        elif Nlastdims == 3:
            var[prevdims[0], prevdims[1], :, :, :] = fillvals
        else:
            var[prevdims[0], :, :, :, :] = fillvals
    elif Ndims == 6:
        if Nlastdims == 1:
            var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], prevdims[4], :] = fillvals
        elif Nlastdims == 2:
            var[prevdims[0], prevdims[1], prevdims[2], prevdims[3], :, :] = fillvals
        elif Nlastdims == 3:
            var[prevdims[0], prevdims[1], prevdims[2], :, :, :] = fillvals
        elif Nlastdims == 4:
            var[prevdims[0], prevdims[1], :, :, :, :] = fillvals
        else:
            var[prevdims[0], :, :, :, :] = fillvals
    else:
        print errormsg
        print '  ' + fname + ' variable size ', Ndims, ' not ready!!!'
        print errormsg
        quit(-1)

def maskvar(values, filename, varn):
    """ Function to mask a variable using a mask. It is assumed that mask[...,dimM,dimJ,dimK] and 
      var[...,dimM,dimJ,dimK] share the last dimensions
    values= [maskfilename]:[maskvar]:[dimsmask]:[timename]
      [maskfilename]= file with the mask
      [maskvar]= name of the variable with the mask
      [dimsmask]= dimensions d1,d2,d3,... to use from variable [mask] (ahead of the sharing ones with [var])
      [timename]= name of the variable time in [filename]
    filename= netCF file name
    varn= variable name
    """
    fname='maskvar'

    fillValue=1.e20
    ofile = 'mask_' + varn + '.nc'

    maskfilename = values.split(':')[0]
    maskvar = values.split(':')[1]
    maskdims = np.array(values.split(':')[2].split(','), dtype=int)
    timename = values.split(':')[3]

    ncf = NetCDFFile(filename,'r')

    if not ncf.variables.has_key(varn):
        print errormsg
        print '    ' + fname + ': File  "' + filename + '" does not have variable "' + varn + '" !!!!'
        print errormsg
        ncf.close()
        quit(-1)

    varobj = ncf.variables[varn]
    varinf = variable_inf(varobj)
    timeobj = ncf.variables[timename]
    timeinf = cls_time_information(ncf, timename)

    ncfmask = NetCDFFile(maskfilename,'r')

    if not ncfmask.variables.has_key(maskvar):
        print errormsg
        print '    ' + fname + ': File  "' + maskfilename + '" does not have variable mask "' + maskvar + '" !!!!'
        print errormsg
        ncf.close()
        ncfmask.close()
        quit(-1)

    varmaskobj = ncfmask.variables[maskvar]
    varmaskinf = variable_inf(varmaskobj)

    sdims=[]
    for ivdim in range(varinf.Ndims):
        vardim = varinf.Ndims - ivdim - 1
        maskdim = varmaskinf.Ndims - ivdim - 1
        if varinf.dims[vardim] == varmaskinf.dims[vardim]:
            sdims.append(varinf.dims[vardim])
        else:
            break

    samedims = sdims[::-1]
    print '  ' + fname + ': variable: ', varinf.dims, ' and mask: ', varmaskinf.dims,' share: ', samedims

# masking
##
    Nsamedims=len(samedims)

    if not Nsamedims + len(maskdims) == varmaskinf.Ndims:
        print errormsg
        print '  ' + fname + ': desired dimensions of mask ', maskdims, ' and coincident dims with variable ',Nsamedims, ' does not match mask dims ',varmaskinf.Ndims,' !!!!'
        print errormsg
        ncf.close()
        ncfmask.close()
        quit(-1)

    if varmaskinf.Ndims > Nsamedims:
        maskvals = load_ncvariable_lastdims(ncfmask, maskvar, maskdims, Nsamedims)
    else:
        maskvals = maskvarobj[:]

    ncfmask.close()

# Creation of the new file
##
    ncfnew = NetCDFFile(ofile,'w')
   
    for idim in range(varinf.Ndims):
        newdimv = ncf.dimensions[varinf.dimns[idim]]
        if newdimv.isunlimited():
            ncfnew.createDimension(varinf.dimns[idim], None)
        else:
            ncfnew.createDimension(varinf.dimns[idim], varinf.dims[idim])

    newvar = ncfnew.createVariable(varn, varinf.dtype, varinf.dimns, fill_value=fillValue)
    for attrn in varinf.attributes:
        if not attrn == '_FillValue':
            attr = newvar.setncattr(attrn, varobj.getncattr(attrn))

    attr = newvar.setncattr('mask', 'variable masked using ' + values.split(':')[2] + ' of variable ' + maskvar + ' from file ' + maskfilename)

    ncfnew.sync()
    ncfnew.close()

    for varns in ncf.variables:
        if not varns == varn:
            fvaradd(filename + ':' + varns, ofile)

    ncfnew = NetCDFFile(ofile,'a')

    Nptmask = 1
    for isdim in range(Nsamedims):
        Nptmask = Nptmask * samedims[isdim]

    Nvarloop = 1
    varsecdims = []
    for insdim in range(varinf.Ndims - Nsamedims):
        Nvarloop = Nvarloop * varinf.dims[insdim]
        varsecdims.append(0)

    Nsecdims = len(varsecdims)

    for iloop in range(Nvarloop):
        varval = load_ncvariable_lastdims(ncf, varn, varsecdims, Nsamedims)
        newvarval = np.where(maskvals == 1, varval, fillValue)
        if np.sum(newvarval == fillValue) == Nptmask:
            print errormsg
            print '  ' + fname + ': all values got masked!!!'
            print erromsg
            quit(-1)

        fill_ncvariable_lastdims(ncfnew,varn,varsecdims,Nsamedims,newvarval)

        varsecdims[Nsecdims -1] = varsecdims[Nsecdims - 1] + 1
        if varsecdims[Nsecdims - 1] > varinf.dims[Nsecdims - 1] - 1:
            varsecdims[Nsecdims - 1] = 0
            for jdim in range(Nsecdims - 1):
                varsecdims[Nsecdims - 2 - jdim] = varsecdims[Nsecdims - 2 - jdim] + 1
                if varsecdims[Nsecdims - 2 - jdim] <= varinf.dims[Nsecdims - 2 - jdim] - 1:
                    break
                else:
                    varsecdims[Nsecdims - 2 - jdim] = 0
                    
    print 'variable: ', varinf.dims, ' and mask: ', varmaskinf.dims,' share: ', samedims, Nvarloop

    ncfnew.sync()
    ncfnew.close()
    ncf.close()

    print 'masked file "' + ofile + '" generated !!!'

def lonlat2D(ncfobj, lonn, latn):
    """ Function to create 2D matricies of longitudes and latitudes from the file
    ncfobj= netCDF object file
    lonn= longitude name
    latn= latitude name
    """
    fname='lonlat2D'

    lonvar = ncfobj.variables[lonn]
    latvar = ncfobj.variables[latn]

    loninf = variable_inf(lonvar)
    latinf = variable_inf(latvar)

    if loninf.Ndims == 1:
        lonv = lonvar[:]
        latv = latvar[:]
        dx=loninf.dims[0]
        dy=latinf.dims[0]
        lonval = np.zeros((dy, dx), dtype=float)    
        latval = np.zeros((dy, dx), dtype=float)    
        for iy in range(dy):
            lonval[iy,:] = lonv
        for ix in range(dx):
            latval[:,ix] = latv
    elif loninf.Ndims == 2:
        lonval = lonvar[:]
        latval = latvar[:]
    elif loninf.Ndims == 3:
        lonval = lonvar[0,:,:]
        latval = latvar[0,:,:]
    else:
        print errormsg
        print '  ' + fname + ' dimension ', lonvar.Ndims, ' of the lon/lat matrices not ready !!!!'

    return lonval, latval

def indices_mask(mask):
    """ Function to retrieve the indices from a 2D mask
    mask= 2D mask matrix
    mat=np.array(range(100), dtype=float).reshape((10,10))
    >>> mask1= mat >= 45. 
    >>> mask2= mat <= 55.
    >>> mask = mask1 * mask2
    indices_mask(mask)
    [[4 5]
    [4 6]
    [4 7]
    [4 8]
    [4 9]
    [5 0]
    [5 1]
    [5 2]
    [5 3]
    [5 4]
    [5 5]]
    """
    dy=mask.shape[0]
    dx=mask.shape[1]

    Nptmask = np.sum(mask)
    maskindices = np.zeros((Nptmask,2), dtype=int)
    ij=0
    for j in range(dy):
        for i in range(dx):
            if mask[j,i] == True:
                maskindices[ij,0] = j
                maskindices[ij,1] = i
                ij = ij + 1

    return maskindices

def chgtimestep(values, origfile, varN):
    """ Function to change the values of a given time-step of a variable inside a netCDF for values from a nother file
    [values]=[origtime_step]:[newfile]:[newtime_step]
      [origtime_step]: desired original time step to change: 'first', 'last', 'middle', '[num]'
      [newfile]: new netCDF file name
      [newtime_step]: desired new time step to change: 'first', 'last', 'middle', '[num]' 'zeros', 'fillVal'
        'first': first time-step
        'last': last time-step
        'middle': middle time-step
        '[num]': [num] time-step
        'zeros': fill time-step with zeros
        'fillVal': fill time-step with fill_Values [FillValue]
    [varN]= variable name
    [origfile]= original netCDF file name
    """
    fname = 'chgtimestep'
    ermsg = fname + ': ERROR -- error -- ERROR -- error'

    vals = values.split(':')
    origtime_step = vals[0]
    newfile = vals[1]
    newtime_step = vals[2]
    if len(vals) == 4:
       FillValue = np.float(vals[3])

    filexist(origfile, ermsg, 'original')
    filexist(newfile, ermsg, 'new')

    print 'origfile: ',origfile
    ncofile = NetCDFFile(origfile,'a')
    varinfile(ncofile, origfile, ermsg, 'change', varN)

    ncnfile = NetCDFFile(newfile,'r')
    varinfile(ncofile, newfile, ermsg, 'new', varN)
    ncnfile.close()

    if newtime_step == 'zeros':
        newvals = uploading_timestep(newfile, varN, 'first')
        newvals = 0.
    elif newtime_step == 'fillVal':
        newvals = uploading_timestep(newfile, varN, 'first')
        newvals = FillValue
    else:
        newvals = uploading_timestep(newfile, varN, newtime_step)

    varobj = ncofile.variables[varN]
    Ndims = len(varobj.shape)

    dimt = varobj.shape[0]
    if origtime_step == 'first':
        oval = 0
    elif origtime_step == 'middle':
        oval = int(dimt/2)
    elif origtime_step == 'last':
        oval = dimt-1
    else:
        oval = int(origtime_step)

    if Ndims == 1:
        varobj[oval] = newvals
    elif Ndims == 2:
        varobj[oval,:] = newvals
    elif Ndims == 3:
        varobj[oval,:,:] = newvals
    elif Ndims == 4:
        varobj[oval,:,:,:] = newvals
    elif Ndims == 5:
        varobj[oval,:,:,:,:] = newvals
    elif Ndims == 6:
        varobj[oval,:,:,:,:,:] = newvals
    else:
        print ermsg
        print '  variable size', Ndims, 'not ready !'
        print ermsg
        quit(-1)

    ncofile.sync()
    ncofile.close()

def uploading_timestep(ncfile, varN, time_step):
    """ Function to upload a given time-step of a variable inside a netCDF
    [ncfile]= netCDF file name
    [varN]= variable name
    [time_step]= desired time step: 'first', 'last', 'middle', '[num]'
      'first': first time-step
      'last': last time-step
      'middle': middle time-step
      '[num]': [num] time-step
    """
    fname = 'uploading_timestep'
    ermsg = fname + ': ERROR -- error -- ERROR -- error'

    filexist(ncfile, ermsg, 'uploading')
    ncufile = NetCDFFile(ncfile,'r')

    varinfile(ncufile, ncfile, ermsg, 'uploading', varN)

    varobj = ncufile.variables[varN]

    Ndims = len(varobj.shape)

    dimt = varobj.shape[0]
    if time_step == 'first':
        upval = 0
    elif time_step == 'middle':
        upval = int(dimt/2)
    elif time_step == 'last':
        upval = dimt-1
    else:
        upval = int(time_step)

    if Ndims == 1:
        uploadvar = varobj[upval]
    elif Ndims == 2:
        uploadvar = varobj[upval,:]
    elif Ndims == 3:
        uploadvar = varobj[upval,:,:]
    elif Ndims == 4:
        uploadvar = varobj[upval,:,:,:]
    elif Ndims == 5:
        uploadvar = varobj[upval,:,:,:,:]
    elif Ndims == 6:
        uploadvar = varobj[upval,:,:,:,:,:]
    else:
        print ermsg
        print '  variable size', Ndims, 'not ready !'
        print ermsg
        quit(-1)

    return uploadvar

def timeshiftvar(values, fileN, varN, FillVal=1.e20):
    """ Function to temporaly shift a number of time-steps a given variable inside a netCDF file
    [values]=[nsteps]:[[FillValue]]
      [nsteps]: desired number of time step to move
      [FillValue]: value to fill the empty time-steps
    [fileobj]= original netCDF object
    [varN]= variable name
    """
    from sys import stdout

    fname = 'timesfhitvar'
    ermsg = fname + ': ERROR -- error -- ERROR -- error'

    nsteps = int(values.split(':')[0])
    if len(values.split(':')) == 2:
        FillVal = np.float(values.split(':')[1])

    fileobj = NetCDFFile(fileN,'a')

    varobj = fileobj.variables[varN]
    vardims = varobj.shape
    vartype = varobj.dtype
    Ndims = len(vardims)

    dimt = vardims[0]
    if nsteps > 0:
        inimov = nsteps
        endmov = dimt
        iniplace = 0
        endplace = dimt - nsteps 
    else:
        inimov = 0
        endmov = dimt + nsteps 
        iniplace = -nsteps
        endplace = dimt

    print '  ' + fname + ': moving', nsteps, '___ __ _'
    print '    from (', inimov, ',', endmov, ') to (',iniplace,',',endplace,')' 

    if Ndims == 1:
        vals = np.ones(vardims, dtype=vartype)*FillVal
        vals[iniplace:endplace] = varobj[inimov:endmov]
        varobj[:] = vals
    elif Ndims == 2:
        vals = np.ones(vardims, dtype=vartype)*FillVal
        vals[iniplace:endplace,:] = varobj[inimov:endmov,:]
        varobj[:] = vals
    elif Ndims == 3:
        vals = np.ones((vardims[0], vardims[1]), dtype=vartype)*FillVal
        for i in range(vardims[2]):
            if (i%10 == 0): 
                print '\r  shifted ' + '{0:5.2g}'.format(i*100./vardims[2]) + ' %',
                stdout.flush()

            vals[iniplace:endplace,:] = varobj[inimov:endmov,:,i]
            varobj[:,:,i] = vals
    elif Ndims == 4:
        vals = np.ones((vardims[0], vardims[1]), dtype=vartype)*FillVal
        for i in range(vardims[2]):
            for j in range(vardims[3]):
                nvals=i*vardims[3] + j
                if (nvals%100 == 0): 
                    print '\r  shifted ' + '{0:5.2g}'.format(nvals*100./(vardims[2]*vardims[3])) + ' %',
                    stdout.flush()

                vals[iniplace:endplace,:] = varobj[inimov:endmov,:,i,j]
                varobj[:,:,i,j] = vals
    elif Ndims == 5:
        vals = np.ones((vardims[0], vardims[1]), dtype=vartype)*FillVal
        for i in range(vardims[2]):
            for j in range(vardims[3]):
                for k in range(vardims[4]):
                    nvals=i*vardims[3]*vardims[4] + j*vardims[4] + k
                    if (nvals%1000 == 0): 
                        print '\r  shifted ' + '{0:5.2g}'.format(nvals*100./(vardims[2]*vardims[3]*vardims[4])) + ' %',
                        stdout.flush()

                    vals[iniplace:endplace,:] = varobj[inimov:endmov,:,i,j,k]
                    varobj[:,:,i,j,k] = vals
    elif Ndims == 6:
        vals = np.ones((vardims[0], vardims[1]), dtype=vartype)*FillVal
        for i in range(vardims[2]):
            for j in range(vardims[3]):
                for k in range(vardims[4]):
                    for l in range(vardims[5]):
                        nvals=i*vardims[3]*vardims[4]*vardims[5] + j*vardims[4]*vardims[5] + k*vardims[5] + l
                        if (nvals%10000 == 0): 
                            print '\r  shifted ' + \
                             '{0:5.2g}'.format(nvals*100./(vardims[2]*vardims[3]*vardims[4]*vardims[5])) + ' %',
                            stdout.flush()

                        vals[iniplace:endplace,:] = varobj[inimov:endmov,:,i,j,k,l]
                        varobj[:,:,i,j,k,l] = vals
    else:
        print ermsg
        print '  variable size', Ndims, 'not ready !'
        print ermsg
        quit(-1)

    print stdout.write("\n")

    fileobj.sync()
    fileobj.close()

def typemod(value, typeval):
    """ Function to give back a value in a given dtype
    >>> print(typemod(8.2223, 'np.float64'))
    <type 'numpy.float64'>
    >>> print(typemod(8.2223, 'tuple'))
    <type 'tuple'>
    """
    fname='typemod'

    if typeval == 'int':
        return int(value)
    elif typeval == 'long':
        return long(value)
    elif typeval == 'float':
        return float(value)
    elif typeval == 'complex':
        return complex(value)
    elif typeval == 'str':
        return str(value)
    elif typeval == 'bool':
        return bool(value)
    elif typeval == 'list':
        newval = []
        newval.append(value)
        return newval
    elif typeval == 'dic':
        newval = {}
        newval[value] = value
        return newval
    elif typeval == 'tuple':
        newv = []
        newv.append(value)
        newval = tuple(newv)
        return newval
    elif typeval == 'np.int8':
        return np.int8(value)
    elif typeval == 'np.int16':
        return np.int16(value)
    elif typeval == 'np.int32':
        return np.int32(value)
    elif typeval == 'np.int64':
        return np.int64(value)
    elif typeval == 'np.uint8':
        return np.uint8(value)
    elif typeval == 'np.uint16':
        return np.uint16(value)
    elif typeval == 'np.np.uint32':
        return np.uint32(value)
    elif typeval == 'np.uint64':
        return np.uint64(value)
    elif typeval == 'np.float':
        return np.float(value)
    elif typeval == 'np.float16':
        return np.float16(value)
    elif typeval == 'np.float32':
        return np.float32(value)
    elif typeval == 'float32':
        return np.float32(value)
    elif typeval == 'np.float64':
        return np.float64(value)
    elif typeval == 'float64':
        return np.float64(value)
    elif typeval == 'np.complex':
        return np.complex(value)
    elif typeval == 'np.complex64':
        return np.complex64(value)
    elif typeval == 'np.complex128':
        return np.complex128(value)
    else:
        print errormsg
        print fname + ':  data type "' + typeval + '" is not ready !'
        print errormsg
        quit(-1)

def addvals(values, filename, varN):
    """ Function to add values to a given variable at a given dimension
    values = [dimension]:[position]:[Nvals]:[addval]
      [dimension]: dimension at which the variable wants to be increased
      [position]: position within the dimension at which the variable wants to be increased
        'first': first position of the dimension
        'middle': middle position of the dimension
        'last': last position of the dimension
        'num': before given value
      [Nvals]: number of values
      [addval]: value to give to the added values
        'val': a given value
        'missing': as a missing value
    filename = name of the netCDF file
    varN = name of the variable
    """
    import subprocess as sub
    fname = 'addvals'
    newf = 'addvals.nc'

    dimension=int(values.split(':')[0])
    position=values.split(':')[1]
    Nvals=int(values.split(':')[2])
    addval=values.split(':')[3]

    filexist(filename, errormsg, 'add values')
    ncfa = NetCDFFile(filename,'r')

    varinfile(ncfa, filename, errormsg, 'adding', varN)
    varobj=ncfa.variables[varN]

    varinf=variable_inf(varobj)

    newdims=list(varinf.dims)
    newdims[dimension] = newdims[dimension] + Nvals
    newdims=tuple(newdims)

    dmodname = varinf.dimns[dimension]
    print '      ' + fname + ' increassed dimension "' + dmodname + '"'

    if position == 'first':
        pos=0
    elif position == 'middle':
        pos=(varinf.dims[dimension])/2
    elif position == 'last':
        pos=varinf.dims[dimension]
    else:
        pos=int(position)
    
    if pos > varinf.dims[dimension]:
        print errormsg
        print fname + ': position given ', pos,' is too big for the size of the dimension ',varinf.dims[dimension], '!!!!'
        print errormsg
        quit(-1)

##    print 'starting position: ', pos

    if addval == 'missing':
        addvaln = 1.e20
    else:
        addvaln = addval
    addvalv=typemod(addvaln, str(varinf.dtype))

    newnc = NetCDFFile(newf,'w')

# Dimensions
## 
    for dim in ncfa.dimensions.values():
        dname = dim._name
        dsize = dim.__len__()
        if dim.isunlimited():
            dsize=None 
        else:
            if dname == varinf.dimns[dimension]:
                dsize=newdims[dimension]

        newnc.createDimension(dname, size=dsize)

# Variables
## 
    for var in ncfa.variables.values():
        vname = var._name
        vkind = var.dtype
        vdims = var.dimensions

        varattrs = var.ncattrs()
        if searchInlist(varattrs, '_FillValue'):
            fillval = var.getncattr('_FillValue')
            newvarobj = newnc.createVariable(vname, vkind, vdims, fill_value=fillval)
        elif vname == varN and addval == 'missing':
            newvarobj = newnc.createVariable(vname, vkind, vdims, fill_value=typemod(addvaln, str(varinf.dtype)))
        else:
            newvarobj = newnc.createVariable(vname, vkind, vdims)
         
        varobj=ncfa.variables[vname]
        vals = varobj[:]

        if vname == varN:
             for ival in range(Nvals):
##                 print '      ' + fname + ': adding',ival+1,' values'
                 newvar = np.insert(vals, pos, addvalv, axis=dimension)
                 vals=newvar.copy()
             newvarobj[:] = newvar
        else:
            if vname == dmodname:
# Increassing dimension variable values
##
                dvals=ncfa.variables[dmodname][:]
                if position == 'first':
                    valB = -(dvals[1] - dvals[0])
                    valA=dvals[pos] + valB
                elif position == 'last':
                    valB=-(dvals[1] - dvals[0])
                    valA=dvals[pos-1] - valB*Nvals
                else:
                    valB=-(dvals[pos] - dvals[pos-1])/(Nvals+1)
                    valA=dvals[pos-1] - valB*Nvals

                for ival in range(Nvals):
                     addvalvar=valA + valB*ival
##                     print '    ' + fname + ' adding to dimension variable "', dmodname ,'" value ',addvalvar
                     newvar = np.insert(vals, pos, addvalvar, axis=0)
                     vals=newvar.copy()
                newvarobj[:] = newvar

            else:
                vardims=list(var.dimensions)
                if searchInlist(vardims,varinf.dimns[dimension]):
##                    print '    ' + fname + ' variable ' + vname + ' has also the increased dimension!'
                    vardim=vardims.index(varinf.dimns[dimension])
                    for ival in range(Nvals):
                        newvar = np.insert(vals, pos, addvalvar, axis=vardim)
                        vals=newvar.copy()
                    newvarobj[:] = newvar
                else:
                    newvarobj[:] = vals

        for vattr in varobj.ncattrs():
            if not vattr == '_FillValue':
                vattrval = var.getncattr(vattr)
                newvarobj = set_attribute(newvarobj, vattr, vattrval)

# Global attributes
##
    for gattr in ncfa.ncattrs():
        gattrval = ncfa.getncattr(gattr)
        newnc = set_attribute(newnc, gattr, gattrval)

    newnc.sync()
    newnc.close()
    ncfa.close()
    try:
        ins = 'cp ' + newf + ' ' + filename
        syscode = sub.call(ins, shell=True)
 
        if not syscode == 0:
            print errmsg
            print "  Copy to the new file failed!"
            print ins
            print syscode
            quit()
        else:
            sub.call('rm ' + newf, shell=True)

    except OSError, e:
        print errmsg
        print "   Copy failed!"
        print e
        quit(-1)    

def TimeInf(filename, tname):
    """ Function to print all the information from the variable time
    filename= netCDF file name
    tname= name of the variable time
    """

    ncfobj = NetCDFFile(filename,'r')
    timeinf = cls_time_information(ncfobj, tname)
    fmtprinting_class(timeinf)
    ncfobj.close()

def sorttimesmat(filename, tname):
    """ Function to sort the time values of a given file
    tname, name of the variable time
    filename= netCDF file name
    """    

    fname = 'sorttimesmat'
  
    filexist(filename, errormsg, 'to sort time')
    ncft = NetCDFFile(filename,'a')

    varinfile(ncft, filename, errormsg, 'time', tname)
    timeobj = ncft.variables[tname]

    timeinf = cls_time_information(ncft, tname)
    tvals = timeobj[:]

    Ltvals = list(tvals)

    sortedtvals = sorted(Ltvals)

    tinds = np.zeros((timeinf.dimt), dtype=int)

    for it in range(timeinf.dimt):
        tinds[it] = Ltvals.index(sortedtvals[it])

    for var in ncft.variables:
        varobj = ncft.variables[var]
        if searchInlist(varobj.dimensions, tname):
            print fname + ': time-sorting variable: "' + var + '" '
            varvals = varobj[:]
            newvarvals = varvals.copy()
            for it in range(timeinf.dimt):
                if var == 'time':
                    print it, tinds[it], tvals[tinds[it]]
                newvarvals[it,] = varvals[tinds[it],]

            varobj[:] = newvarvals

    ncft.sync()
    ncft.close()

def diffdate_units(diffdate, units):
    """ Function to transform a difference of dates to a certain units
    diffdate = difference of dates (as timedelta)
    units = units to transform: 'weeks', 'days', 'hours', 'minutes', 'seconds', 'miliseconds', 'nanoseconds'
    >>> diffdate_units(dt.datetime(1976, 2, 17, 8, 32, 5) - dt.datetime(1949, 12, 1, 0, 0, 0), 'seconds')
    827224325.0
    """
    fname = 'diffdate_units'

    if units == 'weeks':
        diffunits = diffdate.days/7. + diffdate.seconds/(3600.*24.*7.)
    elif units == 'days':
        diffunits = diffdate.days + diffdate.seconds/(3600.*24.)
    elif units == 'hours':
        diffunits = diffdate.days*24. + diffdate.seconds/(3600.)
    elif units == 'minutes':
        diffunits = diffdate.days*24.*60. + diffdate.seconds/(60.)
    elif units == 'seconds':
        diffunits = diffdate.days*24.*3600. + diffdate.seconds
    elif units == 'miliseconds':
        diffunits = diffdate.days*24.*3600.*1000. + diffdate.seconds*1000.
    elif units == 'nanoseconds':
        diffunits = diffdate.days*24.*3600.*1000000. + diffdate.seconds*1000000.
    else:
        print errormsg
        print fname + ': time units "' + units + '" not ready!!!'
        print errormsg
        quit(-1)

    return diffunits

def days_period(yeari,yearf):
    """ Function to give the number of days for a period
    >>> days_period(1976,1980)
    1827
    """

    ndays=0    
    for iyr in range(yearf-yeari+1):
        ndays=ndays+days_year(yeari+iyr)

    return ndays

def check_times_file(values, filename, timen):
    """ Function to check time-steps of a given file
    values=[FirstDate]:[LastDate][freq]:[auxmonth]
      [FirstDate] = first date within file (in [YYYY][MM][DD][HH][MI][SS] format)
      [LastDate] = last date within file (in [YYYY][MM][DD][HH][MI][SS] format)
      [freq] = frequency of the data within the file (in time units) or 'month' (monthly values)
      [auxmonth] = in case of 'month'
        midmon: dates are given according to the mid time value of the month
        real: dates are given increassing a month from the previous date
    filename: name of the file
    timen: name of the variable time
    """
    import datetime as dt
    fname = 'check_times_file'

    FirstDate = values.split(':')[0]
    LastDate = values.split(':')[1]
    freq = values.split(':')[2]

    ncft = NetCDFFile(filename, 'r')

    varinfile(ncft, filename, errormsg, '', timen)

    timeobj=ncft.variables[timen]
    timeinf = cls_time_information(ncft, timen)

    timevals = timeobj[:]
    ncft.close()

    yrf=int(FirstDate[0:4])
    monf=int(FirstDate[4:6])
    dayf=int(FirstDate[6:8])
    horf=int(FirstDate[8:10])
    minf=int(FirstDate[10:12])
    secf=int(FirstDate[12:14])
 
    firstdate = dt.datetime(yrf, monf, dayf, horf, minf, secf)

    yrl=int(LastDate[0:4])
    monl=int(LastDate[4:6])
    dayl=int(LastDate[6:8])
    horl=int(LastDate[8:10])
    minl=int(LastDate[10:12])
    secl=int(LastDate[12:14])
 
    lastdate = dt.datetime(yrl, monl, dayl, horl, minl, secl)

    firstT = firstdate - timeinf.refdate
    t0 = diffdate_units(firstT, timeinf.units)

    period = lastdate - firstdate
    per = diffdate_units(period, timeinf.units)

    if freq != 'month':
        theordimt = np.int(per / np.float64(freq) + 1)
    else:
        theordimt = np.int((yrl - yrf + 1) * 12)

    theordates = np.zeros((theordimt), dtype=np.float64)
    theordates[0] = t0
    if freq != 'month':
        theordates = t0 + np.arange(1,theordimt) * np.float64(freq)
    else:
        auxmonth = values.split(':')[3]
        if auxmonth == 'midmon':
            t01 = np.array([yrf, monf, 1, 0, 0, 0])
        else:
            t01 = np.array([yrf, monf, dayf, horf, minf, secf])

        for it in range(1,theordimt):
            t02 = t01.copy()
            t02[1] = t02[1] + 1
            if t02[1] > 12:
                t02[1] = 1
                t02[0] = t02[0] + 1

            date01 = dt.datetime(t01[0], t01[1], t01[2], t01[3], t01[4], t01[5])

            if auxmonth == 'midmon':
                date02 = dt.datetime(t02[0], t02[1], t02[2], t02[3], t02[4], t02[5])
                diff012 = date02 - date01
                t01 = diffdate_units(date01 - timeinf.refdate, timeinf.units)
                theordates[it] = t01 + diffdate_units(diff012, timeinf.units) / 2.
            elif auxmonth == 'real':
                date02 = dt.datetime(t02[0], t02[1], t01[2], t01[3], t01[4], t01[5])
                diff012 = date02 - date01
                theordates[it] = theordates[it-1] + diffdate_units(diff012, timeinf.units)
            else:
                print errormsg
                print fname + ': frequency "month" required third argument "' + auxmonth + '" not ready!!!!'
                print errormsg
                quit(-1)

            t01 = t02.copy()        

    equaldates = sorted(list(set(theordates) & set(timevals)))
    missdates = sorted(list(set(theordates) - set(timevals)))

    Nequal = len(equaldates)
    Ndiff = len(missdates)

    if theordimt != timeinf.dimt:
        print warnmsg
        print fname + ': there are not the correspondant times (', theordimt ,') in file "' + filename + '" with ', timeinf.dimt,' time-steps !!!!!!!'
        print '    ' + fname + ': Number equal times-steps: ', Nequal, 'Number different time-steps: ', Ndiff

        if Nequal >= Ndiff or Nequal == 0:
            matdates = netCDFdatetime_realdatetime(timeinf.unitsval, timeinf.calendar, missdates)
            print '  file does not have dates: '
            for it in range(Ndiff):
                print matdates[it,:]
        else:
            matdates = netCDFdatetime_realdatetime(timeinf.unitsval, timeinf.calendar, equaldates)
            print '  file only has: '
            for it in range(Nequal):
                print matdates[it,:]

def writing_str_nc(varo, values, Lchar):
    """ Function to write string values in a netCDF variable as a chain of 1char values
    varo= netCDF variable object
    values = list of values to introduce
    Lchar = length of the string in the netCDF file
    """

    Nvals = len(values)
    for iv in range(Nvals):    
        stringv=values[iv]  
        charvals = np.chararray(Lchar)
        Lstr = len(stringv)
        charvals[Lstr:Lchar] = ''

        for ich in range(Lstr):
            charvals[ich] = stringv[ich:ich+1]

        varo[iv,:] = charvals

class ysubsetstats(object):
    """ Class to compute multi-year statistics of a given subset of values 
    values= values to use
    dates=dates of the values in matrix format ([year], [month], [day], [hour], [minute], [second])
    sec=section to use for the period='year', 'month', 'day', 'hour', 'minute', 'second'
    vali=initial value of the period
    valf=final value of the period
    Nq= number of quantiles (20 for 5% bins, it is going to produce 21 values)
    missval= missing value
      self.Nvalues = Number of non masked values
      self.min = minimum non masked value
      self.max = maximum non masked value
      self.mean = mean non masked value
      self.mean2 = quandratic mean non masked value
      self.stdev = standard deviation non masked value
      self.quantiles = quantiles non masked value
    """  
    def __init__(self, values, dates, sec, vali, valf, Nq, missVal):
        import numpy.ma as ma

        fname = 'ysubsetstats'
  
        if values is None:
            self.Nvalues = None
            self.min = None
            self.max = None
            self.mean = None
            self.mean2 = None
            self.stdev = None
            self.quantiles = None

        else:
            Nvalues = len(values)
            Ndates = len(dates)

            if Nvalues != Ndates:
                print errormsg
                print fname + ': number of values ', Nvalues,' does not coincide with the number of dates ',Ndates, '!!!!!!'
                print errormsg
                quit(-1)

            initialsub = np.zeros(Nvalues, dtype=bool)
            endsub = initialsub.copy()

            if missVal > 0.:
                valuesmask = ma.masked_greater_equal(values, missVal*0.9)
            else:
                valuesmask = ma.masked_less_equal(values, missVal*0.9)

            if sec == 'year':
                isec = 0
            elif sec == 'month':
                isec = 1
            elif sec == 'day':
                isec = 2
            elif sec == 'hour':
                isec = 3
            elif sec == 'minute':
                isec = 4
            elif sec == 'second':
                isec = 5
            else:
                print errormsg
                print fname + ': sction "' + sec + '" not ready!!!!'
                print errormsg
                quit(-1)
    
            if vali < valf:
                timesmaskperi = ma.masked_less(dates[:,isec], vali)
                timesmaskperf = ma.masked_greater(dates[:,isec], valf)
            else:
                timesmaskperi = ma.masked_less(dates[:,isec], valf)
                timesmaskperf = ma.masked_greater(dates[:,isec], vali)

            finalmask = valuesmask.mask+timesmaskperi.mask+timesmaskperf.mask
            finalvalues = ma.array(values, mask=finalmask)

            finalvalues2 = finalvalues*finalvalues

            self.Nvalues = finalvalues.count()
            self.min = finalvalues.min()
            self.max = finalvalues.max()
            self.mean = finalvalues.mean()
            self.mean2 = finalvalues2.mean()
            self.stdev = finalvalues.std()
            self.quantiles = mask_quantiles(finalvalues, Nq) 

def remapnn(values, filename, varn, ddegL, ddegl):
    """ Function to remap to the nearest neightbor a variable using projection from another file
    values=[newprojectionfile]:[newlonname,newlatname]:[oldlonname,oldlatname]
      [newprojectionfile]: name of the file with the new projection
      [newlonname,newlatname]: name of the longitude and the latitude variables in the new file
      [oldlonname,oldlatname]: name of the longitude and the latitude variables in the old file
    filename= netCDF file name
    varn= variable name
    ddegL= Longitude range around the old grid point to speed up the search
    ddegl= latitude range around the old grid point to speed up the search
    """ 
    fname = 'remapnn'
    ofile = 'remapnn_' + varn + '.nc'
    fillValue = 1.e20

    newprojectionfile = values.split(':')[0]
    newlonlatname = values.split(':')[1]
    oldlonlatname = values.split(':')[2]

    filexist(filename, errormsg, 'old file')
    filexist(newprojectionfile, errormsg, 'new file')

    oldlon=oldlonlatname.split(',')[0]
    oldlat=oldlonlatname.split(',')[1]

    oldncfile = NetCDFFile(filename,'r')
    varinfile(oldncfile, filename, errormsg, 'old', oldlon)
    varinfile(oldncfile, filename, errormsg, 'old', oldlat)

    newlon=newlonlatname.split(',')[0]
    newlat=newlonlatname.split(',')[1]

    newncfile = NetCDFFile(newprojectionfile,'r')
    varinfile(newncfile, newprojectionfile, errormsg, 'old', newlon)
    varinfile(newncfile, newprojectionfile, errormsg, 'old', newlat)

    oldlon2d, oldlat2d = lonlat2D(oldncfile, oldlon, oldlat)
    newlon2d, newlat2d = lonlat2D(newncfile, newlon, newlat)

    newdimx = newlon2d.shape[1]
    newdimy = newlon2d.shape[0]

    olddimx = oldlon2d.shape[1]
    olddimy = oldlon2d.shape[0]

# pairs old-->new
##
    mappairs = np.ones((newdimy, newdimx, 2), dtype=int)
    mappairs = mappairs*fillValue
    print '     ' + fname + ' searching nearest neighbor...'
    for jnew in range(newdimy):
        for inew in range(newdimx):
            if ((jnew*newdimx + inew)%100 == 0): print '      ' + fname + ' found: ', '{0:5.2f}'.format(float(jnew*newdimx + inew)*100/(newdimx*newdimy*1)), '%'

            masklonoldpt1 = oldlon2d >= newlon2d[jnew, inew] - float(ddegL)
            masklonoldpt2 = oldlon2d <= newlon2d[jnew, inew] + float(ddegL)
            masklonoldpt = masklonoldpt1 * masklonoldpt2
            masklatoldpt1 = oldlat2d >= newlat2d[jnew, inew] - float(ddegl)
            masklatoldpt2 = oldlat2d <= newlat2d[jnew, inew] + float(ddegl)
            masklatoldpt = masklatoldpt1 * masklatoldpt2

            Nmasklon = np.sum(masklonoldpt)
            Nmasklat = np.sum(masklatoldpt)

            if Nmasklon > 0 and Nmasklat > 0:
##                print newlon2d[jnew, inew], newlat2d[jnew, inew], ' ### ', np.min(oldlon2d), np.max(oldlon2d), ':', np.min(oldlat2d), np.max(oldlat2d)
##                print masklonoldpt.shape, masklatoldpt.shape, Nmasklon, Nmasklat
                maskindlon = indices_mask(masklonoldpt)
                maskindlat = indices_mask(masklatoldpt)

                mindist=1000000.
#            for jold in range(olddimy):
#                for iold in range(olddimx):
#                   dist=np.sqrt((oldlon2d[jold, iold] - newlon2d[jnew, inew])**2. + \
#                     (oldlat2d[jold, iold] - newlat2d[jnew, inew])**2.)
                for jold in range(Nmasklat):
                    for iold in range(Nmasklon):
                       if maskindlon[iold,0] == maskindlat[jold,0] and maskindlon[iold,1] == maskindlat[jold,1]:
                           dist=np.sqrt((newlon2d[jnew, inew] - oldlon2d[maskindlon[iold,0], maskindlon[iold,1]])**2. + \
                             (newlat2d[jnew, inew] - oldlat2d[maskindlat[jold,0], maskindlat[jold,1]])**2.)
##                           print jold, iold, ':' , maskindlon[iold,:], '-->', maskindlat[jold,:], '**** L:', \
##                             oldlon2d[maskindlon[iold,0], maskindlon[iold,1]], 'l', oldlat2d[maskindlat[jold,0], maskindlat[jold,1]], \
##                             'dist:',dist, 'mindist: ', mindist
                           if dist < mindist:
                               mindist = dist
##                               print jold, iold, 'dist: ', dist, 'L', maskindlon[iold,:], 'l', maskindlat[jold,:]
                               mappairs[jnew, inew, 0] = maskindlon[iold,1]
                               mappairs[jnew, inew, 1] = maskindlat[jold,0]

##                quit()

    varobj = oldncfile.variables[varn]
    varinf = variable_inf(varobj)

# Creation of the new file
##
    ncfnew = NetCDFFile(ofile,'w')
   
    for idim in range(varinf.Ndims):
        newdimv = oldncfile.dimensions[varinf.dimns[idim]]
        if dimensions[varinf.dimns[idim]] == oldlon or dimensions[varinf.dimns[idim]] == oldlat:
            if dimensions[varinf.dimns[idim]] == oldlon:
                ncfnew.createDimension(varinf.dimns[idim], newdimx)
            else:
                ncfnew.createDimension(varinf.dimns[idim], newdimy)
        else:
            if newdimv.isunlimited():
                ncfnew.createDimension(varinf.dimns[idim], None)
            else:
                ncfnew.createDimension(varinf.dimns[idim], varinf.dims[idim])

    newvar = ncfnew.createVariable(varn, varinf.dtype, varinf.dimns, fill_value=fillValue)
    for attrn in varinf.attributes:
        if not attrn == '_FillValue':
            attr = newvar.setncattr(attrn, varobj.getncattr(attrn))

    attr = newvar.setncattr('mask', 'variable remapped at nearest neighbors using ' + values.split(':')[0] + ' using ' + \
      ' as longitude and latitude valules ')

    ncfnew.sync()
    ncfnew.close()

    for varns in ncf.variables:
        if not varns == varn:
            fvaradd(filename + ':' + varns, ofile)
        elif varns == oldlon:
            fvaradd(newprojectionfile + ':' + newlon, ofile)
        elif varns == oldlat:
            fvaradd(newprojectionfile + ':' + newlat, ofile)

    chvarname(oldlon, ofile, newlon)
    chvarname(oldlat, ofile, newlat)

    ncfnew = NetCDFFile(ofile,'a')

# looping on all the variable
##
    varsecdims = []
    Nvarloop=1
    for insdim in range(varinf.Ndims - 2):
        Nvarloop = Nvarloop * varinf.dims[insdim]
        varsecdims.append(0)

    Nsecdims = len(varsecdims)

    for iloop in range(Nvarloop):
        varval = load_ncvariable_lastdims(oldncfile, varn, varsecdims, 2)
        for jnew in range(newdimy):
            for inew in range(newdimx):
                if ((jnew*newdimx + inew)%100 == 0): print '      ' + fname + ': ', '{0:5.2f}'.format(float((jnew*newdimx + inew)*100./(newdimx*newdimy))), '% done'
                newvarval[jnew, inew] = varval[mappairs[jnew, inew, 0], mappairs[jnew, inew, 1]]
                
        fill_ncvariable_lastdims(ncfnew,varn,varsecdims,2,newvarval)

        varsecdims[Nsecdims -1] = varsecdims[Nsecdims - 1] + 1
        if varsecdims[Nsecdims - 1] > varinf.dims[Nsecdims - 1] - 1:
            varsecdims[Nsecdims - 1] = 0
            for jdim in range(Nsecdims - 1):
                varsecdims[Nsecdims - 2 - jdim] = varsecdims[Nsecdims - 2 - jdim] + 1
                if varsecdims[Nsecdims - 2 - jdim] <= varinf.dims[Nsecdims - 2 - jdim] - 1:
                    break
                else:
                    varsecdims[Nsecdims - 2 - jdim] = 0

    ncfnew.sync()
    ncfnew.close()

    print 'remapnn: new file "' + ofile + '" with remapped variable written !!'

#remapnn('data/R1_CCRC_NARCliM_MOM_1950-2009_pracc.nc:lon,lat:lon,lat', 'data/AWAP_pre_1950-2009_pre_mask.nc', 'pre', 1. , 1.)
#quit()

### Options
##string_operation="operation to make: " + '\n' + " out, output values -S inidim1,[inidim2,...]:enddim1,[enddim2,...]"
"""operation to make: 
  addfattr, add a attributes to a variable from another file: addfattr -S [file]:[var] 
  addfdim, add a new dimension from another file: addfdim -S [file]:[dimension] 
  addfgattr, add global attribute from another file: addfgattr -S [file]
  addfvar, add a new variable from another file: addfvar -S [file]:[variable] 
  addgattr, add a new global attribute: addatr -S [attrname]|[attrvalue]
  addgattrk, add a new global attribute: addatr -S [attrname]|[attrvalue]|[kind(S (!, white spaces),I,R,D)]
  addref, add a new variable with dimension and attributes from an already existing 'variable ref' in the file: addref -S [variable ref]:[attr name]@[value]:[[attr2]@[value2], ...]:[value/file with values]  mname, modify name -S newname
  addvals, Function to add values to a given variable at a given dimension: addvals -S [dimension]:[position]:[Nvals]:[addval], 
    *[position]: position within the dimension at which the variable wants to be increased
       'first': first position of the dimension
       'middle': middle position of the dimension
       'last': last position of the dimension
       'num': before given value
    *[addval]: value to give to the added values
       'val': a given value
       'missing': as a missing value
  addvattr, add a new attribute to any given variable: addvattr -S [attrname]|[attrvalue]
  addvattrk, add a new attribute to any given variable: addvattrk -S [attrname]|[attrvalue]|[kind(S (!, white spaces),I,R,D)]
  flipdim, flips the value following one dimension: flipdim -S Ndim
  infgattrs, give the values of all the global attributes of the file: infgattrs
  infsinggattrs, give the value of a single global attribute of the file: infsinggattrs -S [attribute name]
  infsingvattrs, give the value of a single attribute of the variable: infsingvattrs -S [attribute name]
  inftime, give all the information of the variable time: inftime
  infvars, give the names of all the variables of the file: infvars
  infvattrs, give the values of all the attributes of a variable: infvattrs
  mdname, modify dimension name: mdname -S olddname:newdname
  means, computes the spatial mean of the variable: means
  meanseas, compute the seasonal mean of a variable: meanseas -S [timename]
  meant, computes the temporal mean of the variable: meant -S [power](power of the polynomial fit)
  mname, modify name: mname -S newname
  out, output values: out -S inidim1,[inidim2,...]:enddim1,[enddim2,...]
  rgattr, read a global attribute: rgattr -S [attrname]
  rvattr, read a variable attribute: rvattr -S [attrname]
  rmgattr, remove a global attribute: rmgattr -S [attrname]
  rmvariable, remove a variable: rmvariable
  rmvattr, remove an attribute to any given variable: rmvattr -S [attrname]
  subsetmns, retrieve a subset of values based on months: subsetmns -S [MM1]:[...:[MMn]] or [MMi]-[MMe] for a period (output as 'subsetmns.nc')
  subsetyrs, retrieve a subset of values based on years: subsetyrs -S [YYYY1]:[...:[YYYYn]] or [YYYYi]-[YYYYe] for a period (output as 'subsetyrs.nc')
  timescheck, checks time-steps of a given file: timescheck -S [FirstDate]:[LastDate][freq]:[[auxmonth]]
      [FirstDate] = first date within file (in [YYYY][MM][DD][HH][MI][SS] format)
      [LastDate] = last date within file (in [YYYY][MM][DD][HH][MI][SS] format)
      [freq] = frequency of the data within the file (in time units) or 'month' (monthly values)
      [auxmonth] = in case of 'month'
        midmon: dates are given according to the mid time value of the month
        real: dates are given increassing a month from the previous date
  timesort, sort a variable time: timesort
  timestepchg, changes a time-step value from a file, using a given time-step from another one: timestepchg -S [origtime_step]:[newfile]:[newtime_step]
  valmod, modifiy values of variable -S [modification]:
     sumc,[constant]: add [constant] to variables values
     subc,[constant]: substract [constant] to variables values
     mulc,[constant]: multipy by [constant] to variables values
     divc,[constant]: divide by [constant] to variables values
     lowthres,[threshold],[newvalue]: modify all values below [threshold] to [newvalue]
     upthres,[threshold],[newvalue]: modify all values above [threshold] to [newvalue]
  vartimeshidft, temporaly shift a number of time-steps a given variable inside a netCDF file: vartimeshift -S [nsteps]:[[FillValue]]
  varmask, mask a variable using a mask: varmask -S [maskfilename]:[maskvar]:[dimsmask]:[timename]. It is assumed that mask[...,dimM,dimJ,dimK] 
    and var[...,dimM,dimJ,dimK] share the last dimensions

'addfattr': fattradd(opts.varname, opts.values, opts.ncfile)
'addfdim': fdimadd(opts.values, opts.ncfile)
'addfgattr': fgaddattr(opts.values, opts.ncfile)
'addfvar': fvaradd(opts.values, opts.ncfile)
'addgattr': gaddattr(opts.values, opts.ncfile)
'addgattrk': gaddattrk(opts.values, opts.ncfile)
'addref': varaddref(opts.values, opts.ncfile, opts.varname)
'addvattr': varaddattr(opts.values, opts.ncfile, opts.varname)
'addvattrk': varaddattrk(opts.values, opts.ncfile, opts.varname)
'dimflip': flipdim(opts.values,opts.ncfile,opts.varname)
'infgattrs': igattr(opts.ncfile)
'infsinggattrs': isgattrs(opts.values, opts.ncfile)
'infsingvattrs': isvattrs(opts.values, opts.ncfile, opts.varname)
'inftime': timeinf(opts.ncfile, opts.varname)
'infvars': ivars(opts.ncfile)
'infvattrs': ivattr(opts.ncfile, opts.varname)
'mdname': chdimname(opts.values, opts.ncfile, opts.varname)
'means': spacemean(opts.ncfile, opts.varname)
'meanseas': spacemean(opst.values, opts.ncfile, opts.varname)
'meant': timemean(opts.values, opts.ncfile, opts.varname)
'mname': chvarname(opts.values, opts.ncfile, opts.varname)
'out': varout(opts.values, opts.ncfile, opts.varname)
'rgattr': grattr(opts.values, opts.ncfile)
'rvattr': vrattr(opts.values, opts.ncfile, opts.varname)
'rmgattr': grmattr(opts.values, opts.ncfile)
'rmvariable': varrm(opts.ncfile, opts.varname)
'rmvattr': varrmattr(opts.values, opts.ncfile, opts.varname)
'subsetmns': submns(opts.values, opts.ncfile, opts.varname)
'subsetyrs': subyrs(opts.values, opts.ncfile, opts.varname)
'timescheck': check_times_file(opts.values, opts.ncfile, opts.varname)
'timesort': sorttimesmat(opts.ncfile, opts.varname)
'timestepchg': chgtimestep(opts.values, opts.ncfile, opts.varname)
'valsadd': addvals(opts.values, opts.ncfile, opts.varname)
'valmod': valmod(opts.values, opts.ncfile, opts.varname)
'varmask': maskvar(opts.values, opts.ncfile, opts.varname)
'vartimeshift': timeshiftvar(opts.values, opts.ncfile, opts.varname)
"""
