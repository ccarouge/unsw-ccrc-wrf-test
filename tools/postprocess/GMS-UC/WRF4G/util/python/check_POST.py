#!/usr/bin/python
# e.g. # python /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/WRF4G/util/python/check_POST.py -f /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/NARCliM11 -w /home/lluis/UNSW-CCRC-WRF/tools/python -p 33 33 -v 1 -q /home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/requested_variables.inf.ccrc.NARCliM11.1yearPeriod.cordex_1var -r 19970201000000 -o 'reqfile'

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *
from pylab import *
import matplotlib as plt
import subprocess as sub
import datetime as dt
import os


####### Classes
# plotValXY: class with all the plotting attributes
# plotValStatXY: class with all the plotting attributes of a statistics variable
# statsVal: Statistics class providing min, max, mean, mean2, median, standard deviation, non None values and percentiles of a list of values
# ncFileStats: Statistics class for a netCDF file providing: format, name of the dimensions, number of dimensions, name of the variables, 
#    number of variables, name of attributes and number of attributes, time difference between time-steps, first time-step

####### Functions
# get_xy_timesec: Function to get necessary information to plot a variable in a given point over specific period of time in hours
# get_2D_timesec: Function to get necessary information to plot a variable for the whole domain over specific period of time in hours
# SecondsBetween: Gives the number of seconds between two dates [date1], [date2]  ([YYYY][MM][DD][HH][MI][SS], format)
# IsinPeriod: Gives a true/false if a given [valdate] format is within a period [iniper], [endper] ([YYYY][MM][DD][HH][MI][SS], format)
# frequence_postprocess: Provides the yearly time-steps according to the postprocessed frequency and the given year
# rgb_val: Function to provide the value of a color between two colors rgbi = (red_i, green_i, blue_i), rgbe = (red_e, green_e, blue_e), for 
#    the value 'ival' from 'Nval' values

class plotValXY(object):
  """class with all the plotting attributes"""
  def __init__(self, vals):
    self.timename = vals
  def __len__(self):
    return 0
  def timeVarVal(self, vals):
    self.timeVarVal = vals
  def varValPT(self, vals):
    self.varValPT = vals
  def varUnits(self, vals):
    self.varUnits = vals
  def timeUnits(self, vals):
    self.timeUnits = vals

class plotValStatXY(object):
  """class with all the plotting attributes of a statistics variable"""
  def __init__(self, vals):
    self.timename = vals
  def __len__(self):
    return 0
  def timeVarVal(self, vals):
    self.timeVarVal = vals
  def varValPT(self, vals):
    self.varValPT = vals
  def varBnds(self, vals):
    self.varBnds = vals 
  def varBndsValt(self, vals):
    self.varBndsValt = vals
  def varBndsValT(self, vals):
    self.varBndsValT = vals

def get_xy_timesec(varname, timename, filename, pt, inits, endts):
# Function to get necessary information to plot a variable in a given point for a given period in hours
  sub='get_xy_timesec'
  errormsg='  ERROR -- error -- ' + sub + ' -- ERROR -- error' 
  if not os.path.isfile(filename):
    print errormsg
    print '    File ' + filename + ' does not exist !!'
    print errormsg
    quit()  
 
#  print filename

  ncf = NetCDFFile(filename,'r')

  if not ncf.variables.has_key(timename):
    print errormsg
    print '    File does not have time !!!!'
    print errormsg
    quit()

  timeVar = ncf.variables[timename]
  valPlot = plotValXY(timename)

  if not timename == 'Times':
    valPlot.timeUnits=getattr(timeVar, 'units')
    timevals = timeVar.getValue()

# Defining time interval
#
  init = -1
  endt = -1
  for it in range(len(timevals)):
    tval = int(timevals[it])
    if tval >= inits and tval <= endts:
      if init == -1:
        if tval == inits:
          init = it
        else:
          init = it
          if init < 0: 
            init = 0
      endt = it + 1

  okvalues = False
  if init == -1 and endt == -1:
    print errormsg
    print '    File without values in range ', inits, ' - ', endts,' !!'
    valPlot.timeVarVal = None
#    print errormsg 
  elif not init == -1 and not endt == -1 and not endt == 1:
    valPlot.timeVarVal=timevals[init:endt]
    okvalues = True
  elif init == 0 and endt == 1:
    valPlot.timeVarVal=timevals[init:endt]
    okvalues = True

  if not timename == 'Times':
    valPlot.timeUnits=getattr(timeVar, 'units')
  else:
    valPlot.timeUnits=''
 
  if not ncf.variables.has_key(varname):
    print errormsg
    print '    File does not have variable ' + varname + ' !!!!'
    print errormsg
    quit()  

  var = ncf.variables[varname]
  varVal = var.getValue()
  valPlot.varUnits = getattr(var, 'units')
  dimt = varVal.__len__() 

  if okvalues:
    if varVal.ndim == 4:
      valPlot.varValPt=varVal[init:endt,0,pt[0],pt[1]]
    elif varVal.ndim == 3:
      valPlot.varValPt=varVal[init:endt,pt[0],pt[1]]
#  elif varVal.ndim == 2:
#    valPlot.varValPt=varVal[0:dimt,pt[0]]
#  elif varVal.ndim == 1:
#    valPlot.varValPt=varVal[0:dimt]
    else:
      print errormsg
      print '     Variable ' + varname + ' has ', varVal.ndim, ' dimensions !!'
      print errormsg
      quit()
  else:
    valPlot.varValPt = None

  ncf.close
  return valPlot

def get_2D_timesec(varname, timename, filename, inits, endts):
# Function to get necessary information to plot a variable for the whole domain over specific period of time in hours
  sub='get_2D_timesec'
  errormsg='  ERROR -- error -- ' + sub + ' -- ERROR -- error' 
  if not os.path.isfile(filename):
    print errormsg
    print '    File ' + filename + ' does not exist !!'
    print errormsg
    quit()  

  ncf = NetCDFFile(filename,'r')

  if not ncf.variables.has_key(timename):
    print errormsg
    print '    File does not have time !!!!'
    print errormsg
    quit()

  timeVar = ncf.variables[timename]
  valPlot = plotValXY(timename)
  timevals = timeVar.getValue()

  if not timename == 'Times':
    valPlot.timeUnits=getattr(timeVar, 'units')
  else:
    valPlot.timeUnits=''

# Defining time interval
#
  init = -1
  endt = -1
  for it in range(len(timevals)):
    tval = int(timevals[it])
    if tval >= inits and tval <= endts:
      if init == -1:
        if tval == inits:
          init = it
        else:
          init = it - 1
          if init < 0: 
            init = 0
      endt = it

  okvalues = False
  if init == -1 and endt == -1:
    print errormsg
    print '    File without values in range ', inits, ' - ', endts,' !!'
    valPlot.timeVarVal = None
#    print errormsg 
  elif not init == -1 and not endt == -1 and not endt == 1:
    okvalues = True
    valPlot.timeVarVal=timevals[init:endt]

  if not ncf.variables.has_key(varname):
    print errormsg
    print '    File does not have variable ' + varname + ' !!!!'
    print errormsg
    quit()  

  var = ncf.variables[varname]
  varVal = var.getValue()
  valPlot.varUnits = getattr(var, 'units')
  dimt = varVal.__len__() 

  if okvalues:
    if varVal.ndim == 4:
      valPlot.varValPt=varVal[init:endt,0,:,:]
    elif varVal.ndim == 3:
      valPlot.varValPt=varVal[init:endt,:,:]
#  elif varVal.ndim == 2:
#    valPlot.varValPt=varVal[0:dimt,pt[0]]
#  elif varVal.ndim == 1:
#    valPlot.varValPt=varVal[0:dimt]
    else:
      print errormsg
      print '    Variable ' + varname + ' has ', varVal.ndim, ' dimensions !!'
      print errormsg
      quit()
  else:
    valPlot.varValPt = None

  ncf.close
  return valPlot

def SecondsBetween(date1, date2):
  """ Gives the number of seconds between two dates [date1], [date2]  ([YYYY][MM][DD][HH][MI][SS], format)
  >>> print SecondsBetweeen('19760101000000', '19761231235959')
  31622399
  """
  import datetime as dt
  import numpy as np

  dt1=dt.datetime(int(date1[0:4]), int(date1[4:6]), int(date1[6:8]), int(date1[8:10]), int(date1[10:12]), int(date1[12:14]))
  dt2=dt.datetime(int(date2[0:4]), int(date2[4:6]), int(date2[6:8]), int(date2[8:10]), int(date2[10:12]), int(date2[12:14]))

  difft = dt2-dt1

  return difft.days*24*3600 + difft.seconds


def IsinPeriod(valdate, iniper, endper):
  """ Gives a true/false if a given [valdate] format is within a period [iniper], [endper] ([YYYY][MM][DD][HH][MI][SS], format)
  >>> print IsinPeriod('19760217083000', '19760101000000', '19761231235959')
  """
  import datetime as dt
  import numpy as np

  vdate=dt.datetime(int(valdate[0:4]), int(valdate[4:6]), int(valdate[6:8]), int(valdate[8:10]), int(valdate[10:12]), int(valdate[12:14]))
  idate=dt.datetime(int(iniper[0:4]), int(iniper[4:6]), int(iniper[6:8]), int(iniper[8:10]), int(iniper[10:12]), int(iniper[12:14]))
  edate=dt.datetime(int(endper[0:4]), int(endper[4:6]), int(endper[6:8]), int(endper[8:10]), int(endper[10:12]), int(endper[12:14]))

  if vdate >= idate and vdate <= edate:
    return True
  else:
    return False

"""
File	quantile.py
Desc	computes sample quantiles
Author  Ernesto P. Adorio, PhD.
		UPDEPP (U.P. at Clarkfield)
Version 0.0.1 August 7. 2009
http://adorio-research.org/wordpress/?p=125
"""
from math import modf, floor
def quantile(x, q,  qtype = 7, issorted = False):
	"""
	Args:
	   x - input data
	   q - quantile
	   qtype - algorithm
	   issorted- True if x already sorted.
	Compute quantiles from input array x given q.For median,
	specify q=0.5.
	References:
	   http://reference.wolfram.com/mathematica/ref/Quantile.html
	   http://wiki.r-project.org/rwiki/doku.php?id=rdoc:stats:quantile
	Author:
	Ernesto P.Adorio Ph.D.
	UP Extension Program in Pampanga, Clark Field.
	"""
	if not issorted:
		y = sorted(x)
	else:
		y = x
	if not (1 <= qtype <= 9):
	   return None  # error!
	# Parameters for the Hyndman and Fan algorithm
	abcd = [(0,   0, 1, 0), # inverse empirical distrib.function., R type 1
			(0.5, 0, 1, 0), # similar to type 1, averaged, R type 2
			(0.5, 0, 0, 0), # nearest order statistic,(SAS) R type 3
			(0,   0, 0, 1), # California linear interpolation, R type 4
			(0.5, 0, 0, 1), # hydrologists method, R type 5
			(0,   1, 0, 1), # mean-based estimate(Weibull method), (SPSS,Minitab), type 6
			(1,  -1, 0, 1), # mode-based method,(S, S-Plus), R type 7
			(1.0/3, 1.0/3, 0, 1), # median-unbiased ,  R type 8
			(3/8.0, 0.25, 0, 1)   # normal-unbiased, R type 9.
		   ]
	a, b, c, d = abcd[qtype-1]
	n = len(x)
	g, j = modf( a + (n+b) * q -1)
	if j < 0:
		return y[0]
	elif j >= n:
		return y[n-1]   # oct. 8, 2010 y[n]???!! uncaught  off by 1 error!!!
	j = int(floor(j))
	if g ==  0:
	   return y[j]
	else:
	   return y[j] + (y[j+1]- y[j])* (c + d * g)

class statsVal(object):
  """Statistics class providing min, max, mean mean2, median, standard deviation, non None values and percentiles of a list of values"""

  def __init__(self, values):
    import math

    if values is None:
      self.Nv = None
      self.minv = None
      self.maxv = None
      self.meanv = None
      self.mean2v = None
      self.stdv = None
      self.Nokvalues = None
      self.quantilesv = None
      self.minstepv = None
      self.maxstepv = None
      self.absmaxstepv = None
      self.meanstepv = None
      self.meannomaxstepv = None
      self.mean2stepv = None
      self.mean2nomaxstepv = None
      self.stdstepv = None
      self.stdnomaxstepv = None
    elif len(values) == 1:
      self.Nv = 1
      self.minv = values
      self.maxv = values
      self.meanv = values
      self.mean2v = values*values
      self.stdv = 0.
      self.Nokvalues = 1
      self.medianv = values
      self.quantilesv = None
      self.minstepv = 0.
      self.maxstepv = 0.
      self.absmaxstepv = 0.
      self.meanstepv = 0.
      self.meannomaxstepv = 0.
      self.mean2stepv = 0.
      self.mean2nomaxstepv = 0.
      self.stdstepv = 0.
      self.stdnomaxstepv = 0.
    else:
      self.Nv=len(values)
      self.minv=10000000000.
      self.maxv=-self.minv
      self.meanv=0.
      self.mean2v=0.
      self.stdv=0.
      self.minstepv = self.minv
      self.maxstepv = -self.minv
      self.absmaxstepv = 0.
      self.meanstepv = 0.
      self.meannomaxstepv = 0.
      self.mean2stepv = 0.
      self.mean2nomaxstepv = 0.
      self.stdstepv = 0.
      self.stdnomaxstepv = 0.

      sortedvalues = sorted(values)

      self.Nokvalues = 0
      for inum in range(self.Nv):
        if not values[inum] == None:
          self.Nokvalues = self.Nokvalues + 1
          if values[inum] < self.minv:
            self.minv = values[inum]
          if values[inum] > self.maxv:
            self.maxv = values[inum]

          self.meanv = self.meanv+values[inum]
          self.mean2v = self.mean2v+values[inum]*values[inum]

      self.meanv = self.meanv/float(self.Nv)
      self.mean2v = self.mean2v/float(self.Nv)
      self.stdv = math.sqrt(self.mean2v-self.meanv*self.meanv)
      self.medianv = quantile(sortedvalues, 0.5, 7, issorted=True)
      self.quantilesv = []
      for iq in range(20): 
#        self.quantilesv.append(sortedvalues[int((self.Nv-1)*iq/20)])
        self.quantilesv.append(quantile(sortedvalues, float(iq/20.), 7, issorted=True))

      self.quantilesv.append(sortedvalues[self.Nv-1])
      valsac = values[0:self.Nv-1]
      valsad = values[1:self.Nv]
      stepvals = valsad - valsac

      for inum in range(self.Nv - 1):
        if not values[inum] == None:
          if stepvals[inum] < self.minstepv:
            self.minstepv = stepvals[inum]
          if stepvals[inum] > self.maxstepv:
            self.maxstepv = stepvals[inum]
          if abs(stepvals[inum]) > self.absmaxstepv:
            self.absmaxstepv = abs(stepvals[inum])

          self.meanstepv = self.meanstepv+stepvals[inum]
          self.mean2stepv = self.mean2stepv+stepvals[inum]*stepvals[inum]

      posabsmax = argmax(abs(stepvals))

      if posabsmax <= self.Nv - 3:
        self.meannomaxstepv = self.meanstepv - stepvals[posabsmax] - stepvals[posabsmax+1]
        self.mean2nomaxstepv = self.mean2stepv - stepvals[posabsmax]*stepvals[posabsmax] - \
          stepvals[posabsmax+1]*stepvals[posabsmax+1]
        Nnomax = self.Nv - 3
      else:
        self.meannomaxstepv = self.meanstepv - stepvals[posabsmax]
        self.mean2nomaxstepv = self.mean2stepv - stepvals[posabsmax]*stepvals[posabsmax]
        Nnomax = self.Nv - 2

      self.meanstepv = self.meanstepv/float(self.Nv - 1.)
      self.mean2stepv = self.mean2stepv/float(self.Nv - 1.)
      self.stdstepv = sqrt(self.mean2stepv - self.meanstepv*self.meanstepv)
      self.meannomaxstepv = self.meannomaxstepv/float(Nnomax)
      self.mean2nomaxstepv = self.mean2nomaxstepv/float(Nnomax)
      self.stdnomaxstepv = sqrt(self.mean2nomaxstepv - self.meannomaxstepv*self.meannomaxstepv)

#      print 'meanstepv: ', self.meanstepv, 'absmaxstepv: ', self.absmaxstepv, 'stdstepv: ',      \
#        self.stdstepv, 'meannomaxstep: ', self.meannomaxstepv, 'stdnomaxstep: ',self.stdnomaxstepv


def listdate_2_date(listdate):
  """ Conversion of a list date [Y] [Y] [Y] [Y] '-' [M] [M] '-' [D] [D] '_' [H] [H] ':' [M] [M] ':' [S] [S]

  >>> print timelist
  ['2' '0' '0' '2' '-' '0' '2' '-' '1' '7' '_' '0' '0' ':' '0' '0' ':' '0' '0']
  >>> listdate_2_date(timelist).strftime("%Y/%m/%d %H:%M:%S")
  2002/02/17 00:00:00
  """
  import datetime as dt  
  
  year=int(listdate[0])*1000 + int(listdate[1])*100 + int(listdate[2])*10 + int(listdate[3])
  month=int(listdate[5])*10 + int(listdate[6])
  day=int(listdate[8])*10 + int(listdate[9])
  hour=int(listdate[11])*10 + int(listdate[12])
  minute=int(listdate[14])*10 + int(listdate[15])
  second=int(listdate[17])*10 + int(listdate[18])

  date = dt.datetime(year, month, day, hour, minute, second)
  return date

class ncFileStats(object):
  """Statistics class for a netCDF file providing: format, name of the dimensions, number of dimensions, name of the variables, 
     number of variables, name of attributes and number of attributes, number of times, time difference between time-steps, first time-step 
     and last time-step
        self.dims: dictionary with the name of the dimension and its size
        self.ndims: number of dimensions
        self.vars: dictionary with the name of the variable
        self.ndims: number of variables
        self.ntimes: number of times
        self.dT: difference in seconds between time-steps
        self.firstT: first date in seconds since refdatestr
        self.firstTS: first date in [YYYY][MM][DD][HH][MI][HH][SS]
        self.lastT: last date in seconds since refdatestr
        self.lastTS: last date in [YYYY][MM][DD][HH][MI][HH][SS]
  """
  import numpy as np
  from netCDF4 import Dataset as NetCDFFile
  import datetime as dt
  import time

  def __init__(self, ncfile, timename, refdatestr):
    sub = 'ncFileStats'
    errmsg = '  ERROR -- error -- ' + sub + ' -- ERROR -- error'
    print ncfile
    ncf = NetCDFFile(ncfile,'r')
#    self.format = ncf.file_format
    self.dims = ncf.dimensions
    self.ndims = len(self.dims)
    self.vars = ncf.variables
    self.nvars = len(self.vars)
#    self.attrs = ncf.ncattrs()
#    self.nattrs = len(self.attrs)

    if not ncf.variables.has_key(timename):
      print errmsg
      print '    File ' + ncfile + ' does not have the variable time as ' + timename
      print errmsg
      quit()

    vartime = ncf.variables[timename]
    timeval = vartime.getValue()
    self.ntimes = len(timeval)

    refdate = dt.datetime(int(refdatestr[0:4]), int(refdatestr[4:6]), int(refdatestr[6:8]), 
      int(refdatestr[8:10]), int(refdatestr[10:12]), int(refdatestr[12:14]) )
    if len(timeval.shape) == 1:
      if self.ntimes > 1:
        dT = timeval[1]-timeval[0]
      else:
        dT = 0
      firstT = timeval[0]
      time0 = refdate + dt.timedelta(seconds=firstT*3600)
      firstTS = time0.strftime("%Y%m%d%H%M%S")
      lastT = timeval[self.ntimes-1]
      time0 = refdate + dt.timedelta(seconds=lastT*3600)
      lastTS = time0.strftime("%Y%m%d%H%M%S")
    else:
      if self.ntimes > 1:
        time0 = listdate_2_date(timeval[0])
        time1 = listdate_2_date(timeval[1])
      else:
        time0 = listdate_2_date(timeval[0])
        time1 = listdate_2_date(timeval[0])

      dtime = time1 - time0
      dT = dtime.days*24 + dtime.seconds/3600
      DfirsT = time0 - refdate
      firstT = DfirsT.days*24 + DfirsT.seconds/3600
      firstTS = time0.strftime("%Y%m%d%H%M%S")
      time0 = listdate_2_date(timeval[self.ntimes]-1)
      DlastT = time0 - refdate
      lastT = DlastT.days*24 + DlastT.seconds/3600
      lastTS = time0.strftime("%Y%m%d%H%M%S")

    self.dT = dT*3600
    self.firstTS = firstTS
    self.firstT = firstT*3600
    self.lastTS = lastTS
    self.lastT = lastT*3600

    ncf.close()

class frequence_postprocess(object):
  """ Class that provides the yearly time-steps according to the postprocessed frequency and the given year
    self.dift : diference between time-steps (on 'MO' frequencies will be a list of values)
    self.Ntimesteps: number of time steps
    self.first: seconds from the reference date for the first date
    self.timebounds: values (in seconds since 'refdate') of the time boundaries 
    self.timesteps: values (in seconds since 'refdate') of the time-step
  >>> tvals = frequence_postprocess('01H', 1977, '19491201000000', 'tas')
  >>> print tvals.dift, tvals.Ntimesteps, tvals.first 
  3600 8760 854755200
  >>> print tvals.timebounds
  [[0 0]
   [0 0]
   [0 0]
   ..., 
   [0 0]
   [0 0]
   [0 0]]
  >>> print tvals.timesteps
  [854755200 854758800 854762400 ..., 886280400 886284000 886287600]
  >>> tvals = frequence_postprocess('MOM', 1976, '19491201000000', 'tasmax')
  >>> print tvals.dift
  [ 2548800.  2548800.  2635200.  2635200.  2635200.  2635200.  2678400.
    2635200.  2635200.  2635200.  2592000.        0.]
  >>> print tvals.Ntimesteps, tvals.first 
  12 854755200
  >>> print tvals.timebounds
  [[854755200 857433600]
   [857433600 859852800]
   [859852800 862531200]
   [862531200 865123200]
   [865123200 867801600]
   [867801600 870393600]
   [870393600 873072000]
   [873072000 875750400]
   [875750400 878342400]
   [878342400 881020800]
   [881020800 883612800]
   [883612800 886291200]]
  >>> print tvals.timesteps
  [856094400 858643200 861192000 863827200 866462400 869097600 871732800
   874411200 877046400 879681600 882316800 884952000]
  >>> tvals = frequence_postprocess('DAM', 1976, '19491201000000', 'tasmax')
  >>> print tvals.dift, tvals.Ntimesteps, tvals.first 
  86400 365 854755200
  >>> print tvals.timebounds
  [[854755200 854841600]
   [854841600 854928000]
   [854928000 855014400]
   ...,
   [886032000 886118400]
   [886118400 886204800]
   [886204800 886291200]]
  >>> print tvals.timesteps
  [854798400 854884800 854971200,...,886075200 886161600 886248000]
  """
  import datetime as dt
  import numpy as np
  
  def __init__(self, freqpost, year, refdate, varN):
    char2 = freqpost[0:2]
    charL = freqpost[2:3]

    stats=['min', 'max', 'mean', 'ac', 'mintstep', 'maxtstep', 'meantstep', 'actstep']
    Lvarn = len(varN)
    lstats = len(stats)    

    statv = False
    for ist in range(lstats):
      lstatn = len(stats[ist])
      if lstatn < Lvarn:
        if varN[Lvarn-lstatn:Lvarn] == stats[ist]:
          statv = True
          break

    init = dt.datetime( year, 1, 1, 0, 0, 0)
    endt = dt.datetime( year + 1, 1, 1, 0, 0, 0)
    reft = dt.datetime(int(refdate[0:4]), int(refdate[4:6]), int(refdate[6:8]), int(refdate[8:10]), int(refdate[10:12]), int(refdate[12:14]))
    difft = endt - init

    if char2 == 'DA':
      self.Ntimesteps = difft.days
      self.dift = 24*3600
    elif char2 == 'MO':
      self.Ntimesteps = 12
      diftvals =  np.arange(self.Ntimesteps)*1.
    elif charL == 'H':
      hours = difft.days*24 + difft.seconds/3600
      self.Ntimesteps = hours / int(char2)
      self.dift = int(char2) * 3600

#
# First and last time since reference date (in seconds)
    firsdT = init - reft
    self.first = firsdT.days*24*3600 + firsdT.seconds
    lasdT = endt - reft
    self.last = lasdT.days*24*3600 + lasdT.seconds

    timestepsval = np.arange(self.Ntimesteps)
    timebounds = np.reshape(np.arange(2*self.Ntimesteps), (self.Ntimesteps, 2))
    newt = init
    newtsec = self.first
    for it in range(self.Ntimesteps):
      if not char2 == 'MO':
        if charL == 'H' and not statv:
          timestepsval[it] = self.first + self.dift*(it)
          timebounds[it,0] = 0.
          timebounds[it,1] = 0.
        else:
          timestepsval[it] = self.first + self.dift/2 + self.dift*(it)
          timebounds[it,0] = self.first + self.dift*(it)
          timebounds[it,1] = self.first + self.dift*(it+1)
      else:
        newdate = np.array([int(newt.strftime("%Y")), int(newt.strftime("%m")), int(newt.strftime("%d")), int(newt.strftime("%H")), 
          int(newt.strftime("%M")), int(newt.strftime("%S"))])
        newdate1mon = newdate.copy()
        newdate1mon[1] = newdate1mon[1] + 1
        if newdate1mon[1] > 12:
          newdate1mon[0] = newdate1mon[0] + 1
          newdate1mon[1] = 1
        newt1mon = dt.datetime(newdate1mon[0], newdate1mon[1], newdate1mon[2], newdate1mon[3], newdate1mon[4], newdate1mon[5])
 
        dift = newt1mon - newt 
        diftvals[it] = dift.days*24*3600 + dift.seconds
        timestepsval[it] = newtsec + (diftvals[it])/2.
        timebounds[it,0] = newtsec
        timebounds[it,1] = newtsec + diftvals[it]
        newt = newt1mon
        newtsec = newtsec + diftvals[it]

    self.timesteps = timestepsval
    self.timebounds = timebounds
#
# Computing seconds between monthly values
    if char2 == 'MO':
      diftvalsadv = diftvals.copy()
      diftvals[0:self.Ntimesteps-2] = diftvalsadv[0:self.Ntimesteps-2]/2 + diftvals[1:self.Ntimesteps-1]/2
      diftvals[self.Ntimesteps-1] = 0.
      self.dift = diftvals

def rgb_val(rgbi, rgbe, ival, Nval):
  """ Function to provide the value of a color between two colors rgbi = (red_i, green_i, blue_i), rgbe = (red_e, green_e, blue_e), for the value
      'ival' from 'Nval' values
  >>> print rgb_val(np.asarray((1., 0., 0.), float), np.asarray((0., 0., 1.), float), 5, 10)
  [0.5 0 0.5]
  """
  rgbdist = rgbe - rgbi
  rgbnew = rgbi + rgbdist*float(ival*1./Nval*1.)
  return rgbnew

def rgb_3val(rgbi, rgbm, rgbe, ival, Nval):
  """ Function to provide the value of a color between two colors rgbi = (red_i, green_i, blue_i), rgbm = (red_m, green_m, blue_m), 
      rgbe = (red_e, green_e, blue_e),for the value 'ival' from 'Nval' values
  >>> print rgb_3val(np.asarray((1., 0., 0.), float), np.asarray((0., 1., 0.), float), np.asarray((0., 0., 1.), float), 4, 10)
  [0.2 0.8 0.]
  """
  rgbdist1 = rgbm - rgbi
  rgbdist2 = rgbe - rgbm
  if ival < int(Nval/2):
    rgbnew = rgbi + rgbdist1*float(ival*2./Nval*1.)
  elif ival == int(Nval/2):
    rgbnew = rgbm
  elif ival > int(Nval/2):
    rgbnew = rgbm + rgbdist2*float((ival-Nval/2)*2./Nval*1.)

  for icol in range(3):
    if rgbnew[icol] < 0.: rgbnew[icol] = 0.
    if rgbnew[icol] > 1.: rgbnew[icol] = 1.

  return rgbnew

def mondate(quantity, refdate, unit):
  """ Function to provide the month of a quantity according to its reference date
  >>> print mondate(526332, '19491201000000', 'h')
  12
  """
  import datetime as dt

  reft = dt.datetime(int(refdate[0:4]), int(refdate[4:6]), int(refdate[6:8]), int(refdate[8:10]), int(refdate[10:12]), int(refdate[12:14]))
  if unit == 'd':
    date = reft + dt.timedelta(seconds=quantity*3600.*24.)
  elif unit == 'h':
    date = reft + dt.timedelta(seconds=quantity*3660.)
  elif unit == 'm':
    date = reft + dt.timedelta(seconds=quantity*60.)
  elif unit == 's':
    date = reft + dt.timedelta(seconds=quantity)

  mon=int(date.strftime("%m"))

  return mon

def set_newline_val(linev):
    """ function to provide the values of a line readed from a file without
        spaces and end-line character
    >>> print set_newline_val('12312 abc asdad       123121     131 1 11 1    1 1    \n')
    ['12312', 'abc', 'asdad', '123121', '131', '1', '11', '1', '1', '1']
    """
    linevals = filter(None, linev.split(' '))
    newlineval = []
    for val in linevals:
        newlineval.append(val.replace('\n',''))

    newlineval = filter(None, newlineval)

    return newlineval

def set_newline_val_quota(linev):
    """ function to provide the values of a line readed from a file without
        spaces and end-line character taking words between quotas as a single word
    >>> print set_newline_val_quota('12312 abc asdad       123121     "131 1 11 1"    "A" 1    \n')
    ['12312', 'abc', 'asdad', '123121', '131 1 11 1', 'A', '1']
    """

    linevals = filter(None, linev.split(' '))
    newlineval = []
    begin = False
    finishedword = False
    word = ''
    for val in linevals:
        Nval = len(val)

        if begin == True and not val[Nval-1:Nval] == '"':
            word = word + ' ' + val.replace('"', '')
        elif val[0:1] + val[Nval-1:Nval] == '""' or val[0:1] + val[Nval-2:Nval] == '""\n' :
            newval = val.replace('"', '')
            word = newval.replace('\n', '')
            finishedword = True
        elif val[0:1] == '"' and begin == False: 
            begin = True
            word = val.replace('"', '')
        elif val[Nval-1:Nval] == '"' or val[Nval-2:Nval] == '"\n':
            begin = False
            newval = val.replace('"', '')
            word = word + ' ' + newval.replace('\n', '')
            finishedword = True
        else:
            word = val.replace('\n', '')
            finishedword = True 

        if finishedword == True:
            newlineval.append(word)
            word=''
            finishedword = False

    newlineval = filter(None, newlineval)

    return newlineval

def statistics_name(varn, varstdn, stat):
    """Function to provide de name of the variable according to the statistics of the 
       period
    >>>print statistics_name('prac', 'precipitation_amount', 'MOM')
    pracmean
    """
    statnames = {}
    statnames['N']='min'
    statnames['X']='max'
    statnames['M']='mean'
    statnames['S']='ac'

    noaccum = {}
    noaccum['prac'] = ['precipitation_flux', 'pr', '']
    noaccum['praccac'] = ['precipitation_amount', 'pracc', '']

    statn = statnames[stat[2:3]]

    for name in noaccum:
      if statn == name and varstdn == name[0]:
        statn = name[2]

    return statn

def check_postfiles(reqfile, refdatefile):
    """ Function to determine if the given postPROJ folder has all the required files
    >>> checkpost = check_postfiles('/home/lluis/UNSW-CCRC-WRF/tools/postprocess/GMS-UC/ requested_variables.inf.ccrc.NARCliM11.1yearPeriod.cordex_1997', '19970201000000')
    print checkpost
    ['/home/lluis/studies/NARCliMGreatSydney2km/data/d01/postPROJ/1990-1999/CCRC_NARCliM_Sydney_03H_1990-1994_psl.nc']
    """
    import os
    import subprocess as sub
    import numpy as np

    errmsg = 'ERROR -- error -- ERROR -- error'


# Charging requested file
##
    reqinf = requested(reqfile)

# Charging wrfncxnj.table 
##
    wrfncxnj = wrfncxnjTable(reqinf.mainframe['WRF4Ghome'] + '/util/postprocess/wrfncxnj/wrfncxnj.table')

# WRFraw file
##
    outfiles = []
    rfdatef = refdatefile[0:4] + '-' + refdatefile[4:6] + '-' + refdatefile[6:8] + '_' +         \
      refdatefile[8:10] + ':' + refdatefile[10:12] + ':' + refdatefile[12:14]
    for varname in reqinf.varvals:
        varnvals = reqinf.varvals[varname]
        wrfki = varnvals[1]
        if wrfki[0:2] == 'p@':
            Nwrfki = len(wrfki)
            Namp = len(wrfki.split('&'))
            if Namp > 1:
                wrfk = wrfki[2:Nwrfki].split('&')[0]
            else:
                wrfk = wrfki[2:Nwrfki]
        else:
            wrfk = wrfki

        WRFfile = reqinf.mainframe['EXPDIR'] + '/wrf' + wrfk + '_d01_' + rfdatef
        if not os.path.isfile(WRFfile):
            print errmsg
            print '  File ' + WRFfile + ' does not exist !!'
            print errmsg
            quit()

# Level
##
        lev = varnvals[2]
        levS = ''
        if not lev == 'SFC':
            levS = '{0:04d}'.format(int(lev))

# Frequency
##
        freqvar = varnvals[3]
        Nfreqvar = len(freqvar)
        statname = ''
        if freqvar == 'All':
            ins = 'python ' + reqinf.mainframe['WRF4Ghome'] + '/util/python/nc_time_tools.py'    \
              ' -f ' + WRFfile + ' -o timeINF -r 19491201000000 -t Times'
            cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
            cmd=cmdout.communicate()[0].split(' ')
            freqn = '{0:02d}H'.format(int(cmd[1]))
        else:
            if freqvar[0:1] == '&':
                freqi = freqvar[1:Nfreqvar].split('+')[0]
                freqi = freqi.replace('&', '')
            else:
                freqi = freqvar

            if freqi[Nfreqvar-1:Nfreqvar] == 'H':
                freqn = freqi.split(':')[0]
                freqnchk = freqn
            elif freqi[Nfreqvar-1:Nfreqvar] == 'm':
                freqn = freqi.split(':')[0]
                freqnchk = freqn
            else:
                freqn=freqi[0:2] + 'M'
                freqnchk = freqi[0:3]
#
#               checking statistics name of the variable
                statname = statistics_name(str(wrfncxnj.var[varnvals[0]][1]) + levS,             \
                  str(wrfncxnj.var[varnvals[0]][3]), freqnchk)

        OUTfile = reqinf.mainframe['POSTPROJDIR'] + '/' + reqinf.gennames['experiment'] + '_' +  \
          freqn + '_' + str(reqinf.periods[varnvals[4]]) + '_' +                                 \
          str(wrfncxnj.var[varnvals[0]][1]) + statname + levS + '.nc'
        if not os.path.isfile(OUTfile):
#            print errmsg
            print '   ERROR.does_not_exist: ' + OUTfile
        else:
            outfiles.append(OUTfile)

#    print 'outfiles: ', outfiles
    return outfiles

class requested(object):
    """ Class to provide all the information of the requested[...] file as four different dictionaries:
    self.mainframe: all the environ variables
    self.gennames: experiment name
    self.periods: all the periods
    self.varvals: all the variables (word as [varname][freq])
    """
    def __init__(self, reqfile):
        errmsg = 'ERROR -- error -- ERROR -- error'

        reqf = open(reqfile, 'r')

        self.mainframe = {}
        self.gennames = {}
        self.periods = {}
        self.varvals = {}
  
        mframe = True
        gnames = False
        pers = False
        varvs = False

        for line in reqf:
            if line == '#GeneralNames#\n':
                mframe = False
                gnames = True
            elif line == '#Periods#\n':
                gnames = False
                pers = True
            elif line == '#Variables#\n':
                pers = False
                varvs = True

            if mframe == True and not line == '#MainFrame#\n' and not line == '\n':
                lineval = set_newline_val(line)
                self.mainframe[lineval[0]] = lineval[1]

            if gnames == True and not line == '#GeneralNames#\n' and not line == '\n':
                lineval = set_newline_val(line)
                self.gennames[lineval[0].replace('@','')] = lineval[1]

            if pers == True and not line == '#Periods#\n' and not line == '\n':
                lineval = set_newline_val(line)
                self.periods[lineval[0].replace('@','')] = lineval[1]

            if varvs == True and not line == '#Variables#\n' and not line == '\n':
                linevals = set_newline_val(line)
                if not linevals[0] == '#':
                    self.varvals[linevals[0] + linevals[3]] = linevals

        reqf.close()

class wrfncxnjTable(object):
    """ Class to provide all the variables from a 'wrfncxnj.table' file in a dictionary
    self.variables: entry for each variable
    """

    def __init__(self, wrfncxnjtable):
        wrfncxnjt = open(wrfncxnjtable, 'r')
        self.var = {}
        for line in wrfncxnjt: 
            lineval = set_newline_val_quota(line)
            if not lineval[0] == '#':
                self.var[lineval[0]] = lineval

        wrfncxnjt.close()

### Options

parser = OptionParser()
parser.add_option("-1", "--1file", dest="filename",
                  help="unique file to check (folder from '-f')", metavar="FILENAME")
parser.add_option("-d", "--domain", dest="domN",
                  help="domain number to check", metavar="DOM")
parser.add_option("-f", "--folder", dest="folder",
                  help="folder with the files (without a value, POSTPROJDIR will be used)", 
                  metavar="FOLDERNAME")
parser.add_option("-o", "--optionfiles", dest="opfile", type='choice', choices=['all', 'reqfile'],
                  help="which option for the files ('all': all files in folder, 'reqfile': only active variables in requestedfile)",
                  metavar="OPTION")
parser.add_option("-p", "--point", dest="point", type="int", 
                  nargs=2, help="point to check (2 values); -1 -1 all values", metavar="PT")
parser.add_option("-q", "--requestedvariables", dest="reqfile",
                  help="file with the requested variables", metavar="FILENAME")
parser.add_option("-r", "--referencedatefile", dest="refdatefile",
                  help="reference date of checking files [YYYY][MM][DD][HH][MI][SS] valid for all kind of outputs", 
                  metavar="DATE")
parser.add_option("-v", "--viewresults", dest="viewres", type="int", 
                  help="to see results (1), not see (0)", metavar="BOLEAN")
parser.add_option("-w", "--workingfolder", dest="workfold",
                  help="working folder ('chkfigs/' folder will be created)", metavar="FOLDERNAME")
(opts, args) = parser.parse_args()

### Static values
Ntheorvar = 5 # number of variables in each file
colA = np.asarray((0.75, 0., 0.), float)
colB = np.asarray((0., 0.75, 0.), float)
colC = np.asarray((0., 0., 0.75), float)

#######    #######
## MAIN
    #######
errmsg = 'ERROR -- error -- ERROR -- error '
warnmsg = 'warning *** WARNING '

# Checking number of files
##
if not os.path.isfile(opts.reqfile):
  print errmsg
  print '  File ' + opts.reqfile + ' does not exist !!'
  print errmsg
  quit()

postfiles = check_postfiles(opts.reqfile, opts.refdatefile)

requestfile = requested(opts.reqfile)
# number of files in the folder (according to non '#' values in requestedfile)
Ntheorfiles = len(requestfile.varvals)

#
# Ref date
refdateval = requestfile.mainframe['RefDate']
refdate = refdateval[0:4] + refdateval[5:7] + refdateval[8:10] + refdateval[11:13] +             \
  refdateval[14:16] + refdateval[17:19]
refdateval = dt.datetime(int(refdateval[0:4]), int(refdateval[5:7]), int(refdateval[8:10]),      \
  int(refdateval[11:13]), int(refdateval[14:16]),  int(refdateval[17:19]))

#
# Checking point
point=np.array(opts.point)
if point[0] == -1 and point[1] == -1:
  ptall = 1
else:
  ptall = 0

if ptall == 0:
  ptS=["%03d" % number for number in point]

#
# Experiment name
expname = requestfile.gennames['experiment']

#
# files
goodfiles = 0
badfiles = 0

if opts.folder is None:
  projfolder = requestfile.mainframe['POSTPROJDIR']
else:
  projfolder = opts.folder

if opts.filename is None:
  if opts.opfile == 'all':
    ins='ls ' + '-1 ' + projfolder + '/' + expname + '_*'
    cmdout=sub.Popen(ins, stdout=sub.PIPE, shell=True)
    cmd=cmdout.communicate()[0] 
    files=cmd.split('\n')
  else:
    files = postfiles
else:
  files=[projfolder + '/' + opts.filename]

Nfiles=len(files)

#`cat -n ${file} | grep '#Variables#' | awk '{print $1}'`

if not Nfiles == Ntheorfiles:
  print errmsg
  print '  folder does not have (', Nfiles,') all the theroetical amount of tiles ', Ntheorfiles
##  quit()

valfiles = []
for ifile in range(Nfiles):
  if opts.viewres == 1:
    sub.call('killall display ', shell=True)
#    sub.call('killall gedit ', shell=True)
  if ifile == 0:
    foldfigs = opts.workfold + '/chkfigs/'
    print 'foldfigs ', foldfigs

    sub.call('mkdir -p ' + foldfigs, shell=True)

  datefile=files[ifile].split('_')
  Nnamesec = len(datefile)
  freq = datefile[Nnamesec-3]
  period = datefile[Nnamesec-2]
  varsec = datefile[Nnamesec-1]
  varnf = varsec.split('.')[0]
# removing pressure level from variable name (when appl.)
##
  Nvarnf = len(varnf)
  if varnf[Nvarnf-4:Nvarnf].isdigit():
    varn = varnf[0:Nvarnf-4]
  else:
    varn = varnf

  if len(varsec.split('.')) == 3:
    gzip = True
  else:
    gzip = False

  dateIniPer = period.split('-')[0] + '0101000000'
  yearend = int(period.split('-')[1])
  dateEnd1Per = str(yearend) + '0101000000'
  yearend = yearend + 1
  dateEndPer = str(yearend) + '0101000000'

  yearmid = int(period.split('-')[0]) + int( (yearend - int(period.split('-')[0]) ) / 2)
  dateMidPer = str(yearmid) + '0101000000'

  print '  Varname : ' + varn + ' frequency: ' + freq + ' period: ' + dateIniPer + '-' + dateEndPer

  timesTOT=0

  filen=files[ifile]
  filestats = ncFileStats(filen, 'time', refdate)
#
# number of variables
  if filestats.nvars == Ntheorvar: 
    print errmsg
    print '  File has ', filestats.nvars, ' variables and should have ', Ntheorvar, ' !!!'
    print errmsg
    quit()
#
# difference between time-steps
  dTprev = filestats.dT
#  if ifile == 0:
#    dTprev = filestats.dT
#  else:
#    if not filestats.dT == dTprev: 
#      print errmsg
#      print '  time-step frequency in file has change from previous ', dTprev, ' to ', filestats.dT, ' !!!'
#      print errmsg
#      quit()
#    else:
#      dTprev = filestats.dT

  timesTOT = timesTOT + filestats.ntimes

  iniyr = int(period.split('-')[0])
  endyr = int(period.split('-')[1])
  NyearsPeriod = endyr - iniyr + 1
  tINFiniyr = frequence_postprocess(freq, iniyr, refdate, varn)
#
# yearly time-steps comparison and values statistics 
#    Matrix of values is overdimensioned (just in case of leap years, for example)
  if not ptall:
    if not freq[0:2] == 'MO' and not freq[0:2] == 'DA':
      allyrsvals = zeros([int(tINFiniyr.Ntimesteps+24/int(freq[0:2]) + 1),NyearsPeriod], float)
    elif freq[0:2] == 'DA':
      allyrsvals = zeros([int(tINFiniyr.Ntimesteps + 1),NyearsPeriod], float)
    else:
      allyrsvals = zeros([int(tINFiniyr.Ntimesteps + 1),NyearsPeriod], float)
    allyrsvals[:] = None
    resfilename = foldfigs + 'stats_check_POST_' + ptS[0] + '-' + ptS[1] + '_' + freq + '_' +     \
      period + '_' + varn + '.dat'
  else:
    dimxf = filestats.dims['x']
    dimyf = filestats.dims['y']
    print 'matrix size all domain all timesteps: ',int(tINFiniyr.Ntimesteps+1)*dimxf*dimyf,NyearsPeriod
    allyrsvals = zeros([int(tINFiniyr.Ntimesteps+1)*dimxf*dimyf,NyearsPeriod], float)
    allyrsvals[:] = None
    resfilename = foldfigs + 'stats_check_POST_All_' + freq + '_' + period + '_' + varn + '.dat'

  resfile = open(resfilename, 'w')

  resfile.write(' ' + filen + '\n')
  timesTOTtheor = 0
  txtlegend = []
  years = []
  for iyear in range(NyearsPeriod):
    years.append(iniyr + iyear)
    resfile.write('year= ' + str(iniyr + iyear) + '\n' )
    timestheorinf = frequence_postprocess(freq, iniyr + iyear, refdate, varn)

    if not ptall:
#
# Adding 1 second to the inital date. Values in files are for the time before the date in file
      FileValpt=get_xy_timesec(varn, 'time', filen, point, timestheorinf.first/3600, timestheorinf.last/3600)

      Stats=statsVal(FileValpt.varValPt)
#      print 'yr: ', iniyr + iyear, timestheorinf.first/3600, '<-->', timestheorinf.last/3600
      if Stats.Nokvalues > 0:
        valuesvar = FileValpt.varValPt
        valuest = FileValpt.timeVarVal*3600
        valuestime = np.arange(len(valuest))
        valuestime = valuest
      else:
        valuesvar = None
        valuestime = None
    else:
#
# Adding 1 second to the inital date. Values in files are for the time before the date in file
      FileValpt=get_2D_timesec(varn, opts.timename, filen, timestheorinf.first/3600, timestheorinf.last/3600)
      valuesvar = np.arange(FileValpt.varValPt.size)*1.
      valuesvar = np.reshape(FileValpt.varValPt, FileValpt.varValPt.size)
      Stats=statsVal(valuesvar)
      if Stats.Nokvalues > 0:
        valuest = FileValpt.timeVarVal*3600
        valuestime = np.arange(len(valuest))
        valuestime = valuest
      else:
        valuesvar = None
        valuestime = None

    Ndiffvalues=0
    if Stats.Nokvalues < 1:
      print 'No values for the ', iniyr + iyear, ' year !!'
      allyrsvals[:, iyear] = None
      txtlegend.append(str(iniyr + iyear) + '\n' + 'No values')
    else:
      if not freq[0:2] == 'MO':
        firstit = int((filestats.firstT - timestheorinf.first)/filestats.dT)
        lastit = firstit+len(valuesvar) 
        for it in range(len(valuestime)):
          itval = int((valuestime[it] - timestheorinf.first)/filestats.dT)
          allyrsvals[itval, iyear] = valuesvar[it]
      else:
        for it in range(len(valuestime)):
          itval=mondate(valuestime[it], refdate, 's') - 1
          allyrsvals[itval, iyear] = valuesvar[it]

#
# Time-steps (time values in the files are in hours)
      if not timestheorinf.Ntimesteps == len(FileValpt.timeVarVal):
        badfiles = badfiles + 1
        print errmsg
        print '  Theoretical ( ', timestheorinf.Ntimesteps,' ) and time values in file ( ',      \
          len(FileValpt.timeVarVal),' ) in file for year ', iniyr + iyear, ' do not coincide!!!'

        diffvalues = sorted(list(set(timestheorinf.timesteps) - set(valuestime) ))
        equalvalues = sorted(list(set(timestheorinf.timesteps) & set(valuestime) ))

        Ndiffvalues = len(diffvalues)
#        print 'ldiffvals: ', len(diffvalues), ' leqvals: ',len(equalvalues)
        if not len(diffvalues) == 0:
          if len(diffvalues) > len(equalvalues):
            if len(equalvalues) == 0:
              resfile.write('  File has not any value !!' + '\n')
            else:
              resfile.write('   File only has values for : ' + '\n')

              for it in range(len(equalvalues)/6 + 2):
                if 6*it+5 <= len(equalvalues) -1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                  datediff3 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                  datediff4 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                  datediff5 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+4]))).strftime("%Y%m%d%H%M%S")
                  datediff6 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+5]))).strftime("%Y%m%d%H%M%S")
                else:
                  if 6*it == len(equalvalues) -1:
                    datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                    datediff2 = datediff3 = datediff4 = datediff5 = datediff6 = ''
                  elif 6*it+1 == len(equalvalues) -1:
                    datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                    datediff2 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                    datediff3 = datediff4 = datediff5 = datediff6 = ''
                  elif 6*it+2 == len(equalvalues) -1:
                    datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                    datediff2 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                    datediff3 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                    datediff4 = datediff5 = datediff6 = ''
                  elif 6*it+3 == len(equalvalues) -1:
                    datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                    datediff2 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                    datediff3 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                    datediff4 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                    datediff5 = datediff6 = ''
                  elif 6*it+4 == len(equalvalues) -1:
                    datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                    datediff2 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                    datediff3 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                    datediff4 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                    datediff5 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+4]))).strftime("%Y%m%d%H%M%S")
                    datediff6 = ''
                  elif 6*it+5 == len(equalvalues) -1:
                    datediff1 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                    datediff2 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                    datediff3 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                    datediff4 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                    datediff5 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+4]))).strftime("%Y%m%d%H%M%S")
                    datediff6 = (refdateval + dt.timedelta(seconds = int(equalvalues[6*it+5]))).strftime("%Y%m%d%H%M%S")
                  else:
                    break

                resfile.write('     ' + ' ' + datediff1 + ' ' + datediff2 + ' ' + datediff3  + ' '\
                  + datediff4  + ' ' + datediff5  + ' ' + datediff6 + '\n')
          else:
            resfile.write('   File has not value for : ' + '\n')
            for it in range(len(diffvalues)/6 + 2):
              if 6*it+5 < len(diffvalues) -1:
                datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                datediff2 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                datediff3 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                datediff4 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                datediff5 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+4]))).strftime("%Y%m%d%H%M%S")
                datediff6 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+5]))).strftime("%Y%m%d%H%M%S")
              else:
                if 6*it == len(diffvalues) - 1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = datediff3 = datediff4 = datediff5 = datediff6 = ''
                elif 6*it + 1 == len(diffvalues) - 1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                  datediff3 = datediff4 = datediff5 = datediff6 = ''
                elif 6*it + 2 == len(diffvalues) - 1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                  datediff3 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                  datediff4 = datediff5 = datediff6 = ''
                elif 6*it + 3 == len(diffvalues) - 1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                  datediff3 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                  datediff4 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                  datediff5 = datediff6 = ''
                elif 6*it + 4 == len(diffvalues) - 1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                  datediff3 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                  datediff4 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                  datediff5 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+4]))).strftime("%Y%m%d%H%M%S")
                  datediff6 = ''
                elif 6*it + 5 == len(diffvalues) - 1:
                  datediff1 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it]))).strftime("%Y%m%d%H%M%S")
                  datediff2 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+1]))).strftime("%Y%m%d%H%M%S")
                  datediff3 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+2]))).strftime("%Y%m%d%H%M%S")
                  datediff4 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+3]))).strftime("%Y%m%d%H%M%S")
                  datediff5 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+4]))).strftime("%Y%m%d%H%M%S")
                  datediff6 = (refdateval + dt.timedelta(seconds = int(diffvalues[6*it+5]))).strftime("%Y%m%d%H%M%S")
                else:
                  break

              resfile.write('     ' + datediff1  + ' ' + datediff2  + ' ' + datediff3  + ' ' +   \
                datediff4  + ' ' + datediff5  + ' ' + datediff6 + '\n')

          print '  ', len(diffvalues), ' values are not in the file !!'
#        print errmsg
#        quit()

      else:
        goodfiles = goodfiles + 1

      timesTOTtheor = timesTOTtheor + timestheorinf.Ntimesteps
#
# Values
      if not Stats.Nokvalues > 1:
        print '   File has 1 value NO statistics checks of the values in the file'
      else:
        inquantile = Stats.quantilesv[15] - Stats.quantilesv[5]
        if Stats.minv < Stats.medianv - 2.5*inquantile:
          print warnmsg
          print '  ', iniyr + iyear,' minimum value: ',Stats.minv, ' is less than ',              \
            Stats.medianv - 2.5*inquantile, '(median - 2.5*(q75-q25)) !!!'
          resfile.write(warnmsg + '\n')
          resfile.write('  minimum value: ' + str(Stats.minv) + ' is less than ' +                \
            str(Stats.medianv - 2.5*inquantile) + ' (median - 2.5*(q75-q25)) !!!' + '\n')
        if Stats.maxv > Stats.medianv + 2.5*inquantile:
          print warnmsg
          print '  ',iniyr + iyear,' maximum value: ',Stats.maxv, ' is more than ', Stats.medianv+\
            2.5*inquantile, ' (median + 2.5*(q75-q25)) !!!'
          resfile.write(warnmsg + '\n')
          resfile.write('  maximum value: ' + str(Stats.maxv) + ' is more than ' +                \
            str(Stats.medianv + 2.5*inquantile) + ' (median + 2.5*(q75-q25)) !!!' + '\n')
        if Stats.absmaxstepv > 7.5*Stats.stdnomaxstepv:
          print warnmsg
          print '  ', iniyr + iyear,' maximum abs temporal increment: ',Stats.absmaxstepv,        \
            ' is more than ', 7.5*Stats.stdnomaxstepv,                                            \
            ' (7.5*stdnomaxstepv [std without absmaxstepv]) !!!'
          resfile.write(warnmsg + '\n')
          resfile.write('  maximum abs temporal increment: ' + str(Stats.absmaxstepv) +           \
            ' is more than ' + str(7.5*Stats.stdnomaxstepv) +                                     \
            ' (7.5*stdsnomaxtepv [std without absmaxstepv]) !!!' + '\n')

      txtlegend.append('year= ' + str(iniyr + iyear) + '\n' + 'theoretical number of time-steps: '\
        + str(timestheorinf.Ntimesteps) + '\n' + 'total time steps in file: ' +                   \
        str(len(FileValpt.timeVarVal)) + '\n' + 'N non-missing: ' + str(Stats.Nokvalues) + '\n' + \
        'N missed: ' + str(Ndiffvalues) + '\n' + 'min: ' + '{0:4.2e}'.format(float(Stats.minv)) + \
        '\n' + 'max: ' + '{0:4.2e}'.format(float(Stats.maxv)) + '\n' + 'mean: ' +                 \
        '{0:4.2e}'.format(float(Stats.meanv)) + '\n' + 'median: ' +                               \
        '{0:4.2e}'.format(float(Stats.medianv)) + '\n' + 'std. dev.: ' +                          \
        '{0:4.2e}'.format(float(Stats.stdv)) + '\n')

#
# Values/File statistics:
      resfile.write(txtlegend[iyear])
      resfile.write(' ' + '\n')


#  txtTOTlegend='N years period: ' + str(NyearsPeriod) + '\n' + 'total time steps: ' + str(timesTOT) + '\n' + 'min: ' + str(Stats.minv) + '\n' + 'max: ' + str(Stats.maxv) + '\n' + 'mean: ' + str(Stats.meanv) + '\n' + 'median: ' + str(Stats.medianv) + '\n' + 'std. dev.: ' + str(Stats.stdv) + '\n'

  if not timesTOTtheor == timesTOT:
    print errmsg
    print '  Total theoretical time steps of the period (' + dateIniPer + '-' + dateEndPer + '):',\
      timesTOTtheor, ' does not coincide with the total number of time-steps ', timesTOT , ' from the files !!!'
#    print errmsg

# Plotting
#
  if not Stats.Nokvalues > 1:
    print '   File has 1 value NO plotting checks of the values in the file'
  else:
    if not ptall:
      graphname = foldfigs + 'check_POST_' + ptS[0] + '-' + ptS[1] + '_' + freq + '_' + period + \
        '_' + varn
      graphnameyr = foldfigs + 'yr_check_POST_' + ptS[0] + '-' + ptS[1] + '_' + freq + '_' +     \
         period + '_' + varn
      titletxt = 'Yearly Boxplots for ' + freq  + ' ' + varn + ' at ' + ptS[0] + ', ' + ptS[1] + \
         '\n period ' + dateIniPer + '-' + dateEndPer
      titletxtyr = 'Yearly evolutions for ' + freq  + ' ' + varn + ' at ' + ptS[0] + ', ' +ptS[1]\
         + '\n period ' + dateIniPer + '-' + dateEndPer
    else:
      graphname = foldfigs + 'check_POST_All_' + freq + '_' + period + '_' + varn
      graphnameyr = foldfigs + 'yr_check_POST_All_' + freq + '_' + period + '_' + varn
      titletxt = 'Yearly Boxplots for ' + freq  + ' ' +  varn + ' all domain ' + '\n period ' +  \
        dateIniPer + '-' + dateEndPer
      titletxtyr = 'Yearly evolutions for ' + freq  + ' ' +  varn + ' all domain ' +'\n period '+\
        dateIniPer + '-' + dateEndPer

    plt.pyplot.hold(True)
    title(titletxt)
    xlabel('distributions of each year')
    ylabel(varn + ' (' + FileValpt.varUnits + ')')
    plt.pyplot.boxplot(allyrsvals)

    ax = plt.pyplot.gca()
    ax.set_autoscale_on(False)
    plt.pyplot.xlim(-0.5, 2.0 + NyearsPeriod)
    if NyearsPeriod == 1:
      iyr1 = 1
      plt.pyplot.text(float((iyr1 - 0.4*(iyr1 % 2))/((NyearsPeriod)*1.)), 0.5 + 0.20*(iyr1 % 2), \
        txtlegend[iyr1 - 1], transform = ax.transAxes, fontsize=8)  

    plt.pyplot.xticks(np.arange(NyearsPeriod) + 1, years, rotation=37)
    savefig(graphname)
    plt.pyplot.hold(False)
    plt.pyplot.close('all')
    if opts.viewres == 1:
      sub.call('display ' + graphname + '.png &', shell=True)

# Yearly plot
#
    ratioyr = float(timestheorinf.Ntimesteps*1./365.)

    if freq[0:2] == 'MO':
      NOmon=15.*ratioyr
      mondats=[15.*ratioyr, 46.*ratioyr, 74.*ratioyr, 105.*ratioyr, 135.*ratioyr, 166.*ratioyr,  \
        196.*ratioyr, 227.*ratioyr, 257.*ratioyr, 288.*ratioyr, 318.*ratioyr, 349.*ratioyr]
      for it in range(12):
        mondats[it]=mondats[it] - NOmon
    else:
      mondats=[15.*ratioyr, 46.*ratioyr, 74.*ratioyr, 105.*ratioyr, 135.*ratioyr, 166.*ratioyr,  \
        196.*ratioyr, 227.*ratioyr, 257.*ratioyr, 288.*ratioyr, 318.*ratioyr, 349.*ratioyr]

    montics=['Jan-15', 'Feb-15', 'Mar-15', 'Apr-15', 'Mai-15', 'Jun-15', 'Jul-15', 'Aug-15',     \
      'Sep-15', 'Oct-15', 'Nov-15', 'Dec-15']
    plt.pyplot.hold(True)
    title(titletxtyr)
    xlabel('temporal evolution for each year')
    ylabel(varn + ' (' + FileValpt.varUnits + ')')
    for it in range(NyearsPeriod):
      plt.pyplot.plot(allyrsvals[:,it], label=years[it], color=rgb_3val(colA, colB, colC,        \
        it + 1, NyearsPeriod))

    plt.pyplot.xlim(0., 11.)
    plt.pyplot.xticks(mondats, montics, size=8, rotation=37)
    plt.pyplot.text(0.9, 0.2, dateIniPer, color=colA, transform = ax.transAxes)
    plt.pyplot.text(0.9, 0.15, dateMidPer, color=colB, transform = ax.transAxes)
    plt.pyplot.text(0.9, 0.1, dateEnd1Per, color=colC, transform = ax.transAxes)
    savefig(graphnameyr)
    plt.pyplot.hold(False)
    plt.pyplot.close('all')
 
    if opts.viewres == 1:
      sub.call('display ' + graphnameyr + '.png &', shell=True)

  if opts.viewres == 1:
    sub.call('gedit ' + resfilename + ' &', shell=True)

  resfile.close()
#  quit()

print 'Folder for the figures has been created: ' + foldfigs
print 'Of ' + str(Nfiles) + ' files there are: '
print '  ' + str(goodfiles) + ' correct'
print '  ' + str(badfiles) + ' incorrect'

