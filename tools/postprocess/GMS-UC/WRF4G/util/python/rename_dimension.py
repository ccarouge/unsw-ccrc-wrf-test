##!/usr/bin/python
# e.g. ccrc468-17 # python WRF4G/util/python/rename_dimension.py -d gsize,bnds -f 123/CCRC_NARCliM_Sydney_01H_1990-1999_tasmax.nc -v tasmax

from optparse import OptionParser
import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
import subprocess as sub
import time

def searchInlist(listname, nameFind):
  for x in listname:
    if x == nameFind:
      return True
  return False

### Options

parser = OptionParser()
parser.add_option("-d", "--dimension", dest="dimname",
                  help="dimension to rename [oldname],[newname]", metavar="DIM_NAME")
parser.add_option("-f", "--netCDF_file", dest="ncfile", 
                  help="file to use", metavar="FILE")
parser.add_option("-v", "--variablename", dest="varname", 
                  help="main variable of the file", metavar="VAR")
(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #
errormsg='ERROR -- error -- ERROR -- error'

if not os.path.isfile(opts.ncfile):
  print errormsg
  print '  File ' + opts.ncfile + ' does not exist !!'
  print errormsg
  quit()

fold=os.path.dirname(opts.ncfile)

dimns = opts.dimname.split(',')

ncftest = NetCDFFile(opts.ncfile, 'r')

if not ncftest.dimensions.has_key(dimns[0]):
  print errormsg
  print '  File does not have dimension ' + dimns[0] + ' !!!!'
  print errormsg
  quit()

ncftest.close

try:
  os.rename(opts.ncfile, fold + '/tmp.nc')
except OSError as e:
  if not e.errno == 0:
    print 'mv ' + opts.ncfile + ' ' + fold + '/tmp.nc'
    print e
    quit()

ncfold = NetCDFFile(fold + '/tmp.nc', 'r')
ncfnew = NetCDFFile(opts.ncfile,'w')

#
# Copying dimensions
dimnames = ncfold.dimensions.keys()
Ndims = len(dimnames)

for idim in range(Ndims):
##  print str(idim) + ' ' + dimnames[idim]
  if not ncfnew.dimensions.has_key(dimnames[idim]):
    if not dimnames[idim] == dimns[0]:
      olddim = ncfold.dimensions[dimnames[idim]]
      if dimnames[idim] == 'time':
        dim = ncfnew.createDimension(dimnames[idim], None)
      else:
        dim = ncfnew.createDimension(dimnames[idim], len(olddim))
    else:
      olddim = ncfold.dimensions[dimnames[idim]]
      dim = ncfnew.createDimension(dimns[1], len(olddim))
    
ncfnew.sync()
#
# Copying variables

varnames = ncfold.variables.keys()
Nvars = len(varnames)

for ivar in range(Nvars):
##  print str(ivar) + ' ' + varnames[ivar]
  if not varnames[ivar] == 'time_bnds':
    oldvar = ncfold.variables[varnames[ivar]]
    oldvaratrs = oldvar.ncattrs()
    dimvarn = oldvar.dimensions
    vartype = oldvar.dtype
    if varnames[ivar] == opts.varname and searchInlist(oldvaratrs, '_FillValue'):
      print 'Variable with _FillValue attribute !'
      newvar = ncfnew.createVariable(varnames[ivar], vartype, dimvarn, fill_value=oldvar.getncattr('_FillValue'))
    else:
      newvar = ncfnew.createVariable(varnames[ivar], vartype, dimvarn)
    newvar[:] = oldvar[:]
  else:
    print 'Creating time_bnds as time, bnds'
    oldvar = ncfold.variables[varnames[ivar]]
    dimvarn = oldvar.dimensions
    vartype = oldvar.dtype
    newvar = ncfnew.createVariable(varnames[ivar], vartype, ('time', 'bnds'))
    newvar[:] = oldvar[:]

  if not varnames[ivar] == 'time_bnds':
    for iatr in range(len(oldvaratrs)):
      oldatr = oldvar.getncattr(oldvaratrs[iatr])
      newvarattr = newvar.ncattrs()
      if not oldvaratrs[iatr] == '_FillValue':
        if searchInlist(newvarattr, oldvaratrs[iatr]):
          atr = newvar.delncattr(oldvaratrs[iatr])

        newatr = newvar.setncattr(oldvaratrs[iatr], oldatr)

ncfnew.sync()

#
# Copying atributes
oldvaratrs = ncfold.ncattrs()

for iatr in range(len(oldvaratrs)):
##  print str(iatr) + ' ' + oldvaratrs[iatr]
  oldatr = ncfold.getncattr(oldvaratrs[iatr])
  newatr = ncfnew.setncattr(oldvaratrs[iatr], oldatr)

histval = ncfold.getncattr('history')
newhist = time.ctime(time.time()) + ': rename_dimension.py -d ' + opts.dimname + ' -f ' + opts.ncfile + '\n' + histval
newh = ncfnew.setncattr('history', newhist)

ncfnew.sync()

ncfold.close
ncfnew.close

