#!/bin/bash
## g.e. # writting_periods.bash 1990 2009 1:5:10:20
if test $1 = '-h'; then
  echo "***********************"
  echo "*** Script to write ***"
  echo "***   periods of a  ***"
  echo "***  requested file ***"
  echo "***********************"
  echo "writting_periods.bash [InitialYear]([YYYY] format) [EndingYear]([YYYY] format) [yerfreq](year frequencies [frq1]:[frq2]:...:[frqn])"
else
  rootsh=`pwd`
  iniyr=$1
  endyr=$2
  yrfreqsl=$3

  yrfreqs=`echo ${yrfreqsl} | tr ':' ' '`

  iper=1
  perf='periods_file.txt'
  echo "#Periods#" > ${perf}
  for yrfreq in ${yrfreqs}; do
    iyr=$iniyr
    if test ${yrfreq} -eq 1; then
      nyrfreq=0
    else
      nyrfreq=1
    fi
    while test ${iyr} -le ${endyr}; do
      iyrfrq=`expr ${iyr} + ${yrfreq} - 1`
      if test ${yrfreq} -lt 10; then
        nyrfreqS=`echo ${nyrfreq} | awk '{printf("%02d",$1)}'`
      else
        nyrfreqS=${nyrfreq}
      fi

      echo "@per"${yrfreq}"y"${nyrfreqS}"@ "${iyr}"-"${iyrfrq} >> ${perf}
      iyr=`expr ${iyr} + ${yrfreq}`
      iper=`expr ${iper} + 1`
      nyrfreq=`expr ${nyrfreq} + 1`
    done ## end of years of the period
  done ## end of year frequencies
  cat ${perf}
fi
