#!/bin/bash 
## e.g. # ./correcting_folder.bash /home/lluis/UNSW-CCRC-WRF/tools/Scripts/NARCliM/apps/123 CCRC_NARCliM_Sydney /home/lluis/UNSW-CCRC-WRF/tools  03H hus0850 1997-1997 NNRP requested_NNRP_Std_1990-2009.inf 1:1:wrfncxnj.projection_CCRC_NARCliM_Sydney
if test $1 = '-h'; then
  echo "**********************************"
  echo "*** Script to transform 'time' ***"
  echo "***  to an unlimited dimension ***"
  echo "***    and some minor issues   ***"
  echo "**********************************"
  echo "./correcting_folder.bash [fold] [head] [TOOLfold] [freq] [vars] [periods] [GCM]"        \
    " [requestedfile] [time_bnds]:[proj]:[projfile]"
  echo "   [fold]: folder name "
  echo "   [head]: head of the files to correct "
  echo "   [TOOLfold]: folder with the tools "
  echo "   [freq]: frequencies [FRQ1]:[FRQ2]:... (MON, DAY, ...) or all "
  echo "   [vars]: variables [VAR1]:[VAR2]:... or all "
  echo "   [periods]: preiods ([YYY1s]-[YYY1e]:[YYY2s]-[YYY2e]:...) or all "
  echo "   [GCM]: name of the gcm, must meet the globalattr file "
  echo "   [requestedfile]: file from which come the post-processed files "
  echo "   [time_bnds]:[proj]:[projfile]: have to be create 'time_bnds'? (0: no,"               \
    " 1:yes) Have to add projection information from [projfile]? (0: no, 1:yes)"
  echo " Shell relays in the fact that it will find (or maybe not) ___ __ _"
  echo "   * [gcm]_add-attr.inf: file with the global attributes to add to the files of [gcm]" 
  echo "   * [gcm]_rm-attr.inf: file with the global attributes to remove from files of [gcm]" 
  echo "   * [freq]_add-attr.inf: file with the attributes to add at any variable of [freq]" 
  echo "   * [freq]_rm-attr.inf: file with the attributes to remove from any variable of [freq]" 
  echo "   * [freq]_[var]_add-attr.inf: file with the attributes to add at [var] of [freq]" 
  echo "   * [freq]_[var]_rm-attr.inf: file with the attributes to remove from [var] of [freq]" 

else

allvars_folder() {
# Function to retrieve all the name of the variables from a folder/header combiation
  foldn=$1
  fhead=$2

  Nheader=`echo ${fhead} | tr '_' ' ' | wc -w`
  Nvar=`expr ${Nheader} + 3`

  here=`pwd`
  cd ${foldn}
  echo " " > allvarsrep.inf
  for filevar in ${fhead}*.nc; do
    varn=`echo ${filevar} | tr '_' '\n' | head  -n ${Nvar} | tail -n 1 | tr '.' ' ' | awk '{print $1}'`
    echo ${varn} >> allvarsrep.inf
  done # end of files
  allvars=`cat allvarsrep.inf | sort | uniq`

  rm allvarsrep.inf
  cd ${here}

##  varNs=''
##  for filevar in ${foldn}/${fhead}*; do
##    varval=`ncdump -h ${filevar} | grep float | awk '{print $2}' | tr '(' ' ' | awk '{print $1}'`
##  done # end of files

  echo ${allvars}
}

allpers_folder() {
# Function to retrieve all the name of the periods from a folder/header combiation
  foldn=$1
  fhead=$2

  Nheader=`echo ${fhead} | tr '_' ' ' | wc -w`
  Nper=`expr ${Nheader} + 2`

  here=`pwd`
  cd ${foldn}
  echo " " > allpersrep.inf
  for filevar in ${fhead}*.nc; do
    pern=`echo ${filevar} | tr '_' '\n' | head  -n ${Nper} | tail -n 1 | tr '.' ' ' | awk '{print $1}'`
    echo ${pern} >> allpersrep.inf
  done # end of files
  allpers=`cat allpersrep.inf | sort | uniq`

  rm allpersrep.inf
  cd ${here}

  echo ${allpers}
}

allfreq_folder() {
# Function to retrieve all the name of the frequencies from a folder/header combiation
  foldn=$1
  fhead=$2

  Nheader=`echo ${fhead} | tr '_' ' ' | wc -w`
  Nfrq=`expr ${Nheader} + 1`

  here=`pwd`
  cd ${foldn}
  echo " " > allfrqsrep.inf
  for filevar in ${fhead}*.nc; do
    frqn=`echo ${filevar} | tr '_' '\n' | head  -n ${Nfrq} | tail -n 1 | tr '.' ' ' | awk '{print $1}'`
    echo ${frqn} >> allfrqsrep.inf
  done # end of files
  allfrqs=`cat allfrqsrep.inf | sort | uniq`

  rm allfrqsrep.inf
  cd ${here}

  echo ${allfrqs}
}

head_in_file() {
# Function to include a generic header in a file name
  filen=$1
  head=$2

  nfile=`basename ${filen}`
  nfold=`dirname ${filen}`

  echo ${nfold}/${head}_${nfile}
}

function check_time_file() {
# File time-steps it is assumed that file is in CF-compilant format ('time' is the name of variable time)
# 
### Variables
# varN: name of the variable
# netcdf: netcdf file
# wrfk: kind of wrf file
# frq: name of the desired frequency
# nlstout: 'namelist.output' as reference of the simulations
# domN: number of the domain
# WRF4h: place with the WRF4G structure ( '$WRF4h/util/python' )
# 
  varN=$1
  netcdf=$2
  wrfk=$3
  frqn=$4
  nlstout=$5
  domN=$6
  WRF4h=$7

  refdateYmdHMS=${RefDate:0:4}${RefDate:5:2}${RefDate:8:2}${RefDate:11:2}${RefDate:14:2}${RefDate:17:2}

##  filetimeinf=`$WRF4h/util/python/nc_time_tools.py -f ${netcdf} -t time -r ${refdateYmdHMS} -F 1 -o timeINF`
##  Ntimes=`$WRF4h/util/Fortran/nc_file_inf ${netcdf} | grep -a --text DIMinf | grep time | awk '{print $4}'`
##  filetimes=`$WRF4h/util/Fortran/nc_var_out ${netcdf} time 1 1 1 1 1 1 -1 1 1 1 1 1 | grep -v '#'`
##  file1sttime=`echo ${filetimes} | awk '{print $1}' | tr 'D' 'E'`
##  filelasttime=`echo ${filetimes} | tr ' ' '\n' | tail -n 1 | tr 'D' 'E'`
##  difftime=`echo ${filelasttime}" "${file1sttime}" "${Ntimes}" 1" | awk '{print ($1 - $2)/( $3 - $4)}'`

  simTimeInf=`get_Ntimes_namelist ${nlstout} ${domN} ${wrfk}`

  case ${wrfk} in 
    'xtrm')
#
# extreme values use 'time_step' to compute the results 'xtrm' output assuming from clWRF modifications
#
      timefileinf=`echo ${simTimeInf} | awk '{print $3}'`
      timefileinf='point@values@'${timefileinf}'@second'

      xtrmstat=`check_stats_varn ${varN}`
      xtrmperiod=`echo ${simTimeInf} | awk '{print $1/60}'`
      case ${xtrmstat} in
        'min')           timefileinf='minimum@'${xtrmperiod}'@hour@value@from@'${timefileinf}               ;;
        'max')           timefileinf='maximum@'${xtrmperiod}'@hour@value@from@'${timefileinf}               ;;
        'mean')          timefileinf='mean@'${xtrmperiod}'@hour@value@from@'${timefileinf}                  ;;
        'std')           timefileinf='standard@deviation@'${xtrmperiod}'@hour@value@from@'${timefileinf}    ;;
      esac
    ;;
    *)
      timefileinf=`echo ${simTimeInf} | awk '{print $1/60}'`
      timefileinf='point@values'@${timefileinf}'@hour'
    ;;
  esac

# Specific cases
#
  case ${varN} in
    'pr5max')        timefileinf='maximum@5@minute@values@from@'${timefileinf}     ;;
    'pr10max')       timefileinf='maximum@10@minute@values@from@'${timefileinf}    ;;
    'pr20max')       timefileinf='maximum@20@minute@values@from@'${timefileinf}    ;;
    'pr30max')       timefileinf='maximum@30@minute@values@from@'${timefileinf}    ;;
    'pr1Hmax')       timefileinf='maximum@1@hour@values@from@'${timefileinf}       ;;
    'wss5max')       timefileinf='maximum@5@minute@values@from@'${timefileinf}     ;;
    'wss10max')      timefileinf='maximum@10@minute@values@from@'${timefileinf}    ;;
    'wss20max')      timefileinf='maximum@20@minute@values@from@'${timefileinf}    ;;
    'wss30max')      timefileinf='maximum@30@minute@values@from@'${timefileinf}    ;;
    'wss1Hmax')      timefileinf='maximum@1@hour@values@from@'${timefileinf}       ;;
    'pr5maxtstep')   timefileinf='maximum@5@minute@values@from@'${timefileinf}     ;;
    'pr10maxtstep')  timefileinf='maximum@10@minute@values@from@'${timefileinf}    ;;
    'pr20maxtstep')  timefileinf='maximum@20@minute@values@from@'${timefileinf}    ;;
    'pr30maxtstep')  timefileinf='maximum@30@minute@values@from@'${timefileinf}    ;;
    'pr1Hmaxtstep')  timefileinf='maximum@1@hour@values@from@'${timefileinf}       ;;
    'wss5maxtstep')  timefileinf='maximum@5@minute@values@from@'${timefileinf}     ;;
    'wss10maxtstep') timefileinf='maximum@10@minute@values@from@'${timefileinf}    ;;
    'wss20maxtstep') timefileinf='maximum@20@minute@values@from@'${timefileinf}    ;;
    'wss30maxtstep') timefileinf='maximum@30@minute@values@from@'${timefileinf}    ;;
    'wss1Hmaxtstep') timefileinf='maximum@1@hour@values@from@'${timefileinf}       ;;
  esac

  echo ${timefileinf}
}


function get_Ntimes_namelist(){
# Function to get Ntimes from a namelist.output of a given kind of wrf file and a given domain
#
### Variables
# namlst: 'namelist.output' to use to find the 
# wrfdom: wrf domain
# fk: wrf kind of file 'out', 'xtrm', 'dly', 'hrly',... (must much with value in namelist.output as 'AUXHIST[NN]_OUTNAME')

  namlst=$1
  wrfdom=$2
  fk=$3

  auxhist=`cat ${namlst} | grep ${fk} | awk '{print $1}' | tr '_' ' ' | awk '{print $1}'`
  histfrq=`cat ${namlst} | grep ${auxhist}_INTERVAL | grep -v _[YDHMS] ` # Assuming that [AUXHIST]_INTERVAL_[Y/D/H/M/S] is not used
# interval
#
  nvalues=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
  if test ${nvalues} -ge $wrfdom; then
    histfrq=`echo $histfrq | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  else
    histfrq=`echo $histfrq | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
  fi
# frames x file
# 
  if test ${fk} = 'out'; then
    nvalues=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
    if test  ${nvalues} -ge $wrfdom; then
      framesfile=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    else
      framesfile=`cat ${namlst} | grep FRAMES_PER_OUTFILE | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    fi
  else
    nvalues=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $1}'`
    if test  ${nvalues} -ge $wrfdom; then
      framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $3}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    else
      framesfile=`cat ${namlst} | grep FRAMES_PER_${auxhist} | awk '{print $4}' | tr '*' ' ' | awk '{print $2}' | tr ',' ' '`
    fi
  fi

# Internal time_step (seconds)
#
  timeStepSec=`cat ${namlst} | grep ' TIME_STEP ' | awk '{print $3}' | tr ',' ' '`
  echo $histfrq" "$framesfile" "$timeStepSec
}


function check_stats_frqn() {
# Function to check the name of a frquency output file ('@' will be transformed to a ' ' )
  freqn=$1

  TimeWindow=${freqn:0:2}
  statfrq=${freqn:2:1}

  if test ${statfrq} = 'H'; then
    Tunit='hour'
  else
    Tunit='month'
  fi

  case $TimeWindow in
    'DA')   timeval='daily@values'                ;;
    'MO')   timeval='monthly@values'              ;;
    'SE')   timeval='seasonal@values'             ;;
    'YE')   timeval='yearly@values'               ;;
    '24')   timeval='24@'${Tunit}'@point@value'   ;;
    '12')   timeval='12@'${Tunit}'@point@value'   ;;
    '06')   timeval='6@'${Tunit}'@point@value'    ;;
    '03')   timeval='3@'${Tunit}'@point@value'    ;;
    '01')   timeval='1@'${Tunit}'@point@value'    ;;
  esac
  case $statfrq in
    'M')   statN='mean@within@'                   ;;
    'S')   statN='accumulation@within@'           ;;
    'N')   statN='minimum@within@'                ;;
    'X')   statN='maximum@within@'                ;;
    'H')   statN='every@'                         ;;
    'm')   statN='every@'                         ;;
    *)     statN='NONE'                           ;;
  esac
  echo ${timeval}" "${statN}
}


function check_stats_varn() {
# Function to check if variable name has a statistic section
  varN=$1
  LvarN=`expr length ${varN}`
  statsnames='min max mean ac mintstep maxtstep meantstep actstep'

  statsvar='0'
  for statsn in ${statsnames}; do
    Lstatsn=`expr length ${statsn}`
    Lvarsec=`expr $LvarN - $Lstatsn`
    varsec=${varN:${Lvarsec}:${Lstatsn}}
    if test ${Lvarsec} -gt 1; then
      if test ${varsec} = ${statsn}; then
#
# Removing 'tstep' surname
        if test $Lstatsn -gt 4; then
          Llvarsec=`expr length $varsec`
          LvarsecNOtstep=`expr $Llvarsec - 5`
          varsec=${varsec:0:${LvarsecNOtstep}}
        fi
        statsvar=${varsec}
      fi
    fi
  done # end of statsnames
  echo ${statsvar}
}

function CF_CORDEX_cellmethods() {
# Function to provide to the netCDF files with the variable cellmethod attributes of the CORDEX specifications
#
### Variables
# ncf: netCDF file
# varn: name of the variable
# wrfk: kind of wrf out 'out', 'xtrm', 'hrly', 'dly'
# freqn: name of the frequency 'All', '[DA/MO/SE/YE][M/S/X/N]', ...
# WRF4Ghome: Place with the structure of the WRF4G ($WRF4Ghome/util/python)

  ncf=$1
  varn=$2
  wrfk=$3
  freqn=$4
  nlstout=$5
  dom=$6
  WRf4Ghome=$7
  
# Is variable a statistic field?
# 
  varstat=`check_stats_varn ${varn}`

# Is frequency a statistic field?
# 
  celltime_method='time:@'
  if test ! ${freqn} = 'All'; then
    freqvlues=`check_stats_frqn ${freqn}`
    timeName=`echo ${freqvlues} | awk '{print $1}'`
    statName=`echo ${freqvlues} | awk '{print $2}'`
    if test ! $statName = 'NONE'; then
      celltime_method=${celltime_method}${statName}${timeName}'@'
    else
      celltime_method=${celltime_method}
    fi
  else
    statName='NO'
  fi

# Checking if frequency output is the same as variable frequency
#
  simTInf=`get_Ntimes_namelist ${nlstout} ${dom} ${wrfk} | awk '{print $1}'`
  simTHInf=`expr ${simTInf} / 60`

  equalFrqSimT='0'
  if test $statName = 'every@'; then
    hourfrq=`expr ${freqn:0:2} + 0`
    if test ${hourfrq} -eq ${simTHInf} && test ! ${freqn:2:1} = 'm'; then # The extra if for ${freqn:2:1} = 'm' means that must be better done!
      celltime_method='time:@'
      equalFrqSimT='1'
    fi
  fi

# Original frequency time of the fields
#
  if test ${freqn} == 'All' || test ${equalFrqSimT} -eq 1; then
    filetime=`check_time_file ${varn} ${ncf} ${wrfk} ${freqn} ${nlstout} ${dom} ${WRF4Ghome}`
    celltime_method=${celltime_method}${filetime}
  fi

  echo ${celltime_method}
}

function kinds_CFvar() {
# Functino to guess which kind of wrfoutput has a given CFvariable according to a given requested file
  CFvar=`echo $1 | tr -d [:digit:] `
  reqfile=$2

  wrfnctable=`cat ${reqfile} | grep WRF4Ghome | awk '{print $2}'`
  wrfnctable=${wrfnctable}'/util/postprocess/wrfncxnj/wrfncxnj.table'
  WRFnames=`cat ${wrfnctable} | grep ${CFvar} | awk '{print $1}' `
  for wrfvar in ${WRFnames}; do
    wrfkindA=`cat ${reqfile} | grep ${wrfvar} | grep -v \# | awk '{print $2}' | sort | uniq`
    wrfkind0=${wrfkindA}'0'
    Lwrfkind0=`expr length ${wrfkind0}`
    if test ${Lwrfkind0} -gt 1 && test ${wrfkind0} -ne ${wrfvar}'0'; then
      wrfkind=${wrfkindA}
    fi
    wrfkindA=`cat ${reqfile} | grep ${wrfvar} | grep \# | awk '{print $3}' | sort | uniq`
    wrfkind0=${wrfkindA}'0'
    Lwrfkind0=`expr length ${wrfkind0}`
    if test ${Lwrfkind0} -gt 1; then
      wrfkind=${wrfkindA}
    fi
  done

  Lwrfkind=`expr length ${wrfkind}`
  if test ${Lwrfkind} -gt 1; then
    echo ${wrfkind}
  else
    echo "wrf kind for variable '"${CFvar}"' not found!"
    exit
  fi
  
}
function check_execution() {
# Checking if the execution of a program has worked
  oexec=$1
  program=$2

  errmsg='ERROR -- error -- ERROR -- error'

  if test ! $oexec -eq 0; then
    echo ${errmsg}" "${oexec}
    echo "  execution failed!"
    echo "  "${program}
    exit
  fi
}

#######    #######
## MAIN
    #######

  rootsh=`pwd`
  errmsg='ERROR -- error -- ERROR -- error'
  
  foldfiles=$1
  opfhead=$2
  TOOLShome=$3
  opfreqs=$4
  opvars=$5
  opperiods=$6
  opgcms=$7
  reqfile=$8
  ops=$9

  optbnds=`echo ${ops} | tr ':' ' ' | awk '{print $1}'`
  opproj1=`echo ${ops} | tr ':' ' ' | awk '{print $2}'`
  opproj2=`echo ${ops} | tr ':' ' ' | awk '{print $3}'`

  if test ${opfreqs} = 'all'; then
    freqs=`allfreq_folder ${foldfiles} ${opfhead}`
  else
    freqs=`echo ${opfreqs} | tr ':' ' '`
  fi

  if test ${opperiods} = 'all'; then
    periods=`allpers_folder ${foldfiles} ${opfhead}`
  else
    periods=`echo ${opperiods} | tr ':' ' '`
  fi

  if test ${opvars} = 'all'; then
    vars=`allvars_folder ${foldfiles} ${opfhead}`
  else
    vars=`echo ${opvars} | tr ':' ' '`
  fi

  gcms=`echo ${opgcms} | tr ':' ' '`

  ofile=${rootsh}/correcting_folder.inf 
  echo $* > ${ofile}

  case ${HOSTNAME} in
    'ccrc468-17')
      echo "nothing to prepare we are in '"${HOSTNAME}"' "
    ;;
    *'.ccrc.unsw.edu.au')
      module load hdf5/1.8.8
      module load netcdf/4.1.3
      module load python/2.7.2
      module load nco/4.0.5
    ;;
    'smacsyc'*)
      . /usr/share/modules/init/bash
      module load deprecated
      module load szip/2.1
      module load hdf/1.8.7
      module load netcdf/4.1.3
      module load postgres/8.4.0
      module load atlas/3.9.67
      module load mpc/0.9
      module load gcc/4.6.2
      module load gdal/1.9.0
      module load lapack/3.4.0
      module load R/2.14.1
      module load python/2.7.2
      module load udunits/2.1.24
      module load nco/4.0.9
    ;;
    *)
      echo $HOSTNAME" not ready !!"
      exit
  esac

  ncodir=`which ncks`
  ncodir=`dirname ${ncodir}`

  mkdir -p ${foldfiles}/corrected

  for gcm in ${gcms}; do
    if test -f ${gcm}'_add-attr.inf'; then
      gaddattrfile=${gcm}'_add-attr.inf'
      Ngaddattrs=`wc -l ${gaddattrfile} | awk '{print $1}'`
    else
      Ngaddattrs=0
    fi
    if test -f ${gcm}'_rm-attr.inf'; then
      grmattrfile=${gcm}'_rm-attr.inf'
      Ngrmattrs=`wc -l ${grmattrfile} | awk '{print $1}'`
    else
      Ngrmattrs=0
    fi
    for freq in ${freqs}; do
      if test ${freq:0:2} = 'MO' || test ${freq:0:2} = 'DA'; then
        freqname=${freq:0:2}
        isStat=1
      else
        freqname=${freq}
        isStat=0
      fi
      for per in ${periods}; do
        gen_file=`head_in_file ${generic_file} ${freq}_${per}`
        if test -f ${freq}'_add-attr.inf'; then
          genvaddattrfile=${freq}'_add-attr.inf'
          Ngenvaddattrs=`wc -l ${genvaddattrfile} | awk '{print $1}'`
        else
          Ngenvaddattrs=0
        fi
        if test -f ${freq}'_rm-attr.inf'; then
          genvrmattrfile=${freq}'_rm-attr.inf'
          Ngenvrmattrs=`wc -l ${genvrmattrfile} | awk '{print $1}'`
        else
          Ngenvrmattrs=0
        fi
        for var in ${vars}; do
          if test ${var} = 'pracc'; then
            isStat=1
          fi 
          if test -f ${freq}'_'${var}'_add-attr.inf'; then
            vaddattrfile=${freq}'_'${var}'_add-attr.inf'
            Nvaraddattrs=`wc -l ${vaddattrfile} | awk '{print $1}'`
          else
            Nvaraddattrs=0
          fi
          if test -f ${freq}'_'${var}'_rm-attr.inf'; then
            vrmattrfile=${freq}'_'${var}'_rm-attr.inf'
            Nvarrmattrs=`wc -l ${vrmattrfile} | awk '{print $1}'`
          else
            Nvarrmattrs=0
          fi
          filen=${foldfiles}/${opfhead}_${freq}_${per}_${var}.nc
          if test -f ${filen}; then
            echo ${filen}
# Fixing time
## 
            python ${TOOLShome}/python/correct_xytime.py -n ${ncodir} -f ${filen}
            check_execution $? "$*"

# Additional
##
            python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v time -f       \
              ${filen} -o addvattr -S "_CoordinateAxisType|Time"
            check_execution $? "$*"

            if test ${optbnds} -eq 1 && test ${isStat} -eq 1; then
              wrfvkind=`kinds_CFvar ${var} ${reqfile}`
              namelistf=`cat ${reqfile} | grep NAMELISToutput | awk '{print $2}'`
              domain=`cat ${reqfile} | grep DOMAINsim | awk '{print $2}'`
              WRF4Ghome=`cat ${reqfile} | grep WRF4Ghome | awk '{print $2}'`
              cellTimeMethod=`CF_CORDEX_cellmethods ${filen} ${var} ${wrfvkind} ${per}          \
                ${namelistf} $domain $WRF4Ghome`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/adding_TimeBounds.py -f  \
                ${filen} -r 19491201000000 -q ${freqname} -s ${isStat} -v ${var} -c             \
                ${cellTimeMethod}
              check_execution $? "$*"
            fi
            if test ${opproj1} -eq 1; then
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/adding_mapping_information.py \
                -f ${filen} -m ${opproj2} -v ${var}
              check_execution $? "$*"
            fi
# removing global attributes
##
            iattr=1
            while test ${iattr} -le ${Ngrmattrs}; do
              gattr=`head -n ${iattr} ${grmattrfile} | tail -n 1 | tr '!' ' '`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v ${var} -f   \
                ${filen} -o rmgattr -S "${gattr}"
              check_execution $? "$*"
              iattr=`expr ${iattr} + 1`
            done # end of removing global attributes
# adding global attributes
##
            iattr=1
            while test ${iattr} -le ${Ngaddattrs}; do
              gattr=`head -n ${iattr} ${gaddattrfile} | tail -n 1 | tr '!' ' '`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v ${var} -f   \
                ${filen} -o addgattr -S "${gattr}"
              check_execution $? "$*"
              iattr=`expr ${iattr} + 1`
            done # end of adding global attributes
# removing variable attributes
##
            iattr=1
            while test ${iattr} -le ${Ngenvrmattrs}; do
              vattr=`head -n ${iattr} ${genvrmattrfile} | tail -n 1 | tr '!' ' '`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v ${var} -f   \
                ${filen} -o rmvattr -S "${vattr}"
              check_execution $? "$*"
              iattr=`expr ${iattr} + 1`
            done # end of removing variable attributes
# adding variable attributes
##
            iattr=1
            while test ${iattr} -le ${Ngenvaddattrs}; do
              vattr=`head -n ${iattr} ${genvaddattrfile} | tail -n 1 | tr '!' ' '`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v ${var} -f   \
                ${filen} -o addvattr -S "${vattr}"
              check_execution $? "$*"
              iattr=`expr ${iattr} + 1`
            done # end of adding variable attributes
# removing variable specific attributes
##
            iattr=1
            while test ${iattr} -le ${Nvarrmattrs}; do
              vattr=`head -n ${iattr} ${vrmattrfile} | tail -n 1 | tr '!' ' '`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v ${var} -f   \
                ${filen} -o rmvattr -S "${vattr}"
              check_execution $? "$*"
              iattr=`expr ${iattr} + 1`
            done # end of removing specific variable attributes
# adding variable specific attributes
##
            iattr=1
            while test ${iattr} -le ${Nvaraddattrs}; do
              vattr=`head -n ${iattr} ${vaddattrfile} | tail -n 1 | tr '!' ' '`
              python ${TOOLShome}/postprocess/GMS-UC/WRF4G/util/python/nc_var.py -v ${var} -f   \
               ${filen} -o addvattr -S "${vattr}"
              check_execution $? "$*"
              iattr=`expr ${iattr} + 1`
            done # end of adding specific variable attributes
##            exit
            mv ${filen} ${foldfiles}/corrected
          fi # end file existence
        done # end of periods
      done # end of vars
    done # end of freqs
  done # end of gcms

fi
