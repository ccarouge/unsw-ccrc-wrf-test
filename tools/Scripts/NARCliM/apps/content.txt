***************** content of Scripts/NARCliM/Apps
.BASH______________
change_names.bash: Shell to change period names in a folder
correcting_folder.bash: Shell to correct post-processed files in a 
  given folder
fix_timebnds.bash: Shell to fix 'time' and 'time_bnds' variables in
  post-processed files
