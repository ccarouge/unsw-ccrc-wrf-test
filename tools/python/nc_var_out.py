#!/usr/bin/python
# e.g. ccrc468-17 # ./nc_var_out.py -v time -i 1 -e -1 -f 123/CCRC_NARCliM_Sydney_All_1990-1999_pr10max.nc

from optparse import OptionParser
import numpy as np
from Scientific.IO.NetCDF import *

def rangedim(end):
    """Gives the instruction to retrieve values from a dimension of a variable

    >>> print rangedim(-1)
    
    """
    if end == -1:
      return 
    else:
      return end

### Options

parser = OptionParser()
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check", metavar="VAR")
parser.add_option("-i", "--initial_range", dest="irange", 
                  help="initial range to output", metavar="idim1,[idim2,...]")
parser.add_option("-e", "--ending_range", dest="erange", 
                  help="end range to output (-1 full size)", metavar="idim1,[idim2,...]")
parser.add_option("-f", "--netCDF_file", dest="ncfile", 
                  help="file to use", metavar="FILE")
(opts, args) = parser.parse_args()

####### ###### ##### #### ### ## #
errormsg='ERROR -- error -- ERROR -- error'

varn=opts.varname

print opts.irange
print opts.erange

inirange=opts.irange.split(',')
endrange=opts.erange.split(',')

irange = [int(val) for val in inirange]
erange = [int(val) for val in endrange]

if not len(irange) == len(erange):
  print errormsg
  print '  Different number of values in each range!'
  print '  initial range: ' + irange
  print '  ending range: ' + erange
  print errormsg
else:
  ndims=len(irange)

if not os.path.isfile(opts.ncfile):
  print errormsg
  print '  File ' + filename + ' does not exist !!'
  print errormsg
  quit()    

print '  opening file ' + opts.ncfile + ' ...'
ncf = NetCDFFile(opts.ncfile,'r')

if not ncf.variables.has_key(varn):
  print errormsg
  print '  File does not have variable ' + varn + ' !!!!'
  print errormsg
  quit()

var = ncf.variables[varn]
varVal = var.getValue()
varshape = var.shape
Nvarshape = len(varshape)

if not Nvarshape == ndims:
  print errormsg
  print '  Provided number of values of the range ' + ndims + ' is different of the shape of the variable ' + Nvarshape + ' !!!'
  print errormsg
  quit()

print irange
print erange
print 'Nvarshape: ', Nvarshape
print erange[0], rangedim(erange[0])
print erange[1], rangedim(erange[1])
print erange[2], rangedim(erange[2])
print varVal.shape
print '||||********'
print varVal[1:10,1,1]
print '*******'
print varVal[1,1:10,1]
print '****|||||***'
print varVal[1,1,1:10]
print '*******'
print varVal[1000, :, 64]

#print rangedim(erange[0])-irange[0], rangedim(erange[1])-irange[1], rangedim(erange[2])-irange[2]
if Nvarshape == 1:
  varValrange = varVal[irange[0]:rangedim(erange[0])]
  for i in range(len(varValrange)):
    print '%2s %f' % ( 'NC', varValrange[i] )
elif Nvarshape == 2:
  varValrange = varVal[irange[0]:rangedim(erange[0]), irange[1]:rangedim(erange[1])]
elif Nvarshape == 3:
  if erange[0] == -1:
    varValrange = varVal[:, irange[1]:rangedim(erange[1]), irange[2]:rangedim(erange[2])]  
  else:
    varValrange = varVal[irange[0]:rangedim(erange[0]), irange[1]:rangedim(erange[1]), irange[2]:rangedim(erange[2])]

  print ':', varValrange.shape
  print 'NC', varValrange

  for i in range(erange[0]-rangedim(irange[0]) + 1):
    print i
    for j in range(erange[1]-rangedim(irange[1]) + 1):
      print j
#      print '%2s %f' % ( 'NC', varValrange[:,j,i] )
#      print 'NC', varValrange[:,j,i]

elif Nvarshape == 4:
  varValrange = varVal[irange[0]:rangedim(erange[0]), irange[1]:rangedim(erange[1]), irange[2]:rangedim(erange[2]), 
    irange[3]:rangedim(erange[3])]
elif Nvarshape == 5:
  varValrange = varVal[irange[0]:rangedim(erange[0]), irange[1]:rangedim(erange[1]), irange[2]:rangedim(erange[2]), 
    irange[3]:rangedim(erange[3]), irange[4]:rangedim(erange[4])]
elif Nvarshape == 6:
  varValrange = varVal[irange[0]:rangedim(erange[0]), irange[1]:rangedim(erange[1]), irange[2]:rangedim(erange[2]), 
    irange[3]:rangedim(erange[3]), irange[4]:rangedim(erange[4]), irange[5]:rangedim(erange[5])]

ncf.close()
