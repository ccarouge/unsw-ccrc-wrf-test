## e.g. # python create_grid_mapping_char.py -f CCRC_NARCliM_Sydney_MOM_1990-2009_wssmean.nc -v Lambert_Conformal -n /usr/bin
## e.g. # python create_grid_mapping_char.py -f CCRC_NARCliM_Sydney_MOM_1990-2009_wssmean.nc -v Lambert_Conformal -n /share/apps/nco/4.0.5/bin

from optparse import OptionParser
import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
import re
import subprocess as sub

def searchInlist(listname, nameFind):
    for x in listname:
      if x == nameFind:
        return True
    return False

def set_attribute(ncvar, attrname, attrvalue):
    """ Sets a value of an attribute of a netCDF variable
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    attvar = ncvar.ncattrs()
    if searchInlist(attvar, attrname):
        attr = ncvar.delncattr(attrname)

    attr = ncvar.setncattr(attrname, attrvalue)

    return ncvar

def fvaradd(values, ncfile):
# Adding variable from another file 
  errormsg='ERROR -- error -- ERROR -- error'

  refnc = values.split(':')[0]
  refvar = values.split(':')[1]

  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  if not os.path.isfile(refnc):
    print errormsg
    print '  Reference file ' + refnc + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  refvars = ncref.variables
  if searchInlist(refvars, refvar):
      refvarv = ncref.variables[refvar]
  else:
      print '  ' + refnc + ' does not have variable: ' + refvar
      quit()

  vardims = refvarv.dimensions
  vartype = refvarv.dtype
  varattr = refvarv.ncattrs()

# Checking dimensions
##
  newdims = ncf.dimensions
  for rdim in vardims:
      if not searchInlist(newdims, rdim):
          print '  Adding dimension ' + rdim
          ncf.close()
          ncref.close()
          fdimadd(refnc + ':' + rdim, ncfile)
          ncf = NetCDFFile(ncfile,'a')
          ncref = NetCDFFile(refnc,'r')

# Checking fill value
## 
  if searchInlist(varattr, '_FillValue'):
      varfil = refvar._FillValue
  else:
      varfil = False

  refvarvals = refvarv[:]

  print '  Adding refvar:', refvar, 'shape: ', refvarv.shape
  var = ncf.createVariable(refvar, vartype, vardims, fill_value=varfil)
  if not len(refvarv.shape) == 0:
    var[:] = refvarv  

  newvar = ncf.variables[refvar]
  for attr in varattr:
      attrv = refvarv.getncattr(attr)
      newvar.setncattr(attr, attrv)

  ncf.sync()
  ncf.close()
  ncref.close()

def fdimadd(values, ncfile):
# Adding dimension from another file 
  errormsg='ERROR -- error -- ERROR -- error'

  refnc = values.split(':')[0]
  refdim = values.split(':')[1]

  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  if not os.path.isfile(refnc):
    print errormsg
    print '  Reference file ' + refnc + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  refdims = ncref.dimensions
  if searchInlist(refdims, refdim):
      refdimv = ncref.dimensions[refdim]
  else:
      print '  ' + refnc + ' does not have dimension: ' + refdim
      quit()

  if refdimv.isunlimited():
      print '    Unlimited dimension '
      dimsize = None
  else:
      dimsize = len(refdimv)

  print '  Adding refdim:', refdim, 'size:', dimsize
  dim = ncf.createDimension(refdim, dimsize)
  
  ncf.sync()
  ncf.close()
  ncref.close()

def fattradd(var, values, ncfile):
# Adding attributes from another file
  errormsg='ERROR -- error -- ERROR -- error'

  refnc = values.split(':')[0]
  refvar = values.split(':')[1]

  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  if not os.path.isfile(refnc):
    print errormsg
    print '  Reference file ' + refnc + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  vars = ncf.variables
  if searchInlist(vars, var):
      varv = ncf.variables[var]
  else:
      print '  ' + ncf + ' does not have variable: ' + var
      quit()

  refvars = ncref.variables
  if searchInlist(refvars, refvar):
      refvarv = ncref.variables[refvar]
  else:
      print '  ' + refnc + ' does not have variable: ' + refvar
      quit()

  refvarattrs = refvarv.ncattrs()
  Nattrs = len(refvarattrs)
  print '  Adding ', Nattrs,' atributes from:', refvar

  for attr in refvarattrs:
      attrv = refvarv.getncattr(attr)
      atvar = set_attribute(varv, attr, attrv)
  
  ncf.sync()
  ncf.close()
  ncref.close()

def fgaddattr(values, ncfile):
# Adding global attributes from another file
  errormsg='ERROR -- error -- ERROR -- error'

  refnc = values

  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  if not os.path.isfile(refnc):
    print errormsg
    print '  Reference file ' + refnc + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')
  ncref = NetCDFFile(refnc,'r')

  refgattrs = ncref.ncattrs()
  Nattrs = len(refgattrs)
  print '  Adding ', Nattrs,' global atributes'

  for attr in refgattrs:
      attrv = ncref.getncattr(attr)
      atvar = set_attribute(ncf, attr, attrv)
  
  ncf.sync()
  ncf.close()
  ncref.close()

def varrm(ncfile, var):
# Removing a variable from a file
  import shutil as shu
  errormsg='ERROR -- error -- ERROR -- error'

  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')
  ncvars = ncf.variables
  ncf.close()

  if not searchInlist(ncvars, var):
      print '  ' + ncf + ' does not have variable: ' + var
      quit()

  tmpncf = NetCDFFile('tmp.nc' , 'w')
  gtmpattr = set_attribute(tmpncf, 'copy', 'temporal')
  tmpncf.sync()
  tmpncf.close()

  for varn in ncvars:
      if not varn == var:
           fvaradd(ncfile + ':' + varn, 'tmp.nc')

  fgaddattr(ncfile, 'tmp.nc')
  shu.copyfile('tmp.nc', ncfile)
  os.remove('tmp.nc')


parser = OptionParser()
parser.add_option("-f", "--netCDF_file", dest="ncfile", 
                  help="file to use", metavar="FILE")
parser.add_option("-j", "--projection_file", dest="projfile", 
                  help="ASCII file with the information of the projectioninformation", metavar="FILE")
parser.add_option("-n", "--NCOinsFolder", dest="ncoins", 
                  help="folder with the instalation of NCO", metavar="VARNAME")
parser.add_option("-v", "--VarName", dest="varname", 
                  help="variable with projection", metavar="VARNAME")

(opts, args) = parser.parse_args()

#######    #######
## MAIN
    #######

####### ###### ##### #### ### ## #
errmsg='ERROR -- error -- ERROR -- error'
varn=opts.varname

if not os.path.isfile(opts.ncfile):
  print errmsg
  print '  File ' + opts.ncfile + ' does not exist !!'
  print errmsg
  quit()    

ncf = NetCDFFile(opts.ncfile,'a')
if not ncf.variables.has_key(varn):
    print errmsg
    print '  File does not have the varible ' + varn + ' !!!'
    print '     creating the variable'

    newproj = ncf.createVariable(varn, 'c')
    pjf = open( opts.projfile, 'r')
    for fileline in pjf:
        line = fileline.replace('\n','')
        if len(line) > 1:
            attrn = line.split(' ')[0]
            attrval = line.split(' ')[1].split('@')
        
            newattr = newproj.__setattr__(attrn, ' '.join(attrval))

    pjf.close()
    ncf.sync()
    ncf.close()


else:
    varproj = ncf.variables[varn]
    projattrs = varproj.ncattrs()

    ncvars = ncf.variables
    if searchInlist(ncvars, varn + 'new'):
        print '  Removing existing temporal variable ' + varn + 'new'
        ncf.close()
        varrm(opts.ncfile, varn + 'new')
        ncf = NetCDFFile(opts.ncfile,'a')

    newproj = ncf.createVariable(varn + 'new', 'c')
#
#   Attributes
    for iattr in range(len(projattrs)):
        attrn = projattrs[iattr]
        attrval = varproj.getncattr(attrn)

        newattr = newproj.__setattr__(attrn, attrval)

#   Removing all projected variable
##
    ncf.sync()
    ncf.close()

    varrm(opts.ncfile, varn)
##    ins = opts.ncoins + '/ncks -x -v ' + varn + ' ' + opts.ncfile + ' new.nc'


##    try:
##        syscode = sub.call(ins, shell=True)

##        if not syscode == 0:
##            print errmsg
##            print "  Execution of ncks failed!"
##            print ins
##            print syscode
##            quit()

##    except OSError, e:
##        print errmsg
##        print "   Execution of ncks failed!"
##        print e
##        quit()

##    ins = 'mv new.nc ' + opts.ncfile
##    sub.call(ins, shell=True)

    ncf = NetCDFFile(opts.ncfile,'a')
    var = ncf.renameVariable(varn + 'new', varn)

    ncf.sync()
    ncf.close()

