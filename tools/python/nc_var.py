#!/usr/bin/python
# e.g. ccrc468-17 # ./nc_var.py -v time -f 123/CCRC_NARCliM_Sydney_All_1990-1999_pr10max.nc -o out -S 1:-1
# e.g. ccrc468-17 # ./nc_var.py -v prac -f xyz/CCRC_NARCliM_Sydney_DAM_1990-1999_prac.nc -o mname -S pluja
# e.g. ccrc468-17 # ./nc_var.py -v lluis -f CCRC_NARCliM_Sydney_MOM_1990-1999_prac.nc -o addref -S 'prac:standard_name@lluis_variable:long_name@test variable lluis:units@m s-1:0.'
# e.g. ccrc468-17 # ./nc_var.py -v lluis66 -f ~/UNSW-CCRC-WRF/tools/python/CCRC_NARCliM_Sydney_MOM_1990-1999_prac.nc -o addattr -S 'comment|Lluis Fita-123456'
# e.g. ccrc468-17 # ./nc_var.py -v lluis66 -f ~/UNSW-CCRC-WRF/tools/python/CCRC_NARCliM_Sydney_MOM_1990-1999_prac.nc -o rmvattr -S 'comment'

from optparse import OptionParser
import numpy as np
from netCDF4 import Dataset as NetCDFFile
import os
import re

def valmod(values, ncfile, varn):
    """ Function to modify the value of a variable

    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile
    errormsg='ERROR -- error -- ERROR -- error'
    vals = values.split(',')
    modins = vals[0]
    modval = float(vals[1])

    if not os.path.isfile(ncfile):
      print errormsg
      print '  File ' + ncfile + ' does not exist !!'
      print errormsg
      quit()    

    ncf = NetCDFFile(ncfile,'a')

    if ncf.dimensions.has_key('plev'):
      # removing pressure level from variable name
      varn = re.sub("\d+", "", varn) 

    if not ncf.variables.has_key(varn):
      print errormsg
      print '  File does not have variable ' + varn + ' !!!!'
      print errormsg
      ncf.close()
      quit()

    var = ncf.variables[varn]
    varshape = var.shape
    Ndims = len(varshape)
    
    varshapevals = list(varshape)
    varVal = var[:]

    if modins == 'sumc':
      varVal[:] = varVal[:] + modval
    elif modins == 'subc':
      varVal[:] = varVal[:] - modval
    elif modins == 'mulc':
      varVal[:] = varVal[:] * modval
    elif modins == 'divc':
      varVal[:] = varVal[:] / modval
    else: 
      print errormsg
      print '  Operation to modify values ' + modins + ' is not defined !!!'
      print errormsg
      quit()

    var[:] = varVal
    ncf.sync()
    ncf.close()

def rangedim(end, shape):
    """Gives the instruction to retrieve values from a dimension of a variable

    >>> print rangedim(-1, 15)
    15
    """
    if end == -1:
      return shape
    else:
      return end

def varaddref(values, ncfile, varn):
# Function to add a variable in an existing file copying characteristics from an existing one
  errormsg='ERROR -- error -- ERROR -- error'

  varvalues = values.split(':')

  Nvarvalues = len(varvalues)
  varprev = varvalues[0]
  newattrs = {}
  for iattr in range(Nvarvalues - 2):
    attrv = varvalues[iattr+1]
    newattrs[attrv.split('@')[0]] = attrv.split('@')[1]

  ncf = NetCDFFile(ncfile,'a')

  if ncf.variables.has_key(varn):
    print errormsg
    print '  File already has the varible ' + varn + ' !!!'
    print errormsg
    ncf.close()
    quit()

  if not ncf.variables.has_key(varprev):
    print errormsg
    print '  File does not have variable ' + varprev + ' !!!!'
    print errormsg
    ncf.close()
    quit()

  varp = ncf.variables[varprev]
  varpVal = varp[:]
  varpshape = varp.shape
  Nvarpshape = len(varpshape)
  varpattrs = varp.ncattrs()
  varptype = varp.dtype
  varpdims = varp.dimensions
  varpndims = varp.ndim

  print '  shape of the variable used as reference: ',varpshape
#
# variable characteristics 
  if len(varpshape) == 4:
    dimpt = varpshape[0]
    dimpz = varpshape[1]
    dimpy = varpshape[2]
    dimpx = varpshape[3]
  elif len(varpshape) == 3:
    dimpt = varpshape[0]
    dimpy = varpshape[1]
    dimpx = varpshape[2]
  elif len(varpshape) == 3:
    dimpy = varpshape[0]
    dimpx = varpshape[1]

  newvar = ncf.createVariable(varn, varptype, dimensions=varpdims)
  newvar = ncf.variables[varn]
#
# Values
  varplen=1
  for idim in range(varpndims):
    varplen=varplen*varpshape[idim]

  if varptype == 'float' or varptype == 'float32' or varptype == 'float64':
    newvals = np.reshape(np.arange(varplen), varpshape)*1.
  else:
    newvals = np.reshape(np.arange(varplen), varpshape)

  if not os.path.isfile(varvalues[Nvarvalues-1]):
    print '  Using constant value'
    newvals[:] = float(varvalues[Nvarvalues-1])
  else:
    print '  Using 2-D values from ' + varvalues[Nvarvalues-1]
    asciif = open(varvalues[Nvarvalues-1], 'r')

    fdimx = len(asciif.readlines())
    asciif.seek(0)

    if len(varpshape) == 4:
      if not fdimx == dimpx:
        print errormsg
        print '  Provided file has dimx=', fdimx, ' and variable has dimx=', dimpx, ' !!!'
        print errormsg
        quit()

      iline = 0
      idx = 0
      for fline in asciif:
        line = fline.replace('\n','')
        yvals = []
        for iyval in line.split('     '):
          yvals.append(float(iyval))

        if iline == 0:
          fdimy = len(yvals)
          if not fdimy == dimpy:
            print errormsg
            print '  Provided file has dimy=', fdimy, ' and variable has dimy= ', dimpy, ' !!!'
            print errormsg
            quit()
        for it in range(dimpt):
          for iz in range(dimpz):
            newvals[it,iz,:,idx] = yvals
  
        idx = idx+1
        iline = iline+1

    elif len(varpshape) == 3:
      if not fdimx == dimpx:
        print errormsg
        print '  Provided file has dimx=', fdimx, ' and variable has dimx=', dimpx, ' !!!'
        print errormsg
        quit()

      iline = 0
      idx = 0
      for fline in asciif:
        line = fline.replace('\n','')
        yvals = []
        for iyval in line.split('     '):
          yvals.append(float(iyval))

        if iline == 0:
          fdimy = len(yvals)
          if not fdimy == dimpy:
            print errormsg
            print '  Provided file has dimy=', fdimy, ' and variable has dimy= ',dimpy, ' !!!'
            print errormsg
            quit()
        for it in range(dimpt):
          newvals[it,:,idx] = yvals

        idx = idx+1
        iline = iline+1
    elif len(varpshape) == 2:
      if not fdimx == dimpx:
        print errormsg
        print '  Provided file has dimx=', fdimx, ' and variable has dimx=', dimpx, ' !!!'
        print errormsg
        quit()

      iline = 0
      idx = 0
      for fline in asciif:
        line = fline.replace('\n','')
        yvals = []
        for iyval in line.split('     '):
          yvals.append(float(iyval))

        if iline == 0:
          fdimy = len(yvals)
          if not fdimy == dimpy:
            print errormsg
            print '  Provided file has dimy=', fdimy, ' and variable has dimy= ',dimpy, ' !!!'
            print errormsg
            quit()

        newvals[:,idx] = yvals
        idx = idx+1
        iline = iline+1

  newvar[:] = newvals

#
# Attributes
  for iattr in range(len(varpattrs)):
    attrn = varpattrs[iattr]
    attrval = varp.getncattr(attrn)

    if attrn in newattrs:
      newattr = newvar.setncattr(attrn, newattrs[attrn])
    else:
      newattr = newvar.setncattr(attrn, attrval)

  ncf.sync()
  ncf.close()

def varout(values, ncfile, varn):
# Function when we want to output variable values
  import numpy as np
  from netCDF4 import Dataset as NetCDFFile

  errormsg='ERROR -- error -- ERROR -- error'

  optsIrange = values.split(':')[0]
  optsErange = values.split(':')[1]

  print optsIrange
  print optsErange

  inirange=optsIrange.split(',')
  endrange=optsErange.split(',')

  irange = [int(val) for val in inirange]
  erange = [int(val) for val in endrange]

  if not len(irange) == len(erange):
    print errormsg
    print '  Different number of values in each range!'
    print '  initial range: ' + irange
    print '  ending range: ' + erange
    print errormsg
    quit()
  else:
    ndims=len(irange)
 
  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'r')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '  File does not have variable ' + varn + ' !!!!'
    print errormsg
    ncf.close()
    quit()

  var = ncf.variables[varn]
  varVal = var[:]
  varshape = var.shape
  Nvarshape = len(varshape)
 
  if not Nvarshape == ndims:
    print errormsg
    print '  Provided number of values of the range ' + ndims + ' is different of the shape of the variable ' + Nvarshape + ' !!!'
    print errormsg
    ncf.close()
    quit()

  if Nvarshape == 1:
    varValrange = varVal[irange[0]:rangedim(erange[0], varshape[0])]
  elif Nvarshape == 2:
    varValrange = varVal[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1]), varshape[1]]
  elif Nvarshape == 3:
    varValrange = varVal[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2])]
  elif Nvarshape == 4:
    varValrange = varVal[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2]),      \
      irange[3]:rangedim(erange[3], varshape[3])]
  elif Nvarshape == 5:
    varValrange = varVal[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2]),      \
      irange[3]:rangedim(erange[3], varshape[3]), irange[4]:rangedim(erange[4], varshape[4])]
  elif Nvarshape == 6:
    varValrange = varVal[irange[0]:rangedim(erange[0], varshape[0]),                               \
      irange[1]:rangedim(erange[1], varshape[1]), irange[2]:rangedim(erange[2], varshape[2]),      \
      irange[3]:rangedim(erange[3], varshape[3]), irange[4]:rangedim(erange[4], varshape[4]),      \
      irange[5]:rangedim(erange[5], varshape[5])]

  ncf.close()

  Nshaperange = len(varValrange.shape)
  Noneshape = 0
  for ir in range(Nshaperange):
    if varValrange.shape[ir] == 1:
      Noneshape = Noneshape + 1

  if Noneshape == Nshaperange - 1:
    for i in range(len(varValrange)):
      print '%2s %f' % ( 'NC', varValrange[i] )

def chvarname(values, ncfile, varn):
# Changing the name of the variable
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '  File does not have variable ' + varn + ' !!!!'
    print errormsg
    ncf.close()
    quit()

  newname = ncf.renameVariable(varn, values)

  ncf.sync()
  ncf.close()

def searchInlist(listname, nameFind):
    for x in listname:
      if x == nameFind:
        return True
    return False

def set_attribute(ncvar, attrname, attrvalue):
    """ Sets a value of an attribute of a netCDF variable
    """
    import numpy as np
    from netCDF4 import Dataset as NetCDFFile

    attvar = ncvar.ncattrs()
    if searchInlist(attvar, attrname):
        attr = ncvar.delncattr(attrname)

    attr = ncvar.setncattr(attrname, attrvalue)

    return ncvar

def gaddattr(values, ncfile):
# Add a global attribute to a netCDF
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')

  attrvals=values.split('|')
  attrn=attrvals[0]
  attrv=attrvals[1]

  ncf = set_attribute(ncf, attrn, attrv)

  ncf.sync()
  ncf.close()

def varaddattr(values, ncfile, varn):
# Add an attribute to a variable
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '  File does not have variable ' + varn + ' !!!!'
    print errormsg
    ncf.close()
    quit()

  attrvals=values.split('|')
  attrn=attrvals[0]
  attrv=attrvals[1]

  var = ncf.variables[varn]
  var = set_attribute(var, attrn, attrv)

  ncf.sync()
  ncf.close()

def varrmattr(values, ncfile, varn):
# Removing an attribute from a variable
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')

  if ncf.dimensions.has_key('plev'):
    # removing pressure level from variable name
    varn = re.sub("\d+", "", varn) 

  if not ncf.variables.has_key(varn):
    print errormsg
    print '  File does not have variable ' + varn + ' !!!!'
    print errormsg
    ncf.close()
    quit()

  var = ncf.variables[varn]

  attvar = var.ncattrs()
  if searchInlist(attvar, values):
      attr = var.delncattr(values)
  else:
      print '  ' + varn + ' does not have attribute: ' + values

  ncf.sync()
  ncf.close()

def grmattr(values, ncfile):
# Removing a global attribute
  errormsg='ERROR -- error -- ERROR -- error'
  if not os.path.isfile(ncfile):
    print errormsg
    print '  File ' + ncfile + ' does not exist !!'
    print errormsg
    quit()    

  ncf = NetCDFFile(ncfile,'a')

  attvar = ncf.ncattrs()
  if searchInlist(attvar, values):
      attr = ncf.delncattr(values)
  else:
      print '  ' + ncfile + ' does not have attribute: ' + values

  ncf.sync()
  ncf.close()

### Options
##string_operation="operation to make: " + '\n' + " out, output values -S inidim1,[inidim2,...]:enddim1,[enddim2,...]"
string_operation="""operation to make: 
  addgattr, add a new global attribute: addatr -S [attrname]|[attrvalue]
  addvattr, add a new attribute to any given variable: addatr -S [attrname]|[attrvalue]
  addref, add a new variable with dimension and attributes from an already existing 'variable ref' in the file and -S [variable ref]:[attr name]@[value]:[[attr2]@[value2], ...]:[value/file with values]  mname, modify name -S newname
  mname, modify name -S newname
  out, output values -S inidim1,[inidim2,...]:enddim1,[enddim2,...]
  valmod, modifiy values of variable -S [modification]:
     sumc,[constant]: add [constant] to variables values
     subc,[constant]: substract [constant] to variables values
     mulc,[constant]: multipy by [constant] to variables values
     divc,[constant]: divide by [constant] to variables values
  rmgattr, remove a global attribute: rmgattr -S [attrname]
  rmvattr, remove an attribute to any given variable: rmvattr -S [attrname]
"""

#print string_operation

parser = OptionParser()
parser.add_option("-f", "--netCDF_file", dest="ncfile", 
                  help="file to use", metavar="FILE")
parser.add_option("-o", "--operation", type='choice', dest="operation", choices=['addgattr', 'addvattr', 'addref', 'mname', 'out', 'rmvattr', 'rmgattr', 'valmod'], 
                  help="operation to make: addgattr, addvattr, addref, mname, out, rmgattr, rmvattr, valmod", metavar="OPER")
parser.add_option("-S", "--valueS (when applicable)", dest="values", 
                  help="values to use according to the operation", metavar="VALUES")
parser.add_option("-v", "--variable", dest="varname",
                  help="variable to check", metavar="VAR")

(opts, args) = parser.parse_args()

#if opts.help:
#  parser.print_help()
#  print string_operation
#  sys.exit()

#######    #######
## MAIN
    #######

####### ###### ##### #### ### ## #
errormsg='ERROR -- error -- ERROR -- error'

varn=opts.varname
oper=opts.operation

if not os.path.isfile(opts.ncfile):
  print errormsg
  print '  File ' + opts.ncfile + ' does not exist !!'
  print errormsg
  quit()    

if oper == 'addgattr':
  gaddattr(opts.values, opts.ncfile)
elif oper == 'addvattr':
  varaddattr(opts.values, opts.ncfile, opts.varname)
elif oper == 'addref':
  varaddref(opts.values, opts.ncfile, opts.varname)
elif oper == 'mname':
  chvarname(opts.values, opts.ncfile, opts.varname)
elif oper == 'rmgattr':
  grmattr(opts.values, opts.ncfile)
elif oper == 'rmvattr':
  varrmattr(opts.values, opts.ncfile, opts.varname)
elif oper == 'valmod':
  valmod(opts.values, opts.ncfile, opts.varname)
elif oper == 'out':
  varout(opts.values, opts.ncfile, opts.varname)
else:
  print errormsg
  print '   The operation ' + oper + ' is not ready !!'
  print errormsg
  quit()
