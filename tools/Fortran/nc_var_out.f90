PROGRAM nc_var_out
! Program to retrieve values of a variable
! Nov. 2011
! Lluis Fita. Universidad de Cantabria,  Grupo de Meteorologia de Santander, Santander, Spain

! WRF4G by Santander Meteorology Group is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. 
! Permissions beyond the scope of this license may be available at http://www.meteo.unican.es/software/wrf4g.

! trueno
! export NETCDF_ROOT='/home/lluis/bin/netcdf-4.0.1'
! gfortran nc_var_out.f90 -L${NETCDF_ROOT}/lib -lnetcdf -lm -I${NETCDF_ROOT}/include -o nc_var_out

! ui01
! export NETCDF_ROOT='/software/CentOS/5.2/netcdf/4.1.3/gcc_4.1.2_NOpnetcdf'
! gfortran nc_var_out.f90 -g -O2 -L${NETCDF_ROOT}/lib -lnetcdf -lnetcdff -lm -I${NETCDF_ROOT}/include -o nc_var_out

!! ccrc468-17: gfortran nc_var_out.f90 -L/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/lib -lnetcdf -lnetcdff -lm -I/home/lluis/bin/gcc_netcdf-4.1.3_NOpnetcdf/include -o nc_var_out

  USE netcdf

  IMPLICIT NONE

  INCLUDE 'netcdf.inc'
  
! Arguments
  INTEGER                                                :: iarg, narg
  CHARACTER(LEN=500), ALLOCATABLE, DIMENSION(:)          :: arguments
  CHARACTER(LEN=500)                                     :: filen, varname

! netcdf
  INTEGER                                                :: idim, rcode, ncid, ivar
  INTEGER, DIMENSION(6)                                  :: DIMshape, Vdimsid, startvals, countvals
  INTEGER                                                :: Vid, Vtype, Vndims, Vnatts
  CHARACTER(LEN=100), DIMENSION(6)                       :: DIMname
  INTEGER, ALLOCATABLE, DIMENSION(:,:,:,:,:,:)           :: Ivalues
  REAL, ALLOCATABLE, DIMENSION(:,:,:,:,:,:)              :: Rvalues
  DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:,:,:,:,:)  :: Dvalues
  CHARACTER(LEN=1), ALLOCATABLE, DIMENSION(:,:,:,:,:,:)  :: Cvalues
  LOGICAL, ALLOCATABLE, DIMENSION(:,:,:,:,:,:)           :: Lvalues
 
!!!!!!! Variables
! argument(1): file name
! argument(2): variable name
! DIMshape: shape of dimensions
! Vdimsid: Id of the dimensions of the variable
! Vid: Id of the variable within the file
! Vtype: variable type
! Vndims: number of dimensions of the variable
! Vnatts: number of attributes of the variable
! [I/R/D/S/L]values: [Integer/Real/Double Precission/String/Logial] values of 
!   the variable
! startvals: index of each dimension to start to obtain values
! countvals: number of values to get from each dimension (-1; all values)

!!!!!!! Subroutines/Functions
! [I/R/D/C]print: prints on standard output content of 
!   [Integer/Real/Double Precission/Character] variable 

! Arguments
  narg=COMMAND_ARGUMENT_COUNT()
  IF (ALLOCATED(arguments)) DEALLOCATE(arguments)
  ALLOCATE(arguments(narg))

  DO iarg=1, narg
    CALL GETARG(iarg,arguments(iarg))
  END DO
  IF (TRIM(arguments(1)) == '-h' ) THEN
    PRINT *,"nc_var_out [ncf](netCDF file) [var](variable name) [s1, s2, ..., s6](starting "      &
      //" index of each dimension) [n1, n2, ..., n6](number of values; -1, all values)"
    STOP
  END IF

  filen=TRIM(arguments(1))
  varname=TRIM(arguments(2))
  
  startvals=1
  countvals=-1
  if (narg > 2) THEN
    IF (MOD(narg,2) /= 0.) THEN
      PRINT *,'Incorrect number of starting indexes ",arguments(3:(narg-2)/2),                    &
        " and number of values',arguments(3+(narg-2)/2+1:narg)
    ELSE
      DO iarg=3,2+(narg-2)/2
        READ(arguments(iarg), 5)startvals(iarg-2)
      END DO
      DO iarg=3+(narg-2)/2,narg
        READ(arguments(iarg), 5)countvals(iarg-2-(narg-2)/2)
      END DO
    END IF
  
  END IF

  DEALLOCATE(arguments)
 
  rcode = nf90_open(filen, 0, ncid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)

  rcode = nf90_inq_varid(ncid, varname, Vid)
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
  rcode = nf90_inquire_variable(ncid, varid=Vid, xtype=Vtype, ndims=Vndims, dimids=Vdimsid,       &
     nAtts=Vnatts)    
  IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
!  PRINT *,'var id;',Vid,'name; ',TRIM(varname),' type;',Vtype,' n. of dimensions; ', Vndims,      &
!    ' var shape; ',(Vdimsid(idim),idim=1,Vndims), ' n. of attributes; ', Vnatts

!  PRINT *,'Dimensions; ___ __ _'
  DIMname=''
  DIMshape=0
  DO idim=1, Vndims
    rcode = nf90_inquire_dimension(ncid, Vdimsid(idim), DIMname(idim), DIMshape(idim))
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
!    PRINT *,'   ',idim, Vdimsid(idim), TRIM(DIMname(idim)),'; ', DIMshape(idim)
    IF (countvals(idim) == -1) THEN
      IF (startvals(idim) /= 1) THEN
        countvals(idim)=DIMshape(idim) - startvals(idim) + 1
      ELSE
        countvals(idim)=DIMshape(idim)
      END IF
    END IF
  END DO
  startvals(Vndims+1:6)=1
  countvals(Vndims+1:6)=1
  
!
! Integer values (adding Vtype =6 for CDO outputting)
  vartype: IF (Vtype == NF90_INT) THEN
    IF (ALLOCATED(Ivalues)) DEALLOCATE(Ivalues)
    IF (Vndims==6) THEN
      ALLOCATE(Ivalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),DIMshape(6)))
    ELSE IF (Vndims==5) THEN
      ALLOCATE(Ivalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),1))
    ELSE IF (Vndims==4) THEN
      ALLOCATE(Ivalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),1,1))
    ELSE IF (Vndims==3) THEN
      ALLOCATE(Ivalues(DIMshape(1),DIMshape(2),DIMshape(3),1,1,1))
    ELSE IF (Vndims==2) THEN
      ALLOCATE(Ivalues(DIMshape(1),DIMshape(2),1,1,1,1))
    ELSE IF (Vndims==1) THEN
      ALLOCATE(Ivalues(DIMshape(1),1,1,1,1,1))
    END IF
    rcode = nf90_get_var(ncid, Vid, Ivalues, startvals, countvals)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    CALL Iprint(Ivalues, Vndims, startvals, countvals, DIMname)
!
! Character values
  ELSE IF (Vtype == NF90_CHAR) THEN
    IF (ALLOCATED(Cvalues)) DEALLOCATE(Cvalues)
    IF (Vndims==6) THEN
      ALLOCATE(Cvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),DIMshape(6)))
    ELSE IF (Vndims==5) THEN
      ALLOCATE(Cvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),1))
    ELSE IF (Vndims==4) THEN
      ALLOCATE(Cvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),1,1))
    ELSE IF (Vndims==3) THEN
      ALLOCATE(Cvalues(DIMshape(1),DIMshape(2),DIMshape(3),1,1,1))
    ELSE IF (Vndims==2) THEN
      ALLOCATE(Cvalues(DIMshape(1),DIMshape(2),1,1,1,1))
    ELSE IF (Vndims==1) THEN
      ALLOCATE(Cvalues(DIMshape(1),1,1,1,1,1))
    END IF
    rcode = nf90_get_var(ncid, Vid, Cvalues, startvals, countvals)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    CALL Cprint(Cvalues, Vndims, startvals, countvals, DIMname)
!
! Real values
  ELSE IF (Vtype == NF90_FLOAT) THEN
    IF (ALLOCATED(Rvalues)) DEALLOCATE(Rvalues)
    IF (Vndims==6) THEN
      ALLOCATE(Rvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),DIMshape(6)))
    ELSE IF (Vndims==5) THEN
      ALLOCATE(Rvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),1))
    ELSE IF (Vndims==4) THEN
      ALLOCATE(Rvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),1,1))
    ELSE IF (Vndims==3) THEN
      ALLOCATE(Rvalues(DIMshape(1),DIMshape(2),DIMshape(3),1,1,1))
    ELSE IF (Vndims==2) THEN
      ALLOCATE(Rvalues(DIMshape(1),DIMshape(2),1,1,1,1))
    ELSE IF (Vndims==1) THEN
      ALLOCATE(Rvalues(DIMshape(1),1,1,1,1,1))
    END IF
    rcode = nf90_get_var(ncid, Vid, Rvalues, startvals, countvals)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    CALL Rprint(Rvalues, Vndims, startvals, countvals, DIMname)
!
! Double precission values
  ELSE IF (Vtype == NF90_DOUBLE) THEN
    IF (ALLOCATED(Dvalues)) DEALLOCATE(Dvalues)
    IF (Vndims==6) THEN
      ALLOCATE(Dvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),DIMshape(6)))
    ELSE IF (Vndims==5) THEN
      ALLOCATE(Dvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),DIMshape(5),1))
    ELSE IF (Vndims==4) THEN
      ALLOCATE(Dvalues(DIMshape(1),DIMshape(2),DIMshape(3),DIMshape(4),1,1))
    ELSE IF (Vndims==3) THEN
      ALLOCATE(Dvalues(DIMshape(1),DIMshape(2),DIMshape(3),1,1,1))
    ELSE IF (Vndims==2) THEN
      ALLOCATE(Dvalues(DIMshape(1),DIMshape(2),1,1,1,1))
    ELSE IF (Vndims==1) THEN
      ALLOCATE(Dvalues(DIMshape(1),1,1,1,1,1))
    END IF
    rcode = nf90_get_var(ncid, Vid, Dvalues, startvals, countvals)
    IF (rcode /= nf90_NoErr) PRINT *,nf90_strerror(rcode)
    CALL Dprint(Dvalues, Vndims, startvals, countvals, DIMname)

  ELSE   
    PRINT *,'Variable type; ',Vtype,' not suppported'
    STOP
  END IF vartype

  rcode = nf90_close(ncid)
 5 FORMAT(I4)

END PROGRAM nc_var_out

SUBROUTINE Cprint(vals, ndims, svals, cvals, dnames)
! Subroutine to print integer values

  IMPLICIT NONE
  
  INTEGER                                                 :: i,j,k,l,m,n
  INTEGER, INTENT(IN)                                     :: ndims
  INTEGER, DIMENSION(6), INTENT(IN)                       :: svals, cvals
  CHARACTER(LEN=1), DIMENSION(cvals(1),cvals(2),cvals(3),                                          &
    cvals(4), cvals(5),cvals(6)), INTENT(IN)              :: vals
  CHARACTER(LEN=100), DIMENSION(6), INTENT(IN)            :: dnames
  
!  PRINT *,'Desired values;___ __ _'
!  DO i=1,ndims
!    PRINT *,'o ',TRIM(dnames(i)),';',svals(i),char(44),svals(i)+cvals(i)
!  END DO
  SELECT CASE (ndims)
    CASE(6)
      DO n=1,cvals(6)
        DO m=1, cvals(5)
	  DO l=1, cvals(4)
	    DO k=1, cvals(3)
              PRINT *, '  #',TRIM(dnames(6)),' ;',svals(6)+n-1,TRIM(dnames(5)),' ;',svals(5)+m-1, &
	        TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,              &
		TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', &
		svals(1),char(44),svals(1)+cvals(1)-1
	      DO j=1, cvals(2)
	        PRINT 10,(vals(i,j,k,l,m,n),i=1,cvals(1))
	      END DO
	    END DO
	  END DO
	END DO
      END DO
    CASE(5)
      DO m=1, cvals(5)
        DO l=1, cvals(4)
	  DO k=1, cvals(3)
            PRINT *, '  #',TRIM(dnames(5)),' ;',svals(5)+m-1, TRIM(dnames(4)),' ;',svals(4)+l-1,  &
	      TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),           &
	      svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	    DO j=1, cvals(2)
	      PRINT 10,(vals(i,j,k,l,m,1),i=1,cvals(1))
	    END DO
	  END DO
	END DO
      END DO
    CASE(4)
      DO l=1, cvals(4)
	DO k=1, cvals(3)
          PRINT *, '  #',TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,     &
	    TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;',     &
	    svals(1),char(44),svals(1)+cvals(1)-1
	  DO j=1, cvals(2)
	    PRINT 10,(vals(i,j,k,l,1,1),i=1,cvals(1))
	  END DO
	END DO
      END DO
    CASE(3)
      DO k=1, cvals(3)
        PRINT *, '  #',TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),  &
	  svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	DO j=1, cvals(2)
	  PRINT 10,(vals(i,j,k,1,1,1),i=1,cvals(1))
	END DO
      END DO
    CASE(2)
      PRINT *, '  #',TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)), &
        ' ;',svals(1),char(44),svals(1)+cvals(1)-1
      DO j=1, cvals(2)
	PRINT 10,(vals(i,j,1,1,1,1),i=1,cvals(1))
      END DO
    CASE(1)
      PRINT *, '  #',TRIM(dnames(1)),' ;',svals(1),char(44),svals(1)+cvals(1)-1
      PRINT 10,(vals(i,1,1,1,1,1),i=1,cvals(1))  

  END SELECT

10 FORMAT(1x,1000(A1))

END SUBROUTINE Cprint

SUBROUTINE Rprint(vals, ndims, svals, cvals, dnames)
! Subroutine to print integer values

  IMPLICIT NONE
  
  INTEGER                                                 :: i,j,k,l,m,n
  INTEGER, INTENT(IN)                                     :: ndims
  INTEGER, DIMENSION(6), INTENT(IN)                       :: svals, cvals
  REAL, DIMENSION(cvals(1),cvals(2),cvals(3),cvals(4),                                            &
    cvals(5),cvals(6)), INTENT(IN)                        :: vals
  CHARACTER(LEN=100), DIMENSION(6), INTENT(IN)            :: dnames
  
!  PRINT *,'Desired values;___ __ _'
!  DO i=1,ndims
!    PRINT *,'o ',TRIM(dnames(i)),';',svals(i),char(44),svals(i)+cvals(i)
!  END DO
  SELECT CASE (ndims)
    CASE(6)
      DO n=1,cvals(6)
        DO m=1, cvals(5)
	  DO l=1, cvals(4)
	    DO k=1, cvals(3)
              PRINT *, '  #',TRIM(dnames(6)),' ;',svals(6)+n-1,TRIM(dnames(5)),' ;',svals(5)+m-1, &
	        TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,              &
		TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', &
		svals(1),char(44),svals(1)+cvals(1)-1
	      DO j=1, cvals(2)
	        PRINT 10,(vals(i,j,k,l,m,n),i=1,cvals(1))
	      END DO
	    END DO
	  END DO
	END DO
      END DO
    CASE(5)
      DO m=1, cvals(5)
        DO l=1, cvals(4)
	  DO k=1, cvals(3)
            PRINT *, '  #',TRIM(dnames(5)),' ;',svals(5)+m-1, TRIM(dnames(4)),' ;',svals(4)+l-1,  &
	      TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),           &
	      svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	    DO j=1, cvals(2)
	      PRINT 10,(vals(i,j,k,l,m,1),i=1,cvals(1))
	    END DO
	  END DO
	END DO
      END DO
    CASE(4)
      DO l=1, cvals(4)
	DO k=1, cvals(3)
          PRINT *, '  #',TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,     &
	    TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;',     &
	    svals(1),char(44),svals(1)+cvals(1)-1
	  DO j=1, cvals(2)
	    PRINT 10,(vals(i,j,k,l,1,1),i=1,cvals(1))
	  END DO
	END DO
      END DO
    CASE(3)
      DO k=1, cvals(3)
        PRINT *, '  #',TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),  &
	  svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	DO j=1, cvals(2)
	  PRINT 10,(vals(i,j,k,1,1,1),i=1,cvals(1))
	END DO
      END DO
    CASE(2)
      PRINT *, '  #',TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)), &
        ' ;',svals(1),char(44),svals(1)+cvals(1)-1
      DO j=1, cvals(2)
	PRINT 10,(vals(i,j,1,1,1,1),i=1,cvals(1))
      END DO
    CASE(1)
      PRINT *, '  #',TRIM(dnames(1)),' ;',svals(1),char(44),svals(1)+cvals(1)-1
      PRINT 10,(vals(i,1,1,1,1,1),i=1,cvals(1))  

  END SELECT

10 FORMAT(1x,1000(F10.5,1x))

END SUBROUTINE Rprint

SUBROUTINE Dprint(vals, ndims, svals, cvals, dnames)
! Subroutine to print double precission values

  IMPLICIT NONE
  
  INTEGER                                                 :: i,j,k,l,m,n
  INTEGER, INTENT(IN)                                     :: ndims
  INTEGER, DIMENSION(6), INTENT(IN)                       :: svals, cvals
  DOUBLE PRECISION, DIMENSION(cvals(1),cvals(2),                              &
    cvals(3),cvals(4),cvals(5),cvals(6)), INTENT(IN)      :: vals
  CHARACTER(LEN=100), DIMENSION(6), INTENT(IN)            :: dnames
  
!  PRINT *,'Desired values;___ __ _'
!  DO i=1,ndims
!    PRINT *,'o ',TRIM(dnames(i)),';',svals(i),char(44),svals(i)+cvals(i)
!  END DO
  SELECT CASE (ndims)
    CASE(6)
      DO n=1,cvals(6)
        DO m=1, cvals(5)
	  DO l=1, cvals(4)
	    DO k=1, cvals(3)
              PRINT *, '  #',TRIM(dnames(6)),' ;',svals(6)+n-1,TRIM(dnames(5)),' ;',svals(5)+m-1, &
	        TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,              &
		TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', &
		svals(1),char(44),svals(1)+cvals(1)-1
	      DO j=1, cvals(2)
	        PRINT 10,(vals(i,j,k,l,m,n),i=1,cvals(1))
	      END DO
	    END DO
	  END DO
	END DO
      END DO
    CASE(5)
      DO m=1, cvals(5)
        DO l=1, cvals(4)
	  DO k=1, cvals(3)
            PRINT *, '  #',TRIM(dnames(5)),' ;',svals(5)+m-1, TRIM(dnames(4)),' ;',svals(4)+l-1,  &
	      TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),           &
	      svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	    DO j=1, cvals(2)
	      PRINT 10,(vals(i,j,k,l,m,1),i=1,cvals(1))
	    END DO
	  END DO
	END DO
      END DO
    CASE(4)
      DO l=1, cvals(4)
	DO k=1, cvals(3)
          PRINT *, '  #',TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,     &
	    TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;',     &
	    svals(1),char(44),svals(1)+cvals(1)-1
	  DO j=1, cvals(2)
	    PRINT 10,(vals(i,j,k,l,1,1),i=1,cvals(1))
	  END DO
	END DO
      END DO
    CASE(3)
      DO k=1, cvals(3)
        PRINT *, '  #',TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),  &
	  svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	DO j=1, cvals(2)
	  PRINT 10,(vals(i,j,k,1,1,1),i=1,cvals(1))
	END DO
      END DO
    CASE(2)
      PRINT *, '  #',TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)), &
        ' ;',svals(1),char(44),svals(1)+cvals(1)-1
      DO j=1, cvals(2)
	PRINT 10,(vals(i,j,1,1,1,1),i=1,cvals(1))
      END DO
    CASE(1)
      PRINT *, '  #',TRIM(dnames(1)),' ;',svals(1),char(44),svals(1)+cvals(1)-1
      PRINT 10,(vals(i,1,1,1,1,1),i=1,cvals(1))  

  END SELECT

10 FORMAT(1x,1000(D20.10,1x))

END SUBROUTINE Dprint

SUBROUTINE Iprint(vals, ndims, svals, cvals, dnames)
! Subroutine to print integer values

  IMPLICIT NONE
  
  INTEGER                                                 :: i,j,k,l,m,n
  INTEGER, INTENT(IN)                                     :: ndims
  INTEGER, DIMENSION(6), INTENT(IN)                       :: svals, cvals
  INTEGER, DIMENSION(cvals(1),cvals(2),cvals(3),cvals(4),                                         &
    cvals(5),cvals(6)), INTENT(IN)                        :: vals
  CHARACTER(LEN=100), DIMENSION(6), INTENT(IN)            :: dnames
  
!  PRINT *,'Desired values;___ __ _'
!  DO i=1,ndims
!    PRINT *,'o ',TRIM(dnames(i)),';',svals(i),char(44),svals(i)+cvals(i)
!  END DO
  SELECT CASE (ndims)
    CASE(6)
      DO n=1,cvals(6)
        DO m=1, cvals(5)
	  DO l=1, cvals(4)
	    DO k=1, cvals(3)
              PRINT *, '  #',TRIM(dnames(6)),' ;',svals(6)+n-1,TRIM(dnames(5)),' ;',svals(5)+m-1, &
	        TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,              &
		TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', &
		svals(1),char(44),svals(1)+cvals(1)-1
	      DO j=1, cvals(2)
	        PRINT 10,(vals(i,j,k,l,m,n),i=1,cvals(1))
	      END DO
	    END DO
	  END DO
	END DO
      END DO
    CASE(5)
      DO m=1, cvals(5)
        DO l=1, cvals(4)
	  DO k=1, cvals(3)
            PRINT *, '  #',TRIM(dnames(5)),' ;',svals(5)+m-1, TRIM(dnames(4)),' ;',svals(4)+l-1,  &
	      TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),           &
	      svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	    DO j=1, cvals(2)
	      PRINT 10,(vals(i,j,k,l,m,1),i=1,cvals(1))
	    END DO
	  END DO
	END DO
      END DO
    CASE(4)
      DO l=1, cvals(4)
	DO k=1, cvals(3)
          PRINT *, '  #',TRIM(dnames(4)),' ;',svals(4)+l-1,TRIM(dnames(3)),' ;',svals(3)+k-1,     &
	    TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;',     &
	    svals(1),char(44),svals(1)+cvals(1)-1
	  DO j=1, cvals(2)
	    PRINT 10,(vals(i,j,k,l,1,1),i=1,cvals(1))
	  END DO
	END DO
      END DO
    CASE(3)
      DO k=1, cvals(3)
        PRINT *, '  #',TRIM(dnames(3)),' ;',svals(3)+k-1,TRIM(dnames(2)),' ;',svals(2),char(44),  &
	  svals(2)+cvals(2)-1, TRIM(dnames(1)),' ;', svals(1),char(44),svals(1)+cvals(1)-1
	DO j=1, cvals(2)
	  PRINT 10,(vals(i,j,k,1,1,1),i=1,cvals(1))
	END DO
      END DO
    CASE(2)
      PRINT *, '  #',TRIM(dnames(2)),' ;',svals(2),char(44),svals(2)+cvals(2)-1, TRIM(dnames(1)), &
        ' ;',svals(1),char(44),svals(1)+cvals(1)-1
      DO j=1, cvals(2)
	PRINT 10,(vals(i,j,1,1,1,1),i=1,cvals(1))
      END DO
    CASE(1)
      PRINT *, '  #',TRIM(dnames(1)),' ;',svals(1),char(44),svals(1)+cvals(1)-1
      PRINT 10,(vals(i,1,1,1,1,1),i=1,cvals(1))  

  END SELECT

10 FORMAT(1x,1000(I10,1x))

END SUBROUTINE Iprint
